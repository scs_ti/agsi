<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/cancelamento_motivo_usu.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="usu_cancelamento_motivo.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Cancelamento por Usuário</h3>
    </div>
    <br>


    <?php 

        $_SESSION['selecionar_por'] = $_POST['selecionar_por'];

        if ($_POST['selecionar_por'] == 'P') {
        echo('
            <section class="section">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped" id="table1">
                            <thead>
                                <tr>');
                                    echo("<th>NroUsuarios</th>
                                          <th>CodEmpresa</th>
                                          <th>Empresa</th>
                                          <th>CodPlano</th>
                                          <th>Plano</th>
                                          <th>CodMotivo</th>
                                          <th>Motivo</th>");
                                echo("</tr>
                                        </thead>
                                        <tbody>");
                                while( $row = sqlsrv_fetch_array($mot_cancelamento, SQLSRV_FETCH_ASSOC) ) {
                                echo "<tr>";
                                    echo "<td>".$row['NroUsuarios']. "</td>
                                          <td>".$row['CodEmpresa']. "</td>
                                          <td>".$row['Empresa']. "</td>
                                          <td>".$row['CodPlano']. "</td>
                                          <td>".$row['Plano']. "</td>
                                          <td>".$row['CodMotivo']. "</td>
                                          <td>".$row['Motivo']. "</td>
                                          </tr>";
                            }
                            echo('</tbody>
                                    </table>
                                    <div class="col-12 d-flex col-md-6 order-md-1">
                                        <a href="gerar_planilha_mot_cancelamento.php" style="color: white;">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Salvar</a></button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>');
        }  
    ?>
   
    <?php if ($_POST['selecionar_por'] == 'F') {
        echo('
            <section class="section">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped" id="table1">
                            <thead>
                                <tr>');
                                    if(strlen($ft1) > 0){
                                    echo("<th>".$ft1."</th>");
                                    }
                                    if(strlen($ft2) > 0){
                                            echo("<th>".$ft2."</th>");
                                        }
                                    if(strlen($ft3) > 0){
                                            echo("<th>".$ft3."</th>");
                                        }
                                    if(strlen($ft4) > 0){
                                            echo("<th>".$ft4."</th>");
                                        }
                                    if(strlen($ft5) > 0){
                                            echo("<th>".$ft5."</th>");
                                        }
                                    echo("<th>Total de usuários cancelados</th>");
                                echo("</tr>
                                        </thead>
                                        <tbody>");
                                while( $row = sqlsrv_fetch_array($mot_cancelamento, SQLSRV_FETCH_ASSOC) ) {
                                echo "<tr>";

                                    if(strlen($ft1) > 0){
                                        echo("<td>".$row[$ft1]. "</td>");
                                    }
                                    if(strlen($ft2) > 0){
                                        echo("<td>".$row[$ft2]. "</td>");
                                    }
                                    if(strlen($ft3) > 0){
                                        echo("<td>".$row[$ft3]. "</td>");
                                    }
                                    if(strlen($ft4) > 0){
                                        echo("<td>".$row[$ft4]. "</td>");
                                    }
                                    if(strlen($ft5) > 0){
                                        echo("<td>".$row[$ft5]. "</td>");
                                    }
                                    echo("<td>".$row['Total de usuários cancelados']. "</td>");"
                                </tr>";
                            }
                            echo('</tbody>
                                    </table>
                                    <div class="col-12 d-flex col-md-6 order-md-1">
                                        <a href="gerar_planilha_mot_cancelamento.php" style="color: white;">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Salvar</a></button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>');
        }  
    ?>
    
                        

    <?php include '../html/footer.html' ?>

