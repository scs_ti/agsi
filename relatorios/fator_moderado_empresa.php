<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$empresa_ini = sqlsrv_query($conn, 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc');
    if( 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$empresa_fim = sqlsrv_query($conn, 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc');
    if( 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$mot_cancelamento = sqlsrv_query($conn, 'select * from Mot_Cancelamento mc');
    if( 'select * from Mot_Cancelamento mc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$tipo_guia = sqlsrv_query($conn, 'SELECT ProcGuia_cd, ProcGuia_ds FROM Proc_Guia');
    if ('SELECT ProcGuia_cd, ProcGuia_ds FROM Proc_Guia' === false) {
        die( print_r( sqlsrv_errors(), true));
    }

$_SESSION['valor_atual'] = '';
#sqlsrv_query($conn, "DROP TABLE TB_USERS");
#    if( "DROP VIEW TB_USERS" === false) {
#        die( print_r( sqlsrv_errors(), true) );
#    }

#sqlsrv_query($conn, "DROP VIEW relat_cancelamento_usu");
#    if( "DROP VIEW relat_cancelamento_usu" === false) {
#        die( print_r( sqlsrv_errors(), true) );
#    }

?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Fator Mod Empresa</h3>
    </div><br>

    

    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <!-- -->
                            <div class="col-md-12 col-12">
                                <form id="simples-formulario-ajax">
                                    <fieldset>
                                        <label>Tipo de Guia</label>
                                        <div class="col-md-12 col-12 d-flex">
                                            <select id="options" class="form-control" name="usu" onchange="tipo_guia(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                            </select>
    
                                            <select class="form-control" disabled="disabled" id="nome" style="margin-left: 10px">
                                                    <option></option>            
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $tipo_guia, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['ProcGuia_cd'].">".$row['ProcGuia_cd']." - ".$row['ProcGuia_ds']."</option>";}
                                                        ?>
                                            </select>
                                            <input type="submit" id="Enviar" class="btn btn-primary" value="Cadastrar" / style="margin-left: 10px";>

                                            <a href="apagua_guia.php" class="col-md-1 col-12">
                                                <img border="0" src="https://img.icons8.com/ios/452/trash--v1.png" width="40px">
                                            </a>
                                        </div>                             
                                        <input type="hidden" id="metodo" value="formulario-ajax" />
                                    </fieldset>
                                </form>
                            </div>
                    
                            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                            <script src="../js/formulario.js"></script>

                            <form class="form" action="fator_mod_emp_desc.php" method="POST">
                                    <div class="row">
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo Relatório</label>
                                                <select class="form-control" name="tipo_relatorio">
                                                    <option value="A">Analítico</option>
                                                    <option value="S">Sintético</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo de Empresa</label>
                                                <select class="form-control" name="tipo_empresa">
                                                    <option value="todas">Todas</option>
                                                    <option value="F">Física</option>
                                                    <option value="J">Jurídica</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo de Cobrança</label>
                                                <select class="form-control" name="tipo_cobranca">
                                                    <option value="todas">Todas</option>
                                                    <option value="C">Mensal (Boleto)</option>
                                                    <option value="G">Emissão da Guia</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Operador</label>
                                                <select id="options" class="form-control" name="usu" onchange="opeexiste(this.value)">
                                                    <option value="Todos">Todos</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Código do Operador</label>
                                                <input type="text" class="form-control" name="operator" id="operator" disabled="desabled" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Região</label>
                                                <select id="options" class="form-control" name="usu" onchange="seleRegiao(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Selecionar Região</label>
                                                <select class="form-control" id="regiao" disabled name="regiao">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $consulta_regiao, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['Código'].">".$row['Código']." - ".$row['Região']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Status FM</label>
                                                <select class="form-control" name="status_fm">
                                                    <option value="todas">Todas</option>
                                                    <option value="A">Aberto</option>
                                                    <option value="P">Pago</option>
                                                    <option value="D">Descartado</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>FM vinculada à duplicata</label>
                                                <select class="form-control" name="fm_vinculada">
                                                    <option value="A">Ambos</option>
                                                    <option value="S">Sim</option>
                                                    <option value="N">Não</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Status Usuário</label>
                                                <select class="form-control" name="st_usuario">
                                                    <option value="todas">Todas</option>
                                                    <option value="A">Ativo</option>
                                                    <option value="C">Cancelado</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Dia de Vencimento do Usu</label>
                                                <select class="form-control" name="dia_vencimento_usu" onchange="verifica_dia(this.value)">
                                                    <option value="Todos">Todos</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Dia Inicial</label>
                                                <input type="number" class="form-control" name="dia_ini" disabled="disabled" id="dia_ini" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Dia Final</label>
                                                <input type="number" class="form-control" name="dia_fim" disabled="disabled" id="dia_fim" autocomplete="off">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <select id="options" class="form-control" name="empresa_vl" onchange="verifica_emp(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Inicial</label>
                                                <select class="form-control" id="input2" disabled name="empresa_ini_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_ini, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Final</label>
                                                <select class="form-control" id="input3" disabled name="empresa_fim_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_fim, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Grupo de Empresa</label>
                                                <select id="options" class="form-control" name="empresa" onchange="grupo_empresa(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Selecionar Grupo</label>
                                                <select class="form-control" id="gp_emp" disabled name="emp_cod_grup">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $consulta_grupo_empresa, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['grp_codigo'].">".$row['grp_codigo']." - ".$row['grp_Descricao']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Tipo Cobrança (Boleto)</label>
                                                <select class="form-control" name="tipo_co">
                                                    <option value="Todas">Todas</option>
                                                    <option value="U">Usuário</option>
                                                    <option value="E">Empresa</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Quebrar por:</label>
                                                <div>
                                                  <input type="checkbox" id="scales" name="scales"
                                                         checked>
                                                  <label for="scales">Empresa / Titular</label>
                                                </div>

                                                <div>
                                                  <input type="checkbox" id="horns" name="horns">
                                                  <label for="horns">Somente Consultas</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Usuário</label>
                                                <select id="options" class="form-control" name="usu" onchange="usuexiste(this.value)">
                                                    <option value="Todos">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Código do usuário</label>
                                                <input type="text" class="form-control" name="live_search" id="live_search" disabled="desabled" autocomplete="off">
                                                <div id="search_result"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Nome do usuário</label>
                                                <input type="text" class="form-control" name="nome_usu" id="nome_u" disabled="desabled" autocomplete="off">
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Ordenar Por:</label>
                                                <select class="form-control" name="ordenar">
                                                    <option value="T">Titular</option>
                                                    <option value="U">Usuário</option>
                                                    <option value="A">Atendimento</option>
                                                    <option value="L">Liberação</option>
                                                    <option value="G">Guia</option>
                                                    <option value="F">F.M.</option>
                                                </select>
                                            </div>
                                        </div>

                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $("#live_search").keyup(function () {
                                                    var query = $(this).val();
                                                    if (query != "") {
                                                        $.ajax({
                                                            url: '../consultas/consulta_existe_usu.php',
                                                            method: 'POST',
                                                            data: {
                                                                query: query
                                                            },
                                                            success: function (data) {
                                                                $('#search_result').html(data);
                                                                $('#search_result').css('display', 'bloqued');

                                                                $("#live_search").focusout(function () {
                                                                    $('#search_result').css('display', 'none');
                                                                });
                                                                $("#live_search").focusin(function () {
                                                                    $('#search_result').css('display', 'block');
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        $('#search_result').css('display', 'none');
                                                    }
                                                });
                                            });
                                        </script>

                                        

                                        <p>
                                            <hr>
                                            <div class="col-md-6 col-12">
                                                <h3>Guias Períodos</h3>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <h3>Datas de Cancelamento dos Usuários</h3>
                                            </div>
                                            <br>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Liberação Guia</label>
                                                    <select id="options" class="form-control" name="lib_guia" onchange="libera_guia(this.value)">
                                                        <option value="Todas">Todas</option>
                                                        <option value="Selecionar">Selecionar</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Inicial</label>
                                                    <input type="date" class="form-control" name="ldti" id="guia_ini" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Final</label>
                                                    <input type="date" class="form-control" name="ldtf" id="guia_fim" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Cancelamento </label>
                                                    <select id="options" class="form-control" name="canc" onchange="can_termino(this.value)">
                                                        <option value="Todas">Todas</option>
                                                        <option value="Selecionar">Selecionar</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Inicial</label>
                                                    <input type="date" class="form-control" name="canc_ini" id="dt_can_u_ini" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Final</label>
                                                    <input type="date" class="form-control" name="canc_fim" id="dt_can_u_fim" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                        </p>
                                        <p>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Emissão Guia</label>
                                                    <select id="options" class="form-control" name="emiss_guia" onchange="emissao_guia(this.value)">
                                                        <option value="Todas">Todas</option>
                                                        <option value="Selecionar">Selecionar</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Inicial</label>
                                                    <input type="date" class="form-control" name="emgi" id="emit_guia_ini" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Final</label>
                                                    <input type="date" class="form-control" name="emgf" id="emit_guia_fim" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Movi Cancelamento</label>
                                                    <select id="options" class="form-control" name="movi_can" onchange="mov_cancelamento(this.value)">
                                                        <option value="Todas">Todas</option>
                                                        <option value="Selecionar">Selecionar</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Inicial</label>
                                                    <input type="date" class="form-control" name="movi_can_ini" id="mov_can_ini" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Final</label>
                                                    <input type="date" class="form-control" name="movi_can_fim" id="mov_can_fim" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                        </p>
                                        <p>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Fechamento Copart</label>
                                                    <select id="options" class="form-control" name="fech" onchange="fechamento(this.value)">
                                                        <option value="Todas">Todas</option>
                                                        <option value="Selecionar">Selecionar</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Inicial</label>
                                                    <input type="date" class="form-control" name="fechamento_ini" id="fechamento_ini" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label>Data Final</label>
                                                    <input type="date" class="form-control" name="fechamento_fim" id="fechamento_fim" disabled="desabled" autocomplete="off">
                                                    <div id="search_result"></div>
                                                </div>
                                            </div>
                                        </p>
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>