<section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                    <div class="row">
                                        <p>
                                            <h3>Selecione os Campos que deseja Visualizar na Consulta</h3>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cod_Titular" value="Cod_Titular" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cod Titular
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Titular" value="Titular" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Titular
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cod_Motivo" value="Cod_Motivo" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód.Motivo
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Descr_Motivo" value="Descr_Motivo" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Descr. Motivo
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cd_Empresa" value="Cd_Empresa" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód.Empresa
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Descr_empresa" value="Descr_empresa" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Descr. Empresa
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cod_abrange" value="Cod_abrange" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód.Abrange
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Codigo" value="Codigo" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Código
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Nome" value="Nome" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nome
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Dt_termino" value="Dt_termino" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Dt.Término
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Fone" value="Fone" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Fone
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Endereco" value="Endereco" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Endereço
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cidade" value="Cidade" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cidade
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Dt_Inclusao" value="Dt_Inclusao" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Dt Inclusão
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Vendedor" value="Vendedor" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Vendedor
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="N_meses" value="N_meses" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nº Meses
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="N_parcelas_Pagas" value="N_parcelas_Pagas" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nº Parcelas Pagas
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="N_parcelas_aberto" value="N_parcelas_aberto" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nº Parcelas Aberto
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="N_parcelas_canceladas" value="N_parcelas_canceladas" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nº Parcelas Canceladas
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Valor" value="Valor" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Valor
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Tipo" value="Tipo" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Tipo
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cod_op_Repasse" value="Cod_op_Repasse" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód. Op. Repasse
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Op_repasse" value="Op_repasse" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Op. Repasse
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Op_cancelamento" value="Op_cancelamento" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Op. Cancelamento
                                              </label>
                                            </div> 
                                        </p>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>