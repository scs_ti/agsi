<section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                    <div class="row">
                                        <p>
                                            <h3>Selecione os Campos que deseja Visualizar na Consulta</h3>
                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cd_Empresa" value="[Codigo Empresa]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Codigo Empresa
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Desc_Empresa" value="[Desc Empresa]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Desc Empresa
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cod_Titular" value="[Cod.Titular]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cod.Titular
                                              </label>
                                            </div>
                                            
                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Nome_Titular" value="[Nome Titular]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nome Titular
                                              </label>
                                            </div>    

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Codigo" value="[Código]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Código
                                              </label>
                                            </div> 

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Vencimento" value="[Vencimento]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Vencimento
                                              </label>
                                            </div> 

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Nome_Usuario" value="[Nome Usuário]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nome Usuário
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Codigo_Interno" value="[Código Interno]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Código Interno
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="AMB" value="[AMB]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                AMB
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="FM_Procedimento" value="[FM Procedimento]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                FM Procedimento
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Descricao_AMB" value="[Descrição AMB]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Descrição AMB
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Atendimento" value="[Atendimento]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Atendimento
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Liberacao" value="[Liberação]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Liberação
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Emissao" value="[Emissão]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Emissão
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Especialidade" value="[Especialidade]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Especialidade
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Especialidade_Desc" value="[Especialidade Desc]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Especialidade Desc
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Tipo_Guia" value="[Tipo Guia]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Tipo Guia
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Proc_Desc" value="[Proc Desc]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Proc Desc
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cod_Guia" value="[Cod. Guia]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cod. Guia
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Desc_Fornecedor" value="[Desc Fornecedor]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Desc Fornecedor
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Valor_Fator" value="[Valor Fator]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Valor Fator
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Quantidade" value="[Quantidade]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Quantidade
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Total" value="[Total]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Total
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Operador" value="[Operador]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Operador
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="OBS" value="[OBS]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                OBS
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cd_Tit_Cob" value="[Cód. Titular de Cobrança]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód. Titular de Cobrança
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Nm_Tit_Cob" value="[Nome Titular de Cobrança]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nome Titular de Cobrança
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="CPF_titular" value="[CPF Titular]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                CPF Titular
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Grupo_FM" value="[Grupo FM]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Grupo FM
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Dt_Pagto" value="[Dt.Pagto]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Dt.Pagto
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Duplicata" value="[Duplicata]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Duplicata
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cd_Local_Realizacao" value="[Cód. Local Realizacao]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód. Local Realizacao
                                              </label>
                                            </div>

                                            <div class="form-check col-md-3 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Local_Realizacao" value="[Local Realizacao]" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Local Realizacao
                                              </label>
                                            </div>
                                        </p>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>