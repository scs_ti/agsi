<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/relatorio_usu_can_motivo.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="usuario_cancelamento.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Cancelamento</h3>
    </div>
    <br>

    <?php include 'list_form_cancelamento.php';
#    
    if (!is_null($_POST['Cod_Titular'])) {
        $var1 = 'cod_titular';
        $_SESSION['var1'] = 'cod_titular';
    }else{
        $var1 = "";
        $_SESSION['var1'] = 'cod_titular';
    }
#
    if (!is_null($_POST['Titular']) and !is_null($_POST['Cod_Titular'])) {
        $var2 = ", titular";
        $_SESSION['var2'] = ", titular";
    }elseif (!is_null($_POST['Titular']) and is_null($_POST['Cod_Titular'])){
        $var2 = "titular";
        $_SESSION['var2'] = "titular";
    }else{
        $var2 = "";
        $_SESSION['var2'] = "";
    }
#
    if (!is_null($_POST['Cod_Motivo']) and !is_null($_POST['Cod_Titular'])) {
        $var3 = ", cod_motivo";
        $_SESSION['var3'] = ", cod_motivo";
    }elseif (!is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo'])){
        $var3 = "cod_motivo";
        $_SESSION['var3'] = "cod_motivo";
    }else{
        $var3 = "";
        $_SESSION['var3'] = "";
    }
#
    if (!is_null($_POST['Descr_Motivo']) and !is_null($_POST['Cod_Titular'])) {
        $var4 = ", descr_motivo";
        $_SESSION['var4'] = ", descr_motivo";
    }elseif (!is_null($_POST['Descr_Motivo']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo'])){
        $var4 = "descr_motivo";
        $_SESSION['var4'] = "descr_motivo";
    }else{
        $var4 = "";
        $_SESSION['var4'] = "";
    }
#
    if (!is_null($_POST['Cd_Empresa']) and !is_null($_POST['Cod_Titular'])) {
        $var5 = ", cod_empresa";
        $_SESSION['var5'] = ", cod_empresa";
    }elseif (!is_null($_POST['Cd_Empresa']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo'])){
        $var5 = "cod_empresa";
        $_SESSION['var5'] = "cod_empresa";
    }else{
        $var5 = "";
        $_SESSION['var5'] = "";
    }
#
    if (!is_null($_POST['Descr_empresa']) and !is_null($_POST['Cod_Titular'])) {
        $var6 = ", descr_empresa";
        $_SESSION['var6'] = ", descr_empresa";
    }elseif (!is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa'])){
        $var6 = "descr_empresa";
        $_SESSION['var6'] = "descr_empresa";
    }else{
        $var6 = "";
        $_SESSION['var6'] = "";
    }
#
    if (!is_null($_POST['Cod_abrange']) and !is_null($_POST['Cod_Titular'])) {
        $var7 = ", cod_abrange";
        $_SESSION['var7'] = ", cod_abrange";
    }elseif (!is_null($_POST['Cod_abrange']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa'])){
        $var7 = "cod_abrange";
        $_SESSION['var7'] = "cod_abrange";
    }else{
        $var7 = "";
        $_SESSION['var7'] = "";
    }
#
    if (!is_null($_POST['Codigo']) and !is_null($_POST['Cod_Titular'])) {
        $var8 = ", codigo";
        $_SESSION['var8'] = ", codigo";
    }elseif (!is_null($_POST['Codigo']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange'])){
        $var8 = "codigo";
        $_SESSION['var8'] = "codigo";
    }else{
        $var8 = "";
        $_SESSION['var8'] = "";
    }
#
    if (!is_null($_POST['Nome']) and !is_null($_POST['Cod_Titular'])) {
        $var9 = ", nome";
        $_SESSION['var9'] = ", nome";
    }elseif (!is_null($_POST['Nome']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo'])){
        $var9 = "nome";
        $_SESSION['var9'] = "nome";
    }else{
        $var9 = "";
        $_SESSION['var9'] = "";
    }
#
    if (!is_null($_POST['Dt_termino']) and !is_null($_POST['Cod_Titular'])) {
        $var10 = ", dt_termino";
        $_SESSION['var10'] = ", dt_termino";
    }elseif (!is_null($_POST['Dt_termino']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome'])){
        $var10 = "dt_termino";
        $_SESSION['var10'] = "dt_termino";
    }else{
        $var10 = "";
        $_SESSION['var10'] = "";
    }
#
    if (!is_null($_POST['Fone']) and !is_null($_POST['Cod_Titular'])) {
        $var11 = ", fone";
        $_SESSION['var11'] = ", fone";
    }elseif (!is_null($_POST['Fone']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino'])){
        $var11 = "fone";
        $_SESSION['var11'] = "fone";
    }else{
        $var11 = "";
        $_SESSION['var11'] = "";
    }
#
    if (!is_null($_POST['Endereco']) and !is_null($_POST['Cod_Titular'])) {
        $var12 = ", Endereco";
        $_SESSION['var12'] = ", Endereco";
    }elseif (!is_null($_POST['Endereco']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone'])){
        $var12 = "Endereco";
        $_SESSION['var12'] = "Endereco";
    }else{
        $var12 = "";
        $_SESSION['var12'] = "";
    }
#
    if (!is_null($_POST['Cidade']) and !is_null($_POST['Cod_Titular'])) {
        $var13 = ", cidade";
        $_SESSION['var13'] = ", cidade";
    }elseif (!is_null($_POST['Cidade']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco'])){
        $var13 = "cidade";
        $_SESSION['var13'] = "cidade";
    }else{
        $var13 = "";
        $_SESSION['var13'] = "";
    }
#
    if (!is_null($_POST['Dt_Inclusao']) and !is_null($_POST['Cod_Titular'])) {
        $var14 = ", dt_inclusao";
        $_SESSION['var14'] = ", dt_inclusao";
    }elseif (!is_null($_POST['Dt_Inclusao']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade'])){
        $var14 = "dt_inclusao";
        $_SESSION['var14'] = "dt_inclusao";
    }else{
        $var14 = "";
        $_SESSION['var14'] = "";
    }
#
    if (!is_null($_POST['Vendedor']) and !is_null($_POST['Cod_Titular'])) {
        $var15 = ", vendedor";
        $_SESSION['var15'] = ", vendedor";
    }elseif (!is_null($_POST['Vendedor']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao'])){
        $var15 = "vendedor";
        $_SESSION['var15'] = "vendedor";
    }else{
        $var15 = "";
        $_SESSION['var15'] = "";
    }
#
    if (!is_null($_POST['N_meses']) and !is_null($_POST['Cod_Titular'])) {
        $var16 = ", n_meses";
        $_SESSION['var16'] = ", n_meses";
    }elseif (!is_null($_POST['N_meses']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor'])){
        $var16 = "n_meses";
        $_SESSION['var16'] = "n_meses";
    }else{
        $var16 = "";
        $_SESSION['var16'] = "";
    }
#
    if (!is_null($_POST['N_parcelas_Pagas']) and !is_null($_POST['Cod_Titular'])) {
        $var17 = ", n_parcelas_Pagas";
        $_SESSION['var17'] = ", n_parcelas_Pagas";
    }elseif (!is_null($_POST['N_parcelas_Pagas']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses'])){
        $var17 = "n_parcelas_Pagas";
        $_SESSION['var17'] = "n_parcelas_Pagas";
    }else{
        $var17 = "";
        $_SESSION['var17'] = "";
    }
#
    if (!is_null($_POST['N_parcelas_aberto']) and !is_null($_POST['Cod_Titular'])) {
        $var18 = ", n_parcelas_aberto";
        $_SESSION['var18'] = ", n_parcelas_aberto";
    }elseif (!is_null($_POST['N_parcelas_aberto']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas'])){
        $var18 = "n_parcelas_aberto";
        $_SESSION['var18'] = "n_parcelas_aberto";
    }else{
        $var18 = "";
        $_SESSION['var18'] = "";
    }
#
    if (!is_null($_POST['N_parcelas_canceladas']) and !is_null($_POST['Cod_Titular'])) {
        $var19 = ", n_parcelas_canceladas";
        $_SESSION['var19'] = ", n_parcelas_canceladas";
    }elseif (!is_null($_POST['N_parcelas_canceladas']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas'])){
        $var19 = "n_parcelas_canceladas";
        $_SESSION['var19'] = "n_parcelas_canceladas";
    }else{
        $var19 = "";
        $_SESSION['var19'] = "";
    }
#
    if (!is_null($_POST['Valor']) and !is_null($_POST['Cod_Titular'])) {
        $var20 = ", valor";
        $_SESSION['var20'] = ", valor";
    }elseif (!is_null($_POST['Valor']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas']) and is_null($_POST['N_parcelas_canceladas'])){
        $var20 = "valor";
        $_SESSION['var20'] = "valor";
    }else{
        $var20 = "";
        $_SESSION['var20'] = "";
    }
#
    if (!is_null($_POST['Tipo']) and !is_null($_POST['Cod_Titular'])) {
        $var21 = ", Tipo";
        $_SESSION['var21'] = ", Tipo";
    }elseif (!is_null($_POST['Tipo']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas']) and is_null($_POST['N_parcelas_canceladas']) and is_null($_POST['Valor'])){
        $var21 = "Tipo";
        $_SESSION['var21'] = "Tipo";
    }else{
        $var21 = "";
        $_SESSION['var21'] = "";
    }
#
    if (!is_null($_POST['Cod_op_Repasse']) and !is_null($_POST['Cod_Titular'])) {
        $var22 = ", cod_op_repasse";
        $_SESSION['var22'] = ", cod_op_repasse";
    }elseif (!is_null($_POST['Cod_op_Repasse']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas']) and is_null($_POST['N_parcelas_canceladas']) and is_null($_POST['Valor']) and is_null($_POST['Tipo'])){
        $var22 = "cod_op_repasse";
        $_SESSION['var22'] = "cod_op_repasse";
    }else{
        $var22 = "";
        $_SESSION['var22'] = "";
    }
#
    if (!is_null($_POST['Op_repasse']) and !is_null($_POST['Cod_Titular'])) {
        $var23 = ", op_repasse";
        $_SESSION['var23'] = ", op_repasse";
    }elseif (!is_null($_POST['Op_repasse']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas']) and is_null($_POST['N_parcelas_canceladas']) and is_null($_POST['Valor']) and is_null($_POST['Tipo']) and is_null($_POST['Cod_op_Repasse'])){
        $var23 = "op_repasse";
        $_SESSION['var23'] = "op_repasse";
    }else{
        $var23 = "";
        $_SESSION['var23'] = "";
    }
#
    if (!is_null($_POST['Op_cancelamento']) and !is_null($_POST['Cod_Titular'])) {
        $var24 = ", op_cancelamento";
        $_SESSION['var24'] = ", op_cancelamento";
    }elseif (!is_null($_POST['Op_cancelamento']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Cod_Motivo']) and is_null($_POST['Descr_Motivo']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Descr_empresa']) and is_null($_POST['Cod_abrange']) and is_null($_POST['Codigo']) and is_null($_POST['Nome']) and is_null($_POST['Dt_termino']) and is_null($_POST['Fone']) and is_null($_POST['Endereco']) and is_null($_POST['Cidade']) and is_null($_POST['Dt_Inclusao']) and is_null($_POST['Vendedor']) and is_null($_POST['N_meses']) and is_null($_POST['N_parcelas_Pagas']) and is_null($_POST['N_parcelas_canceladas']) and is_null($_POST['Valor']) and is_null($_POST['Tipo']) and is_null($_POST['Cod_op_Repasse']) and is_null($_POST['Op_repasse'])){
        $var24 = "op_cancelamento";
        $_SESSION['var24'] = "op_cancelamento";
    }else{
        $var24 = "";
        $_SESSION['var24'] = "";
    }

    $_SESSION['usu_cancelamento'] = "select ".$var1.$var2.$var3.$var4.$var5.$var6.$var7.$var8.$var9.$var10.$var11.$var12.$var13.$var14.$var17.$var18.$var19.$var20.$var21.$var22.$var23.$var24." from relat_cancelamento_usu";

    $_SESSION['usu_cancelamento_sql'] = sqlsrv_query($conn, $_SESSION['usu_cancelamento']);
    if( $_SESSION['usu_cancelamento'] === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    ?>
    
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <?php
                            if(!empty($var1)){
                                echo("<th>Cod Titular</th>");
                            }if(!empty($var2)){
                                echo("<th>Titular</th>");
                            }if(!empty($var3)){
                                echo("<th>Cód.Motivo</th>");
                            }if(!empty($var4)){
                                echo("<th>Descr. Motivo</th>");
                            }if(!empty($var5)){
                                echo("<th>Cód.Empresa</th>");
                            }if(!empty($var6)){
                                echo("<th>Descr. Empresa</th>");
                            }if(!empty($var7)){
                                echo("<th>Cód.Abrange</th>");
                            }if(!empty($var8)){
                                echo("<th>Código</th>");
                            }if(!empty($var9)){
                                echo("<th>Nome</th>");
                            }if(!empty($var10)){
                                echo("<th>Dt.Término</th>");
                            }if(!empty($var11)){
                                echo("<th>Fone</th>");
                            }if(!empty($var12)){
                                echo("<th>Endereço</th>");
                            }if(!empty($var13)){
                                echo("<th>Cidade</th>");
                            }if(!empty($var14)){
                                echo("<th>Dt Inclusão</th>");
                            }if(!empty($var15)){
                                echo("<th>Vendedor</th>");
                            }if(!empty($var16)){
                                echo("<th>Nº Meses</th>");
                            }if(!empty($var17)){
                                echo("<th>Nº Parcelas Pagas</th>");
                            }if(!empty($var18)){
                                echo("<th>Nº Parcelas Aberto</th>");
                            }if(!empty($var19)){
                                echo("<th>Nº Parcelas Canceladas</th>");
                            }if(!empty($var20)){
                                echo("<th>Valor</th>");
                            }if(!empty($var21)){
                                echo("<th>Tipo</th>");
                            }if(!empty($var22)){
                                echo("<th>Cód. Op. Repasse</th>");
                            }if(!empty($var23)){
                                echo("<th>Op. Repasse</th>");
                            }if(!empty($var24)){
                                echo("<th>Op. Cancelamento</th>");
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 

                        while( $row = sqlsrv_fetch_array($_SESSION['usu_cancelamento_sql'], SQLSRV_FETCH_ASSOC) ) {

                        if(is_null($row['dt_termino'])){
                            $dt_termino = '';
                        }else{
                            $dt_termino = date_format($row['dt_termino'], 'd/m/Y');
                        }

                        if(is_null($row['Dt Inclusão'])){
                            $dt_inclusao = '';
                        }else{
                            $dt_inclusao = date_format($row['dt_inclusao'], 'd/m/Y');
                        }

                        echo "<tr>";?>
                        <?php 
                            if(!empty($var1)){
                                echo("<td>".$row['cod_titular'].                "</td>");
                            }if(!empty($var2)){
                                echo("<td>".$row['titular'].                "</td>");
                            }if(!empty($var3)){
                                echo("<td>".$row['cod_motivo'].               "</td>");
                            }if(!empty($var4)){
                                echo("<td>".$row['descr_motivo'].           "</td>");
                            }if(!empty($var5)){
                                echo("<td>".$row['cod_empresa'].                  "</td>");
                            }if(!empty($var6)){
                                echo("<td>".$row['descr_empresa'].            "</td>");
                            }if(!empty($var7)){
                                echo("<td>".$row['cod_abrange'].           "</td>");
                            }if(!empty($var8)){
                                echo("<td>".$row['codigo'].               "</td>");
                            }if(!empty($var9)){
                                echo("<td>".$row['nome'].                  "</td>");
                            }if(!empty($var10)){
                                echo("<td>".$dt_termino.                   "</td>");
                            }if(!empty($var11)){
                                echo("<td>".$row['fone'].                     "</td>");
                            }if(!empty($var12)){
                                echo("<td>".$row['Endereco'].                 "</td>");
                            }if(!empty($var13)){
                                echo("<td>".$row['cidade'].               "</td>");
                            }if(!empty($var14)){
                                echo("<td>".$dt_inclusao.               "</td>");
                            }if(!empty($var15)){
                                echo("<td>".$row['vendedor'].        "</td>");
                            }if(!empty($var16)){
                                echo("<td>".$row['n_meses'].       "</td>");
                            }if(!empty($var17)){
                                echo("<td>".$row['n_parcelas_Pagas'].           "</td>");
                            }if(!empty($var18)){
                                echo("<td>".$row['n_parcelas_aberto'].              "</td>");
                            }if(!empty($var19)){
                                echo("<td>".$row['n_parcelas_canceladas'].     "</td>");
                            }if(!empty($var20)){
                                echo("<td>".$row['valor'].              "</td>");
                            }if(!empty($var21)){
                                echo("<td>".$row['tipo'].        "</td>");
                            }if(!empty($var22)){
                                echo("<td>".$row['cod_op_repasse'].                     "</td>");
                            }if(!empty($var23)){
                                echo("<td>".$row['op_repasse'].              "</td>");
                            }if(!empty($var24)){
                                echo("<td>".$row['op_cancelamento'].      "</td>");
                            }"
                        </tr>";}
                      ?>
                        </tbody>
                    </table>
                    <div class="col-12 d-flex col-md-6 order-md-1">
                        <a href="gerar_planilha_cancelamento.php" style="color: white;">
                            <button type="submit" class="btn btn-primary me-1 mb-1">Salvar</a></button>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include '../html/footer.html' ?>

