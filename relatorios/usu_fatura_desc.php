<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/usuario_fatura_con.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="usuario_fatura.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Usuário por Fatura</h3>
    </div>
    <br>

    <?php include 'list_form.php'; ?>

    <?php 
    if (!is_null($_POST['Emp_Cd'])) {
        $var1 = $_POST['Emp_Cd'];
        $_SESSION['var1'] = $var1;
    }else{
        $var1 = "";
        $_SESSION['var1'] = $var1;
    }

    if (!is_null($_POST['Emp_Ds']) and !is_null($_POST['Emp_Cd'])) {
        $var2 = ", ".$_POST['Emp_Ds'];
        $_SESSION['var2'] = ", ".$_POST['Emp_Ds'];
    }elseif (!is_null($_POST['Emp_Ds']) and is_null($_POST['Emp_Cd'])){
        $var2 = $_POST['Emp_Ds'];
        $_SESSION['var2'] = $_POST['Emp_Ds'];
    }else{
        $var2 = "";
        $_SESSION['var2'] = "";
    }

    if (!is_null($_POST['Titular']) and !is_null($_POST['Emp_Cd'])) {
        $var3 = ", ".$_POST['Titular'];
        $_SESSION['var3'] = ", ".$_POST['Titular'];
    }elseif (!is_null($_POST['Titular']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds'])){
        $var3 = $_POST['Titular'];
        $_SESSION['var3'] = $_POST['Titular'];
    }else{
        $var3 = "";
        $_SESSION['var3'] = $var3;
    }

    if (!is_null($_POST['Ocorrencias']) and !is_null($_POST['Emp_Cd'])) {
        $var4 = ", ".$_POST['Ocorrencias'];
        $_SESSION['var4'] = ", ".$_POST['Ocorrencias'];
    }elseif (!is_null($_POST['Ocorrencias']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular'])){
        $var4 = $_POST['Ocorrencias'];
        $_SESSION['var4'] = $_POST['Ocorrencias'];
    }else{
        $var4 = "";
        $_SESSION['var4'] = $var4;
    }

    if (!is_null($_POST['Tipo']) and !is_null($_POST['Emp_Cd'])) {
        $var5 = ", ".$_POST['Tipo'];
        $_SESSION['var5'] = ", ".$_POST['Tipo'];
    }elseif (!is_null($_POST['Tipo']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias'])){
        $var5 = $_POST['Tipo'];
        $_SESSION['var5'] = $var5;
    }else{
        $var5 = "";
        $_SESSION['var5'] = $var5;
    }

    if (!is_null($_POST['CodTitular']) and !is_null($_POST['Emp_Cd'])) {
        $var6 = ", ".$_POST['CodTitular'];
        $_SESSION['var6'] = ", ".$_POST['CodTitular'];
    }elseif (!is_null($_POST['CodTitular']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo'])){
        $var6 = $_POST['CodTitular'];
        $_SESSION['var6'] = $var6;
    }else{
        $var6 = "";
        $_SESSION['var6'] = $var6;
    }

    if (!is_null($_POST['Usu_Interno']) and !is_null($_POST['Emp_Cd'])) {
        $var7 = ", ".$_POST['Usu_Interno'];
        $_SESSION['var7'] = ", ".$_POST['Usu_Interno'];
    }elseif (!is_null($_POST['Usu_Interno']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular'])){
        $var7 = $_POST['Usu_Interno'];
        $_SESSION['var7'] = $var7;
    }else{
        $var7 = "";
        $_SESSION['var7'] = $var7;
    }

    if (!is_null($_POST['URe_Doc']) and !is_null($_POST['Emp_Cd'])) {
        $var8 = ", ".$_POST['URe_Doc'];
        $_SESSION['var8'] = ", ".$_POST['URe_Doc'];
    }elseif (!is_null($_POST['URe_Doc']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno'])){
        $var8 = $_POST['URe_Doc'];
        $_SESSION['var8'] = $var8;
    }else{
        $var8 = "";
        $_SESSION['var8'] = $var8;
    }

    if (!is_null($_POST['NFSE']) and !is_null($_POST['Emp_Cd'])) {
        $var9 = ", ".$_POST['NFSE'];
        $_SESSION['var9'] = ", ".$_POST['NFSE'];
    }elseif (!is_null($_POST['NFSE']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc'])){
        $var9 = $_POST['NFSE'];
        $_SESSION['var9'] = $var9;
    }else{
        $var9 = "";
        $_SESSION['var9'] = $var9;
    }

    if (!is_null($_POST['pg_emissao']) and !is_null($_POST['Emp_Cd'])) {
        $var10 = ", ".$_POST['pg_emissao'];
        $_SESSION['var10'] = ", ".$_POST['pg_emissao'];
    }elseif (!is_null($_POST['pg_emissao']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE'])){
        $var10 = $_POST['pg_emissao'];
        $_SESSION['var10'] = $var10;
    }else{
        $var10 = "";
        $_SESSION['var10'] = $var10;
    }

    if (!is_null($_POST['DtVencto']) and !is_null($_POST['Emp_Cd'])) {
        $var11 = ", ".$_POST['DtVencto'];
        $_SESSION['var11'] = ", ".$_POST['DtVencto'];
    }elseif (!is_null($_POST['DtVencto']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao'])){
        $var11 = $_POST['DtVencto'];
        $_SESSION['var11'] = $var11;
    }else{
        $var11 = "";
        $_SESSION['var11'] = $var11;
    }

    if (!is_null($_POST['BRec_DtBaixa']) and !is_null($_POST['Emp_Cd'])) {
        $var12 = ", ".$_POST['BRec_DtBaixa'];
        $_SESSION['var12'] = ", ".$_POST['BRec_DtBaixa'];
    }elseif (!is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto'])){
        $var12 = $_POST['BRec_DtBaixa'];
        $_SESSION['var12'] = $var12;
    }else{
        $var12 = "";
        $_SESSION['var12'] = $var12;
    }

    if (!is_null($_POST['Emp_CGC']) and !is_null($_POST['Emp_Cd'])) {
        $var13 = ", ".$_POST['Emp_CGC'];
        $_SESSION['var13'] = ", ".$_POST['Emp_CGC'];
    }elseif (!is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa'])){
        $var13 = $_POST['Emp_CGC'];
        $_SESSION['var13'] = $var13;
    }else{
        $var13 = "";
        $_SESSION['var13'] = $var13;
    }

    if (!is_null($_POST['Emp_CPF']) and !is_null($_POST['Emp_Cd'])) {
        $var14 = ", ".$_POST['Emp_CPF'];
        $_SESSION['var14'] = ", ".$_POST['Emp_CPF'];
    }elseif (!is_null($_POST['Emp_CPF']) and is_null($_POST['Emp_Cd'])  and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC'])){
        $var14 = $_POST['Emp_CPF'];
        $_SESSION['var14'] = $var14;
    }else{
        $var14 = "";
        $_SESSION['var14'] = $var14;
    }

    if (!is_null($_POST['EMP_EmpRepasse']) and !is_null($_POST['Emp_Cd'])) {
        $var15 = ", ".$_POST['EMP_EmpRepasse'];
        $_SESSION['var15'] = ", ".$_POST['EMP_EmpRepasse'];
    }elseif (!is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF'])){
        $var15 = $_POST['EMP_EmpRepasse'];
        $_SESSION['var15'] = $var15;
    }else{
        $var15 = "";
        $_SESSION['var15'] = $var15;
    }

    if (!is_null($_POST['Emp_Faturamento']) and !is_null($_POST['Emp_Cd'])) {
        $var16 = ", ".$_POST['Emp_Faturamento'];
        $_SESSION['var16'] = ", ".$_POST['Emp_Faturamento'];
    }elseif (!is_null($_POST['Emp_Faturamento']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse'])){
        $var16 = $_POST['Emp_Faturamento'];
        $_SESSION['var16'] = $var16;
    }else{
        $var16 = "";
        $_SESSION['var16'] = $var16;
    }

    if (!is_null($_POST['URe_Usuario']) and !is_null($_POST['Emp_Cd'])) {
        $var17 = ", ".$_POST['URe_Usuario'];
        $_SESSION['var17'] = ", ".$_POST['URe_Usuario'];
    }elseif (!is_null($_POST['URe_Usuario']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento'])){
        $var17 = $_POST['URe_Usuario'];
        $_SESSION['var17'] = $var17;
    }else{
        $var17 = "";
        $_SESSION['var17'] = $var17;
    }

    if (!is_null($_POST['C_NomUsu']) and !is_null($_POST['Emp_Cd'])) {
        $var18 = ", ".$_POST['C_NomUsu'];
        $_SESSION['var18'] = ", ".$_POST['C_NomUsu'];
    }elseif (!is_null($_POST['C_NomUsu']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario'])){
        $var18 = $_POST['C_NomUsu'];
        $_SESSION['var18'] = $var18;
    }else{
        $var18 = "";
        $_SESSION['var18'] = $var18;
    }

    if (!is_null($_POST['URe_VlMensalidade']) and !is_null($_POST['Emp_Cd'])) {
        $var19 = ", ".$_POST['URe_VlMensalidade'];
        $_SESSION['var19'] = ", ".$_POST['URe_VlMensalidade'];
    }elseif (!is_null($_POST['URe_VlMensalidade']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu'])){
        $var19 = $_POST['URe_VlMensalidade'];
        $_SESSION['var19'] = $var19;
    }else{
        $var19 = "";
        $_SESSION['var19'] = $var19;
    }

    if (!is_null($_POST['URe_VlFM']) and !is_null($_POST['Emp_Cd'])) {
        $var20 = ", ".$_POST['URe_VlFM'];
        $_SESSION['var20'] = ", ".$_POST['URe_VlFM'];
    }elseif (!is_null($_POST['URe_VlFM']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade'])){
        $var20 = $_POST['URe_VlFM'];
        $_SESSION['var20'] = $var20;
    }else{
        $var20 = "";
        $_SESSION['var20'] = $var20;
    }

    if (!is_null($_POST['URe_VlOpcional']) and !is_null($_POST['Emp_Cd'])) {
        $var21 = ", ".$_POST['URe_VlOpcional'];
        $_SESSION['var21'] = ", ".$_POST['URe_VlOpcional'];
    }elseif (!is_null($_POST['URe_VlOpcional']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM'])){
        $var21 = $_POST['URe_VlOpcional'];
        $_SESSION['var21'] = $var21;
    }else{
        $var21 = "";
        $_SESSION['var21'] = $var21;
    }

    if (!is_null($_POST['C_NASUSU']) and !is_null($_POST['Emp_Cd'])) {
        $var22 = ",".$_POST['C_NASUSU'];
        $_SESSION['var22'] = ",".$_POST['C_NASUSU'];
    }elseif (!is_null($_POST['C_NASUSU']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional'])){
        $var22 = $_POST['C_NASUSU'];
        $_SESSION['var22'] = $var22;
    }else{
        $var22 = "";
        $_SESSION['var22'] = $var22;
    }

    if (!is_null($_POST['C_CICUSU']) and !is_null($_POST['Emp_Cd'])) {
        $var23 = ", ".$_POST['C_CICUSU'];
        $_SESSION['var23'] = ", ".$_POST['C_CICUSU'];
    }elseif (!is_null($_POST['C_CICUSU']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_NASUSU'])){
        $var23 = $_POST['C_CICUSU'];
        $_SESSION['var23'] = $var23;
    }else{
        $var23 = "";
        $_SESSION['var23'] = $var23;
    }

    if (!is_null($_POST['Usu_VinculoBenef']) and !is_null($_POST['Emp_Cd'])) {
        $var24 = ", ".$_POST['Usu_VinculoBenef'];
        $_SESSION['var24'] = ", ".$_POST['Usu_VinculoBenef'];
    }elseif (!is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU'])){
        $var24 = $_POST['Usu_VinculoBenef'];
        $_SESSION['var24'] = $var24;
    }else{
        $var24 = "";
        $_SESSION['var24'] = $var24;
    }

    if (!is_null($_POST['CUS_cd']) and !is_null($_POST['Emp_Cd'])) {
        $var25 = ", ".$_POST['CUS_cd'];
        $_SESSION['var25'] = ", ".$_POST['CUS_cd'];
    }elseif (!is_null($_POST['CUS_cd']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef'])){
        $var25 = $_POST['CUS_cd'];
        $_SESSION['var25'] = $var25;
    }else{
        $var25 = "";
        $_SESSION['var25'] = $var25;
    }

    if (!is_null($_POST['CUS_ds']) and !is_null($_POST['Emp_Cd'])) {
        $var26 = ", ".$_POST['CUS_ds'];
        $_SESSION['var26'] = ", ".$_POST['CUS_ds'];
    }elseif (!is_null($_POST['CUS_ds']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd'])){
        $var26 = $_POST['CUS_ds'];
        $_SESSION['var26'] = $var26;
    }else{
        $var26 = "";
        $_SESSION['var26'] = $var26;
    }

    if (!is_null($_POST['TipoBeneficiario']) and !is_null($_POST['Emp_Cd'])) {
        $var27 = ", ".$_POST['TipoBeneficiario'];
        $_SESSION['var27'] = ", ".$_POST['TipoBeneficiario'];
    }elseif (!is_null($_POST['TipoBeneficiario']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds'])){
        $var27 = $_POST['TipoBeneficiario'];
        $_SESSION['var27'] = $var27;
    }else{
        $var27 = "";
        $_SESSION['var27'] = $var27;
    }

    if (!is_null($_POST['Cód Plano']) and !is_null($_POST['Emp_Cd'])) {
        $var28 = ", ".$_POST['Cód Plano'];
        $_SESSION['var28'] = ", ".$_POST['Cód Plano'];
    }elseif (!is_null($_POST['Cód Plano']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario'])){
        $var28 = $_POST['Cód Plano'];
        $_SESSION['var28'] = $var28;
    }else{
        $var28 = "";
        $_SESSION['var28'] = $var28;
    }

    if (!is_null($_POST['Plano']) and !is_null($_POST['Emp_Cd'])) {
        $var29 = ", ".$_POST['Plano'];
        $_SESSION['var29'] = ", ".$_POST['Plano'];
    }elseif (!is_null($_POST['Plano']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano'])){
        $var29 = $_POST['Plano'];
        $_SESSION['var29'] = $var29;
    }else{
        $var29 = "";
        $_SESSION['var29'] = $var29;
    }

    if (!is_null($_POST['Sexo']) and !is_null($_POST['Emp_Cd'])) {
        $var30 = ", ".$_POST['Sexo'];
        $_SESSION['var30'] = ", ".$_POST['Sexo'];
    }elseif (!is_null($_POST['Sexo']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano'])){
        $var30 = $_POST['Sexo'];
        $_SESSION['var30'] = $var30;
    }else{
        $var30 = "";
        $_SESSION['var30'] = $var30;
    }

    if (!is_null($_POST['Parentesco']) and !is_null($_POST['Emp_Cd'])) {
        $var31 = ", ".$_POST['Parentesco'];
        $_SESSION['var31'] = ", ".$_POST['Parentesco'];
    }elseif (!is_null($_POST['Parentesco']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano']) and is_null($_POST['Sexo'])){
        $var31 = $_POST['Parentesco'];
        $_SESSION['var31'] = $var31;
    }else{
        $var31 = "";
        $_SESSION['var31'] = $var31;
    }

    if (!is_null($_POST['ABRANGE']) and !is_null($_POST['Emp_Cd'])) {
        $var32 = ", ".$_POST['ABRANGE'];
        $_SESSION['var32'] = ", ".$_POST['ABRANGE'];
    }elseif (!is_null($_POST['ABRANGE']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano']) and is_null($_POST['Sexo']) and is_null($_POST['Parentesco'])){
        $var32 = $_POST['ABRANGE'];
        $_SESSION['var32'] = $var32;
    }else{
        $var32 = "";
        $_SESSION['var32'] = $var32;
    }

    if (!is_null($_POST['Inclusão']) and !is_null($_POST['Emp_Cd'])) {
        $var33 = ", ".$_POST['Inclusão'];
        $_SESSION['var33'] = ", ".$_POST['Inclusão'];
    }elseif (!is_null($_POST['Inclusão']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano']) and is_null($_POST['Sexo']) and is_null($_POST['Parentesco']) and is_null($_POST['ABRANGE'])){
        $var33 = $_POST['Inclusão'];
        $_SESSION['var33'] = $var33;
    }else{
        $var33 = "";
        $_SESSION['var33'] = $var33;
    }

    if (!is_null($_POST['Idade']) and !is_null($_POST['Emp_Cd'])) {
        $var34 = ", ".$_POST['Idade'];
        $_SESSION['var34'] = ", ".$_POST['Idade'];
    }elseif (!is_null($_POST['Idade']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano']) and is_null($_POST['Sexo']) and is_null($_POST['Parentesco']) and is_null($_POST['ABRANGE']) and is_null($_POST['Inclusão'])){
        $var34 = $_POST['Idade'];
        $_SESSION['var34'] = $var34;
    }else{
        $var34 = "";
        $_SESSION['var34'] = $var34;
    }

    if (!is_null($_POST['Usu_TitCobranca']) and !is_null($_POST['Emp_Cd'])) {
        $var35 = ", ".$_POST['Usu_TitCobranca'];
        $_SESSION['var35'] = ", ".$_POST['Usu_TitCobranca'];
    }elseif (!is_null($_POST['Usu_TitCobranca']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano']) and is_null($_POST['Sexo']) and is_null($_POST['Parentesco']) and is_null($_POST['ABRANGE']) and is_null($_POST['Inclusão']) and is_null($_POST['Idade'])){
        $var35 = $_POST['Usu_TitCobranca'];
        $_SESSION['var35'] = $var35;
    }else{
        $var35 = "";
        $_SESSION['var35'] = $var35;
    }

    if (!is_null($_POST['C_CODUSU']) and !is_null($_POST['Emp_Cd'])) {
        $var36 = ", ".$_POST['C_CODUSU'];
        $_SESSION['var36'] = ", ".$_POST['C_CODUSU'];
    }elseif (!is_null($_POST['C_CODUSU']) and is_null($_POST['Emp_Cd']) and is_null($_POST['Emp_Ds']) and is_null($_POST['Titular']) and is_null($_POST['Ocorrencias']) and is_null($_POST['Tipo']) and is_null($_POST['CodTitular']) and is_null($_POST['Usu_Interno']) and is_null($_POST['URe_Doc']) and is_null($_POST['NFSE']) and is_null($_POST['pg_emissao']) and is_null($_POST['DtVencto']) and is_null($_POST['BRec_DtBaixa']) and is_null($_POST['Emp_CGC']) and is_null($_POST['Emp_CPF']) and is_null($_POST['EMP_EmpRepasse']) and is_null($_POST['Emp_Faturamento']) and is_null($_POST['URe_Usuario']) and is_null($_POST['C_NomUsu']) and is_null($_POST['URe_VlMensalidade']) and is_null($_POST['URe_VlFM']) and is_null($_POST['URe_VlOpcional']) and is_null($_POST['C_CICUSU']) and is_null($_POST['Usu_VinculoBenef']) and is_null($_POST['CUS_cd']) and is_null($_POST['CUS_ds']) and is_null($_POST['TipoBeneficiario']) and is_null($_POST['Cód Plano']) and is_null($_POST['Plano']) and is_null($_POST['Sexo']) and is_null($_POST['Parentesco']) and is_null($_POST['ABRANGE']) and is_null($_POST['Inclusão']) and is_null($_POST['Idade']) and is_null($_POST['Usu_TitCobranca'])){
        $var36 = $_POST['C_CODUSU'];
        $_SESSION['var36'] = $var36;
    }else{
        $var36 = "";
        $_SESSION['var36'] = $var36;
    }

    $_SESSION['sql'] = "select ".$var1.$var2.$var3.$var4.$var5.$var6.$var7.$var8.$var9.$var10.$var11.$var12.$var13.$var14.$var17.$var18.$var19.$var20.$var21.$var22.$var23.$var24.$var25.$var26.$var27.$var28.$var29.$var30.$var31.$var32.$var33.$var34.$var35.$var36." from view_name";
    
    $select_from = sqlsrv_query($conn, $_SESSION['sql']);
    if( $_SESSION['sql'] === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    ?>
    
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <?php
                            if(!empty($var1)){
                                echo("<th>Cód Empresa / Plano</th>");
                            }if(!empty($var2)){
                                echo("<th>Empresa / Plano</th>");
                            }if(!empty($var3)){
                                echo("<th>Usuário Titular</th>");
                            }if(!empty($var4)){
                                echo("<th>Ocorrências</th>");
                            }if(!empty($var5)){
                                echo("<th>Tipo</th>");
                            }if(!empty($var6)){
                                echo("<th>Cód Titular</th>");
                            }if(!empty($var7)){
                                echo("<th>Cód Interno</th>");
                            }if(!empty($var8)){
                                echo("<th>N° Doc</th>");
                            }if(!empty($var9)){
                                echo("<th>NFSE</th>");
                            }if(!empty($var10)){
                                echo("<th>Data de Emissão</th>");
                            }if(!empty($var11)){
                                echo("<th>Data de Vencimento</th>");
                            }if(!empty($var12)){
                                echo("<th>Data de Pagamento</th>");
                            }if(!empty($var13)){
                                echo("<th>Emp CNPJ</th>");
                            }if(!empty($var14)){
                                echo("<th>Emp CPF</th>");
                            }if(!empty($var15)){
                                echo("<th>Cód Emp Faturamento</th>");
                            }if(!empty($var16)){
                                echo("<th>Emp Faturamento</th>");
                            }if(!empty($var17)){
                                echo("<th>Cód Usuário</th>");
                            }if(!empty($var18)){
                                echo("<th>Nome usuário</th>");
                            }if(!empty($var19)){
                                echo("<th>Vl Mensalidade</th>");
                            }if(!empty($var20)){
                                echo("<th>Vl Fat. Moderador</th>");
                            }if(!empty($var21)){
                                echo("<th>Vl. Opcional</th>");
                            }if(!empty($var22)){
                                echo("<th>Data de Nascimento</th>");
                            }if(!empty($var23)){
                                echo("<th>CPF</th>");
                            }if(!empty($var24)){
                                echo("<th>Tipo Vínculo</th>");
                            }if(!empty($var25)){
                                echo("<th>Cód Centro Custo</th>");
                            }if(!empty($var26)){
                                echo("<th>Centro Custo</th>");
                            }if(!empty($var27)){
                                echo("<th>Tipo Benefíciario</th>");
                            }if(!empty($var28)){
                                echo("<th>Cód Plano</th>");
                            }if(!empty($var29)){
                                echo("<th>Plano</th>");
                            }if(!empty($var30)){
                                echo("<th>Sexo</th>");
                            }if(!empty($var31)){
                                echo("<th>Parentesco</th>");
                            }if(!empty($var32)){
                                echo("<th>Abrange</th>");
                            }if(!empty($var33)){
                                echo("<th>Inclusão</th>");
                            }if(!empty($var34)){
                                echo("<th>Idade</th>");
                            }if(!empty($var35)){
                                echo("<th>Titular de Cobrança</th>");
                            }if(!empty($var36)){
                                echo("<th>Código Familia</th>");
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array($select_from, SQLSRV_FETCH_ASSOC) ) {

                        if(is_null($row['pg_emissao'])){
                            $pg_emissao = '';
                        }else{
                            $pg_emissao = date_format($row['pg_emissao'], 'd/m/Y');
                        }if(is_null($row['BRec_DtBaixa'])){
                            $BRec_DtBaixa = '';
                        }else{
                            $BRec_DtBaixa = date_format($row['BRec_DtBaixa'], 'd/m/Y');
                        }if(is_null($row['DtVencto'])){
                            $DtVencto = '';
                        }else{
                            $DtVencto = date_format($row['DtVencto'], 'd/m/Y');
                        }if(is_null($row['C_NASUSU'])){
                            $C_NASUSU = '';
                        }else{
                            $C_NASUSU = date_format($row['C_NASUSU'], 'd/m/Y');
                        }if(is_null($row['Inclusão'])){
                            $Inclusão = '';
                        }else{
                            $Inclusão = date_format($row['Inclusão'], 'd/m/Y');
                        }

                        echo "<tr>";?>
                        <?php 
                            if(!empty($var1)){
                                echo("<td>".$row['Emp_Cd'].                "</td>");
                            }if(!empty($var2)){
                                echo("<td>".$row['Emp_Ds'].                "</td>");
                            }if(!empty($var3)){
                                echo("<td>".$row['Titular'].               "</td>");
                            }if(!empty($var4)){
                                echo("<td>".$row['Ocorrencias'].           "</td>");
                            }if(!empty($var5)){
                                echo("<td>".$row['Tipo'].                  "</td>");
                            }if(!empty($var6)){
                                echo("<td>".$row['CodTitular'].            "</td>");
                            }if(!empty($var7)){
                                echo("<td>".$row['Usu_Interno'].           "</td>");
                            }if(!empty($var8)){
                                echo("<td>".$row['URe_Doc'].               "</td>");
                            }if(!empty($var9)){
                                echo("<td>".$row['NFSE'].                  "</td>");
                            }if(!empty($var10)){
                                echo("<td>".$pg_emissao.                   "</td>");
                            }if(!empty($var11)){
                                echo("<td>".$DtVencto.                     "</td>");
                            }if(!empty($var12)){
                                echo("<td>".$BRec_DtBaixa.                 "</td>");
                            }if(!empty($var13)){
                                echo("<td>".$row['Emp_CGC'].               "</td>");
                            }if(!empty($var14)){
                                echo("<td>".$row['Emp_CPF'].               "</td>");
                            }if(!empty($var15)){
                                echo("<td>".$row['EMP_EmpRepasse'].        "</td>");
                            }if(!empty($var16)){
                                echo("<td>".$row['Emp_Faturamento'].       "</td>");
                            }if(!empty($var17)){
                                echo("<td>".$row['URe_Usuario'].           "</td>");
                            }if(!empty($var18)){
                                echo("<td>".$row['C_NomUsu'].              "</td>");
                            }if(!empty($var19)){
                                echo("<td>".$row['URe_VlMensalidade'].     "</td>");
                            }if(!empty($var20)){
                                echo("<td>".$row['URe_VlFM'].              "</td>");
                            }if(!empty($var21)){
                                echo("<td>".$row['URe_VlOpcional'].        "</td>");
                            }if(!empty($var22)){
                                echo("<td>".$C_NASUSU.                     "</td>");
                            }if(!empty($var23)){
                                echo("<td>".$row['C_CICUSU'].              "</td>");
                            }if(!empty($var24)){
                                echo("<td>".$row['Usu_VinculoBenef'].      "</td>");
                            }if(!empty($var25)){
                                echo("<td>".$row['CUS_cd'].                "</td>");
                            }if(!empty($var26)){
                                echo("<td>".$row['CUS_ds'].                "</td>");
                            }if(!empty($var27)){
                                echo("<td>".$row['TipoBeneficiario'].      "</td>");
                            }if(!empty($var28)){
                                echo("<td>".$row['Cód Plano'].             "</td>");
                            }if(!empty($var29)){
                                echo("<td>".$row['Plano'].                 "</td>");
                            }if(!empty($var30)){
                                echo("<td>".$row['Sexo'].                  "</td>");
                            }if(!empty($var31)){
                                echo("<td>".$row['Parentesco'].            "</td>");
                            }if(!empty($var32)){
                                echo("<td>".$row['ABRANGE'].               "</td>");
                            }if(!empty($var33)){
                                echo("<td>".$Inclusão.                     "</td>");
                            }if(!empty($var34)){
                                echo("<td>".$row['Idade'].                 "</td>");
                            }if(!empty($var35)){
                                echo("<td>".$row['Usu_TitCobranca'].       "</td>");
                            }if(!empty($var36)){
                                echo("<td>".$row['C_CODUSU'].              "</td>");
                            }"
                        </tr>";}
                      ?>
                        </tbody>
                    </table>
                    <div class="col-12 d-flex col-md-6 order-md-1">
                        <a href="gerar_planilha.php" style="color: white;">
                            <button type="submit" class="btn btn-primary me-1 mb-1">Salvar</a></button>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php
        $arquivo = "teste.xls";
    ?>

    <?php include '../html/footer.html' ?>

