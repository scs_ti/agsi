<section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                    <div class="row">
                                        <p>
                                            <h3>Selecione os Campos que deseja Visualizar na Consulta</h3>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Emp_Cd" value="Emp_Cd" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Empresa / Plano
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Emp_Ds" value="Emp_Ds" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Empresa / Plano
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Titular" value="Titular" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Usuário Titular
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Ocorrencias" value="Ocorrencias" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Ocorrências
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Tipo" value="Tipo" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Tipo
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="CodTitular" value="CodTitular" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Titular
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Usu_Interno" value="Usu_Interno" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Interno
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="URe_Doc" value="URe_Doc" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                N° Doc
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="NFSE" value="NFSE" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                NFSE
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="pg_emissao" value="pg_emissao" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Data de Emissão
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="DtVencto" value="DtVencto" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Data de Vencimento
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="BRec_DtBaixa" value="BRec_DtBaixa" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Data de Pagamento
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Emp_CGC" value="Emp_CGC" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Emp CNPJ
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Emp_CPF" value="Emp_CPF" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Emp CPF
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="EMP_EmpRepasse" value="EMP_EmpRepasse" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Emp Faturamento
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Emp_Faturamento" value="Emp_Faturamento" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Emp Faturamento
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="URe_Usuario" value="URe_Usuario" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Usuário
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="C_NomUsu" value="C_NomUsu" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Nome usuário
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="URe_VlMensalidade" value="URe_VlMensalidade" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Vl Mensalidade
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="URe_VlFM" value="URe_VlFM" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Vl Fat. Moderador
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="URe_VlOpcional" value="URe_VlOpcional" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Vl. Opcional
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="C_NASUSU" value="C_NASUSU" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Data de Nascimento
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="C_CICUSU" value="C_CICUSU" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                CPF
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Usu_VinculoBenef" value="Usu_VinculoBenef" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Tipo Vínculo
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="CUS_cd" value="CUS_cd" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Centro Custo
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="CUS_ds" value="CUS_ds" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Centro Custo
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="TipoBeneficiario" value="TipoBeneficiario" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Tipo Benefíciario
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Cód Plano" value="Cód Plano" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Cód Plano
                                              </label>
                                            </div>  
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Plano" value="Plano" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Plano
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Sexo" value="Sexo" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Sexo
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Parentesco" value="Parentesco" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Parentesco
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="ABRANGE" value="ABRANGE" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Abrange
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Inclusão" value="Inclusão" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Inclusão
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Idade" value="Idade" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Idade
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="Usu_TitCobranca" value="Usu_TitCobranca" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Titular de Cobrança
                                              </label>
                                            </div> 
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" checked="checked" name="C_CODUSU" value="C_CODUSU" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Código Familia
                                              </label>
                                            </div> 
                                        </p>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>