<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';


$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}


$empresa_ini = sqlsrv_query($conn, 'select DISTINCT EMP_cd, EMP_ds from Empresa');
    if( 'select DISTINCT EMP_cd, EMP_ds from Empresa' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$empresa_fim = sqlsrv_query($conn, 'select DISTINCT EMP_cd, EMP_ds from Empresa');
    if( 'select DISTINCT EMP_cd, EMP_ds from Empresa' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }


$grupo_empresas = sqlsrv_query($conn, 'SELECT * FROM GrupoEmpresa');
    if( 'SELECT * FROM GrupoEmpresa' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$centro_custos = sqlsrv_query($conn, 'select * from CentroCusto cc order by 1 asc');
    if( 'select * from CentroCusto cc order by 1 asc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

sqlsrv_query($conn, "DROP VIEW view_name");
    if( "DROP VIEW view_name" === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Usuário por Fatura</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="usu_fatura_desc.php" method="POST">
                                    <div class="row">
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo Pessoa</label>
                                                <select class="form-control" name="tipo_pessoa">
                                                    <option value="F">Fisíca</option>
                                                    <option value="J">Jurídica</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo Documento</label>
                                                <select class="form-control" name="tip_doc">
                                                    <option value="NF">N. Fiscal</option>
                                                    <option value="F">Duplicata</option>
                                                    <option value="NFSE">NFSE</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>N° do Documento</label>
                                                <select id="options" name="n_doc" class="form-control" onchange="verifica2(this.value)">
                                                    <option value="Todos">Todos *</option>
                                                    <option value="=">Igual</option>
                                                    <option value=">=">Maior Igual</option>
                                                    <option value="<=">Menor Igual</option>
                                                    <option value="<>">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" name="n_doc_cod" id="input0" onkeypress="mascara(this,cod)" maxlength="19"  class="form-control" placeholder="Entre com o código" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Tipo Cobrança</label>
                                                <select class="form-control" name="tipo_cob">
                                                    <option value="Todas">Todas</option>
                                                    <option value="E">Empresa</option>
                                                    <option value="U">Usuário</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Tipo de Boleto</label>
                                                <select class="form-control" name="tipo_boleto">
                                                    <option value="Ambos">Ambos</option>
                                                    <option value=">">Co-Participação</option>
                                                    <option value="=">Mensalidade</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <select id="options" class="form-control" name="empresa" onchange="verifica_emp(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Inicial</label>
                                                <select class="form-control" id="input2" disabled name="empresa_ini_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_ini, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Final</label>
                                                <select class="form-control" id="input3" disabled name="empresa_fim_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_fim, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Grupo de Empresa</label>
                                                <select id="options" class="form-control" name="grupo_emp" onchange="grupo_empresa(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-10 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <select class="form-control" id="input5" disabled name="grupo_emp_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $grupo_empresas, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['grp_codigo'].">".$row['grp_codigo']." - ".$row['grp_descricao']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Centro de custos</label>
                                                <select class="form-control" name="centro_custo">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $centro_custos, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['CUS_cd'].">".$row['CUS_cd']." - ".$row['CUS_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Data de Emissão</label>
                                                <select id="options" class="form-control" name="data_emicao" onchange="data_emissao(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="=">Igual</option>
                                                    <option value=">=">Maior Igual</option>
                                                    <option value="<=">Menor Igual</option>
                                                    <option value="<>">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="date" id="input6" name="data_emicao_desc" class="form-control" placeholder="" >
                                            </div>
                                        </div>
                                        <p>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" name="imprimir_oco" value="S" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Imprimir ocorrências
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" name="exceto_cancelados" value="S" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Exceto Cancelados
                                              </label>
                                            </div>
                                            <div class="form-check col-md-2 col-12">
                                              <input class="form-check-input" type="checkbox" name="quebra_titu" value="Titular" id="flexCheckDefault">
                                              <label class="form-check-label" for="flexCheckDefault">
                                                Quebra por Titular
                                              </label>
                                            </div>   
                                        </p> 
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>