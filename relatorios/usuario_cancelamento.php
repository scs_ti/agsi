<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$empresa_ini = sqlsrv_query($conn, 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc');
    if( 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$empresa_fim = sqlsrv_query($conn, 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc');
    if( 'select DISTINCT EMP_cd, EMP_ds from Empresa order by 1 asc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$mot_cancelamento = sqlsrv_query($conn, 'select * from Mot_Cancelamento mc');
    if( 'select * from Mot_Cancelamento mc' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

sqlsrv_query($conn, "DROP TABLE TB_USERS");
    if( "DROP VIEW TB_USERS" === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

sqlsrv_query($conn, "DROP VIEW relat_cancelamento_usu");
    if( "DROP VIEW relat_cancelamento_usu" === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Cancelamento</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="usu_cancelamento_desc.php" method="POST">
                                    <div class="row">
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo Relatório</label>
                                                <select class="form-control" name="tipo_relatorio">
                                                    <option value="F">Analítico</option>
                                                    <option value="J">Sintético</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <select id="options" class="form-control" name="empresa" onchange="verifica_emp(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Inicial</label>
                                                <select class="form-control" id="input2" disabled name="empresa_ini_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_ini, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Final</label>
                                                <select class="form-control" id="input3" disabled name="empresa_fim_cod">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_fim, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Usuário</label>
                                                <select id="options" class="form-control" name="usu" onchange="usuexiste(this.value)">
                                                    <option value="Todos">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Código do usuário</label>
                                                <input type="text" class="form-control" name="live_search" id="live_search" disabled="desabled" autocomplete="off">
                                                <div id="search_result"></div>
                                            </div>
                                        </div>

                                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $("#live_search").keyup(function () {
                                                    var query = $(this).val();
                                                    if (query != "") {
                                                        $.ajax({
                                                            url: '../consultas/consulta_existe_usu.php',
                                                            method: 'POST',
                                                            data: {
                                                                query: query
                                                            },
                                                            success: function (data) {
                                                                $('#search_result').html(data);
                                                                $('#search_result').css('display', 'bloqued');

                                                                $("#live_search").focusout(function () {
                                                                    $('#search_result').css('display', 'none');
                                                                });
                                                                $("#live_search").focusin(function () {
                                                                    $('#search_result').css('display', 'block');
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        $('#search_result').css('display', 'none');
                                                    }
                                                });
                                            });
                                        </script>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Motivo Cancelamento</label>
                                                <select id="options" class="form-control" name="cancelamento" onchange="grupo_empresa(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Selecionar Motivo</label>
                                                <select class="form-control" id="input5" disabled name="mot_cancela">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $mot_cancelamento, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['MCAN_Cd'].">".$row['MCAN_Cd']." - ".$row['MCAN_Ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Periodo de Cancelamento</label>
                                                <select id="options" class="form-control" name="periodo" onchange="selecionadata(this.value)">
                                                    <option value="Todos">Todos</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Data inicial</label>
                                                <input type="date" class="form-control" name="cancelamento_ini" id="date1" disabled="desabled">
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Data Final</label>
                                                <input type="date" class="form-control" placeholder="" name="cancelamento_fim" id="date2" disabled="desabled">
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa Pessoa</label>
                                                <select class="form-control" name="empresa_pessoa">
                                                    <option value="Todas">Todos</option>
                                                    <option value="F">Física</option>
                                                    <option value="J">Jurídica</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo de Usuário</label>
                                                <select class="form-control" name="tipo_usuario">
                                                    <option value="Todos">Todos</option>
                                                    <option value="T">Tit. Financeiro</option>
                                                    <option value="U">Usuário</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Parentesco</label>
                                                <select class="form-control" name="parentesco">
                                                    <option value="">Todos</option>
                                                    <option value="T">Titular</option>
                                                    <option value="D">Dependente</option>
                                                    <option value="A">Agregado</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>