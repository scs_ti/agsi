<?php
	error_reporting(E_ERROR | E_PARSE);

	include '../assets/conn.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Contato</title>
	<head>
	<body>
		<?php
		// Definimos o nome do arquivo que será exportado
		$arquivo = 'relatorio_usu_fatura.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '';
		$html .= '<table border="1">';

		// Configurações da tabela
		#$html .= '<tr>';
		#$html .= '<td colspan="5">Planilha Mensagem de Contatos</tr>';
		#$html .= '</tr>';
		
		
		$html .= '<tr>';
		

                            if(!empty($_SESSION['var1'])){
                                $html .= '<td><b>Cód Empresa / Plano</b></td>';
                            }if(!empty($_SESSION['var2'])){
                                $html .= '<td><b>Empresa / Plano</b></td>';
                            }if(!empty($_SESSION['var3'])){
                                $html .= '<td><b>Usuário Titular</b></td>';
                            }if(!empty($_SESSION['var4'])){
                                $html .= '<td><b>Ocorrências</b></td>';
                            }if(!empty($_SESSION['var5'])){
                                $html .= '<td><b>Tipo</b></td>';
                            }if(!empty($_SESSION['var6'])){
                                $html .= '<td><b>Cód Titular</b></td>';
                            }if(!empty($_SESSION['var7'])){
                                $html .= '<td><b>Cód Interno</b></td>';
                            }if(!empty($_SESSION['var8'])){
                                $html .= '<td><b>N° Doc</b></td>';
                            }if(!empty($_SESSION['var9'])){
                                $html .= '<td><b>NFSE</b></td>';
                            }if(!empty($_SESSION['var10'])){
                               $html .= '<td><b>Data de Emissão</b></td>';
                            }if(!empty($_SESSION['var11'])){
                                $html .= '<td><b>Data de Vencimento</b></td>';
                            }if(!empty($_SESSION['var12'])){
                                $html .= '<td><b>Data de Pagamento</b></td>';
                            }if(!empty($_SESSION['var13'])){
                                $html .= '<td><b>Emp CNPJ</b></td>';
                            }if(!empty($_SESSION['var14'])){
                                $html .= '<td><b>Emp CPF</b></td>';
                            }if(!empty($_SESSION['var15'])){
                                $html .= '<td><b>Cód Emp Faturamento</b></td>';
                            }if(!empty($_SESSION['var16'])){
                                $html .= '<td><b>Emp Faturamento</b></td>';
                            }if(!empty($_SESSION['var17'])){
                                $html .= '<td><b>Cód Usuário</b></td>';
                            }if(!empty($_SESSION['var18'])){
                                $html .= '<td><b>Nome usuário</b></td>';
                            }if(!empty($_SESSION['var19'])){
                                $html .= '<td><b>Vl Mensalidade</b></td>';
                            }if(!empty($_SESSION['var20'])){
                                $html .= '<td><b>Vl Fat. Moderador</b></td>';
                            }if(!empty($_SESSION['var21'])){
                                $html .= '<td><b>Vl. Opcional</b></td>';
                            }if(!empty($_SESSION['var22'])){
                                $html .= '<td><b>Data de Nascimento</b></td>';
                            }if(!empty($_SESSION['var23'])){
                                $html .= '<td><b>CPF</b></td>';
                            }if(!empty($_SESSION['var24'])){
                                $html .= '<td><b>Tipo Vínculo</b></td>';
                            }if(!empty($_SESSION['var25'])){
                                $html .= '<td><b>Cód Centro Custo</b></td>';
                            }if(!empty($_SESSION['var26'])){
                                $html .= '<td><b>Centro Custo</b></td>';
                            }if(!empty($_SESSION['var27'])){
                                $html .= '<td><b>Tipo Benefíciario</b></td>';
                            }if(!empty($_SESSION['var28'])){
                                $html .= '<td><b>Cód Plano</b></td>';
                            }if(!empty($_SESSION['var29'])){
                                $html .= '<td><b>Plano</b></td>';
                            }if(!empty($_SESSION['var30'])){
                                $html .= '<td><b>Sexo</b></td>';
                            }if(!empty($_SESSION['var31'])){
                               $html .= '<td><b>Parentesco</b></td>';
                            }if(!empty($_SESSION['var32'])){
                                $html .= '<td><b>Abrange</b></td>';
                            }if(!empty($_SESSION['var33'])){
                                $html .= '<td><b>Inclusão</b></td>';
                            }if(!empty($_SESSION['var34'])){
                                $html .= '<td><b>Idade</b></td>';
                            }if(!empty($_SESSION['var35'])){
                                $html .= '<td><b>Titular de Cobrança</b></td>';
                            }if(!empty($_SESSION['var36'])){
                                $html .= '<td><b>Código Familia</b></td>';
                            }
        $html .= '</tr>';
		
		//Selecionar todos os itens da tabela 
		$select_from = sqlsrv_query($conn, $_SESSION['sql']);
	    if( $_SESSION['sql'] === false) {
	        die( print_r( sqlsrv_errors(), true) );
	    }
    	
                        while( $row = sqlsrv_fetch_array($select_from, SQLSRV_FETCH_ASSOC) ) {

                        if(is_null($row['pg_emissao'])){
                            $pg_emissao = '';
                        }else{
                            $pg_emissao = date_format($row['pg_emissao'], 'd/m/Y');
                        }if(is_null($row['BRec_DtBaixa'])){
                            $BRec_DtBaixa = '';
                        }else{
                            $BRec_DtBaixa = date_format($row['BRec_DtBaixa'], 'd/m/Y');
                        }if(is_null($row['DtVencto'])){
                            $DtVencto = '';
                        }else{
                            $DtVencto = date_format($row['DtVencto'], 'd/m/Y');
                        }if(is_null($row['C_NASUSU'])){
                            $C_NASUSU = '';
                        }else{
                            $C_NASUSU = date_format($row['C_NASUSU'], 'd/m/Y');
                        }if(is_null($row['Inclusão'])){
                            $Inclusão = '';
                        }else{
                            $Inclusão = date_format($row['Inclusão'], 'd/m/Y');
                        }

                        $html .= '<tr>';

                            if(!empty($_SESSION['var1'])){
                                $html .= "<td>".$row['Emp_Cd'].                "</td>";
                            }if(!empty($_SESSION['var2'])){
                                $html .= "<td>".$row['Emp_Ds'].                "</td>";
                            }if(!empty($_SESSION['var3'])){
                                $html .= "<td>".$row['Titular'].               "</td>";
                            }if(!empty($_SESSION['var4'])){
                                $html .= "<td>".$row['Ocorrencias'].           "</td>";
                            }if(!empty($_SESSION['var5'])){
                                $html .= "<td>".$row['Tipo'].                  "</td>";
                            }if(!empty($_SESSION['var6'])){
                                $html .= "<td>".$row['CodTitular'].            "</td>";
                            }if(!empty($_SESSION['var7'])){
                                $html .= "<td>".$row['Usu_Interno'].           "</td>";
                            }if(!empty($_SESSION['var8'])){
                                $html .= "<td>".$row['URe_Doc'].               "</td>";
                            }if(!empty($_SESSION['var9'])){
                                $html .= "<td>".$row['NFSE'].                  "</td>";
                            }if(!empty($_SESSION['var10'])){
                                $html .= "<td>".$pg_emissao.                   "</td>";
                            }if(!empty($_SESSION['var11'])){
                                $html .= "<td>".$DtVencto.                     "</td>";
                            }if(!empty($_SESSION['var12'])){
                                $html .= "<td>".$BRec_DtBaixa.                 "</td>";
                            }if(!empty($_SESSION['var13'])){
                                $html .= "<td>".$row['Emp_CGC'].               "</td>";
                            }if(!empty($_SESSION['var14'])){
                                $html .= "<td>".$row['Emp_CPF'].               "</td>";
                            }if(!empty($_SESSION['var15'])){
                                $html .= "<td>".$row['EMP_EmpRepasse'].        "</td>";
                            }if(!empty($_SESSION['var16'])){
                                $html .= "<td>".$row['Emp_Faturamento'].       "</td>";
                            }if(!empty($_SESSION['var17'])){
                                $html .= "<td>".$row['URe_Usuario'].           "</td>";
                            }if(!empty($_SESSION['var18'])){
                                $html .= "<td>".$row['C_NomUsu'].              "</td>";
                            }if(!empty($_SESSION['var19'])){
                                $html .= "<td>".$row['URe_VlMensalidade'].     "</td>";
                            }if(!empty($_SESSION['var20'])){
                                $html .= "<td>".$row['URe_VlFM'].              "</td>";
                            }if(!empty($_SESSION['var21'])){
                                $html .= "<td>".$row['URe_VlOpcional'].        "</td>";
                            }if(!empty($_SESSION['var22'])){
                                $html .= "<td>"."teste".                     "</td>";
                            }if(!empty($_SESSION['var23'])){
                                $html .= "<td>".$row['C_CICUSU'].              "</td>";
                            }if(!empty($_SESSION['var24'])){
                                $html .= "<td>".$row['Usu_VinculoBenef'].      "</td>";
                            }if(!empty($_SESSION['var25'])){
                                $html .= "<td>".$row['CUS_cd'].                "</td>";
                            }if(!empty($_SESSION['var26'])){
                                $html .= "<td>".$row['CUS_ds'].                "</td>";
                            }if(!empty($_SESSION['var27'])){
                                $html .= "<td>".$row['TipoBeneficiario'].      "</td>";
                            }if(!empty($_SESSION['var28'])){
                                $html .= "<td>".$row['Cód Plano'].             "</td>";
                            }if(!empty($_SESSION['var29'])){
                                $html .= "<td>".$row['Plano'].                 "</td>";
                            }if(!empty($_SESSION['var30'])){
                                $html .= "<td>".$row['Sexo'].                  "</td>";
                            }if(!empty($_SESSION['var31'])){
                                $html .= "<td>".$row['Parentesco'].            "</td>";
                            }if(!empty($_SESSION['var32'])){
                                $html .= "<td>".$row['ABRANGE'].               "</td>";
                            }if(!empty($_SESSION['var33'])){
                                $html .= "<td>"."teste".                     "</td>";
                            }if(!empty($_SESSION['var34'])){
                                $html .= "<td>".$row['Idade'].                 "</td>";
                            }if(!empty($_SESSION['var35'])){
                                $html .= "<td>".$row['Usu_TitCobranca'].       "</td>";
                            }if(!empty($_SESSION['var36'])){
                                $html .= "<td>".$row['C_CODUSU'].              "</td>";
                            };
                        $html .= '</tr>';
                    }
		// Configurações header para forçar o download
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
		header ("Content-Description: PHP Generated Data" );
		// Envia o conteúdo do arquivo
		echo $html;
		exit; ?>
	</body>
</html>