<?php
    error_reporting(E_ERROR | E_PARSE);
	include '../assets/conn.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Contato</title>
	<head>
	<body>
		<?php
		// Definimos o nome do arquivo que será exportado
		$arquivo = 'relatorio_usu_fatura.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '';
		$html .= '<table border="1">';

		// Configurações da tabela
		#$html .= '<tr>';
		#$html .= '<td colspan="5">Planilha Mensagem de Contatos</tr>';
		#$html .= '</tr>';
		
        if ($_SESSION['selecionar_por'] == 'P') {
        
		$html .= '<tr>';
            $html .= '<td><b>NroUsuarios</b></td>';
            $html .= '<td><b>CodEmpresa</b></td>';
            $html .= '<td><b>Empresa</b></td>';
            $html .= '<td><b>CodPlano</b></td>';
            $html .= '<td><b>Plano</b></td>';
            $html .= '<td><b>CodMotivo</b></td>';
            $html .= '<td><b>Motivo</b></td>';
        $html .= '</tr>';
		
        $mot_cancelamento = sqlsrv_query($conn, $_SESSION['SQL_FULL ']);
        if( $_SESSION['SQL_FULL '] === false) {
        die( print_r( sqlsrv_errors(), true) );
        }
		  	
                        while( $row = sqlsrv_fetch_array($mot_cancelamento, SQLSRV_FETCH_ASSOC) ) {
                        
                        $html .= '<tr>';
                            $html .= "<td>".$row['NroUsuarios'].                "</td>";
                            $html .= "<td>".$row['CodEmpresa'].                "</td>";
                            $html .= "<td>".$row['Empresa'].                "</td>";
                            $html .= "<td>".$row['CodPlano'].                "</td>";
                            $html .= "<td>".$row['Plano'].                "</td>";
                            $html .= "<td>".$row['CodMotivo'].                "</td>";
                            $html .= "<td>".$row['Motivo'].                "</td>";    
                            $html .= '</tr>';                        
                        };
                       
        }

        elseif ($_SESSION['selecionar_por'] == 'F') {
            $html .= '<tr>';
            if(strlen($_SESSION['ft1']) > 0){
                $html .= '<td><b>'.$_SESSION['ft1'].'</b></td>';
            }
            if(strlen($_SESSION['ft2']) > 0){
                $html .= '<td><b>'.$_SESSION['ft2'].'</b></td>';
            }
            if(strlen($_SESSION['ft3']) > 0){
                $html .= '<td><b>'.$_SESSION['ft3'].'</b></td>';
            }
            if(strlen($_SESSION['ft4']) > 0){
                $html .= '<td><b>'.$_SESSION['ft4'].'</b></td>';
            }
            if(strlen($_SESSION['ft5']) > 0){
                $html .= '<td><b>'.$_SESSION['ft5'].'</b></td>';
            }
            $html .= '<td><b>Total de usuários cancelados</b></td>';
        $html .= '</tr>';

        $mot_cancelamento = sqlsrv_query($conn, $_SESSION['SQL_FULL ']);
        if( $_SESSION['SQL_FULL '] === false) {
        die( print_r( sqlsrv_errors(), true) );
        }

        while( $row = sqlsrv_fetch_array($mot_cancelamento, SQLSRV_FETCH_ASSOC) ) {
                        
                        $html .= '<tr>';
                            if(strlen($_SESSION['ft1']) > 0){
                                $html .= "<td>".$row[$_SESSION['ft1']].                "</td>";
                            }
                            if(strlen($_SESSION['ft2']) > 0){
                                $html .= "<td>".$row[$_SESSION['ft2']].                "</td>"; 
                            }
                            if(strlen($_SESSION['ft3']) > 0){
                                $html .= "<td>".$row[$_SESSION['ft3']].                "</td>";
                            }
                            if(strlen($_SESSION['ft4']) > 0){
                                $html .= "<td>".$row[$_SESSION['ft4']].                "</td>";
                            }
                            if(strlen($_SESSION['ft5']) > 0){
                                $html .= "<td>".$row[$_SESSION['ft5']].                "</td>";
                            }  
                            $html .= "<td>".$row['Total de usuários cancelados'].                "</td>";  
                            $html .= '</tr>';                        
                        };
        }

		// Configurações header para forçar o download
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
		header ("Content-Description: PHP Generated Data" );
		// Envia o conteúdo do arquivo
		echo $html;
		exit; ?>
	</body>
</html>