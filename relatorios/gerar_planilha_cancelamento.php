<?php
	error_reporting(E_ERROR | E_PARSE);

	include '../assets/conn.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Contato</title>
	<head>
	<body>
		<?php
		// Definimos o nome do arquivo que será exportado
		$arquivo = 'relatorio_usu_fatura.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '';
		$html .= '<table border="1">';

		// Configurações da tabela
		#$html .= '<tr>';
		#$html .= '<td colspan="5">Planilha Mensagem de Contatos</tr>';
		#$html .= '</tr>';
		
		
		$html .= '<tr>';
		

                            if(!empty($_SESSION['var1'])){
                                $html .= '<td><b>Cod Titular</b></td>';
                            }if(!empty($_SESSION['var2'])){
                                $html .= '<td><b>Titular</b></td>';
                            }if(!empty($_SESSION['var3'])){
                                $html .= '<td><b>Cód.Motivo</b></td>';
                            }if(!empty($_SESSION['var4'])){
                                $html .= '<td><b>Descr. Motivo</b></td>';
                            }if(!empty($_SESSION['var5'])){
                                $html .= '<td><b>Cód.Empresa</b></td>';
                            }if(!empty($_SESSION['var6'])){
                                $html .= '<td><b>Descr. Empresa</b></td>';
                            }if(!empty($_SESSION['var7'])){
                                $html .= '<td><b>Cód.Abrange</b></td>';
                            }if(!empty($_SESSION['var8'])){
                                $html .= '<td><b>Código</b></td>';
                            }if(!empty($_SESSION['var9'])){
                                $html .= '<td><b>Nome</b></td>';
                            }if(!empty($_SESSION['var10'])){
                               $html .= '<td><b>Dt.Término</b></td>';
                            }if(!empty($_SESSION['var11'])){
                                $html .= '<td><b>Fone</b></td>';
                            }if(!empty($_SESSION['var12'])){
                                $html .= '<td><b>Endereço</b></td>';
                            }if(!empty($_SESSION['var13'])){
                                $html .= '<td><b>Cidade</b></td>';
                            }if(!empty($_SESSION['var14'])){
                                $html .= '<td><b>Dt Inclusão</b></td>';
                            }if(!empty($_SESSION['var15'])){
                                $html .= '<td><b>Vendedor</b></td>';
                            }if(!empty($_SESSION['var16'])){
                                $html .= '<td><b>Nº Meses</b></td>';
                            }if(!empty($_SESSION['var17'])){
                                $html .= '<td><b>Nº Parcelas Pagas</b></td>';
                            }if(!empty($_SESSION['var18'])){
                                $html .= '<td><b>Nº Parcelas Aberto</b></td>';
                            }if(!empty($_SESSION['var19'])){
                                $html .= '<td><b>Nº Parcelas Canceladas</b></td>';
                            }if(!empty($_SESSION['var20'])){
                                $html .= '<td><b>Valor</b></td>';
                            }if(!empty($_SESSION['var21'])){
                                $html .= '<td><b>Tipo</b></td>';
                            }if(!empty($_SESSION['var22'])){
                                $html .= '<td><b>Cód. Op. Repasse</b></td>';
                            }if(!empty($_SESSION['var23'])){
                                $html .= '<td><b>Op. Repasse</b></td>';
                            }if(!empty($_SESSION['var24'])){
                                $html .= '<td><b>Op. Cancelamento</b></td>';
                            }
        $html .= '</tr>';
		
		//Selecionar todos os itens da tabela 
		$select_from = sqlsrv_query($conn, $_SESSION['usu_cancelamento']);
	    if( $_SESSION['usu_cancelamento'] === false) {
	        die( print_r( sqlsrv_errors(), true) );
	    }
    	
                        while( $row = sqlsrv_fetch_array($select_from, SQLSRV_FETCH_ASSOC) ) {

                        if(is_null($row['dt_termino'])){
                            $dt_termino = '';
                        }else{
                            $dt_termino = date_format($row['dt_termino'], 'd/m/Y');
                        }

                        if(is_null($row['Dt Inclusão'])){
                            $dt_inclusao = '';
                        }else{
                            $dt_inclusao = date_format($row['dt_inclusao'], 'd/m/Y');
                        }

                        $html .= '<tr>';

                            if(!empty($_SESSION['var1'])){
                                $html .= "<td>".$row['cod_titular'].                "</td>";
                            }if(!empty($_SESSION['var2'])){
                                $html .= "<td>".$row['titular'].                "</td>";
                            }if(!empty($_SESSION['var3'])){
                                $html .= "<td>".$row['cod_motivo'].               "</td>";
                            }if(!empty($_SESSION['var4'])){
                                $html .= "<td>".$row['descr_motivo'].           "</td>";
                            }if(!empty($_SESSION['var5'])){
                                $html .= "<td>".$row['cod_empresa'].                  "</td>";
                            }if(!empty($_SESSION['var6'])){
                                $html .= "<td>".$row['descr_empresa'].            "</td>";
                            }if(!empty($_SESSION['var7'])){
                                $html .= "<td>".$row['cod_abrange'].           "</td>";
                            }if(!empty($_SESSION['var8'])){
                                $html .= "<td>".$row['codigo'].               "</td>";
                            }if(!empty($_SESSION['var9'])){
                                $html .= "<td>".$row['nome'].                  "</td>";
                            }if(!empty($_SESSION['var10'])){
                                $html .= "<td>".$dt_termino.                   "</td>";
                            }if(!empty($_SESSION['var11'])){
                                $html .= "<td>".$row['fone'].                  "</td>";
                            }if(!empty($_SESSION['var12'])){
                                $html .= "<td>".$row['Endereco'].                  "</td>";
                            }if(!empty($_SESSION['var13'])){
                                $html .= "<td>".$row['cidade'].               "</td>";
                            }if(!empty($_SESSION['var14'])){
                                $html .= "<td>".$dt_inclusao.               "</td>";
                            }if(!empty($_SESSION['var15'])){
                                $html .= "<td>".$row['vendedor'].        "</td>";
                            }if(!empty($_SESSION['var16'])){
                                $html .= "<td>".$row['n_meses'].       "</td>";
                            }if(!empty($_SESSION['var17'])){
                                $html .= "<td>".$row['n_parcelas_Pagas'].           "</td>";
                            }if(!empty($_SESSION['var18'])){
                                $html .= "<td>".$row['n_parcelas_aberto'].              "</td>";
                            }if(!empty($_SESSION['var19'])){
                                $html .= "<td>".$row['n_parcelas_canceladas'].     "</td>";
                            }if(!empty($_SESSION['var20'])){
                                $html .= "<td>".$row['valor'].              "</td>";
                            }if(!empty($_SESSION['var21'])){
                                $html .= "<td>".$row['tipo'].        "</td>";
                            }if(!empty($_SESSION['var22'])){
                                $html .= "<td>".$row['cod_op_repasse'].        "</td>";
                            }if(!empty($_SESSION['var23'])){
                                $html .= "<td>".$row['op_repasse'].              "</td>";
                            }if(!empty($_SESSION['var24'])){
                                $html .= "<td>".$row['op_cancelamento'].      "</td>";
                            };
                        $html .= '</tr>';
                    }
		// Configurações header para forçar o download
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
		header ("Content-Description: PHP Generated Data" );
		// Envia o conteúdo do arquivo
		echo $html;
		exit; ?>
	</body>
</html>