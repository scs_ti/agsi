<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<?php
error_reporting(E_ERROR | E_PARSE);
include '../assets/conn.php';

$dia = date("d");
$mes = date("m");
$ano = date("y");
$data = $mes."/".$dia."/20".$ano;

if ($_POST['tipo_relatorio'] != 'A') {
    $sql = "SELECT Emp_cd 'Código Empresa',                                    
                   Emp_ds 'Desc Empresa',                                      
                   Sum(Nullif( FM_Qtde, 1)) 'Quantidade',                      
                   Sum(Nullif(FM.FM_Valor,0)) / Sum(Nullif(FM.FM_Qtde, 1)) Unit, 
                   SUM(IsNull( FM_Valor, 0)) Total                               
            INTO TempRelFM_".$_SESSION['usuario']."
            FROM FatorModerador FM                                                                    
               INNER JOIN Guia g ON FM_Guia = g.Gui_cd                                                
               INNER JOIN t_ArqUsu u ON Gui_Usuario = c_CtrUsu                                        
               LEFT OUTER JOIN ItensGuia it ON ( g.Gui_cd=it.Guitem_guia and it.Guitem_Seq = FM_Seq ) 
               INNER JOIN Empresa ON u.c_EmpUsu = Emp_cd                                              
               INNER JOIN Amb ON FM_Amb = c_TipAmb and FM_Procedimento = c_Codamb                     
               LEFT JOIN Fornecedor f ON Guitem_Prestador = f.Fornecedor_cd                           
               LEFT JOIN Fornecedor fLocal ON it.Guitem_Local = fLocal.Fornecedor_cd                  
               LEFT JOIN Especialidade ON g.Gui_Especialidade = Especialidade_cd                      
               LEFT JOIN Proc_Guia ON g.Gui_Tipo = ProcGuia_cd                                        
               LEFT JOIN t_ArqUsu TIT ON LEFT(TIT.c_CtrUsu,13) = LEFT((CASE WHEN U.Usu_TitCobranca is Null THEN U.c_CtrUsu ELSE U.Usu_TitCobranca END),13) and TIT.C_PARUSU = 'T' 
               LEFT JOIN T_ARQUSU TitCob ON TitCob.C_CTRUSU = u.Usu_TitCobranca                       
               LEFT JOIN Contapagar CP ON CP.PG_CD = FM.FM_Duplicata                                  
                         and CP.PG_sequencia = FM.FM_SeqDup and CP.Pg_ContaCrr = FM.FM_ContaCrrDup       
            WHERE FM_Guia is not null  ";
}

if ($_POST['tipo_relatorio'] = 'A') {
    $sql = "SELECT Emp_cd 'Codigo Empresa', Emp_ds 'Desc Empresa',                   
       TIT.C_ctrusu as 'Cod.Titular',                                    
       TIT.c_NomUsu as 'Nome Titular',                                   
      u.C_CtrUsu 'Código',                                               
      dbo.fDataVencto(CP.PG_Vencto,CP.Pg_Mes) Vencimento,                
      SUBSTRING( u.c_NomUsu,1,30 ) 'Nome Usuário',                       
      u.Usu_Interno 'Código Interno',                                    
      FM_Amb 'AMB',                                                      
      FM_Procedimento 'FM Procedimento',                                 
      SUBSTRING( c_DescAmb,1,40 ) 'Descrição AMB',                       
      g.Gui_Atendto 'Atendimento',                                       
      g.Gui_Liberacao 'Liberação',                                       
      g.Gui_Emissao 'Emissão',                                           
      g.Gui_Especialidade 'Especialidade',                               
      SUBSTRING( Especialidade_ds,1,20 ) 'Especialidade Desc',           
      g.Gui_Tipo 'Tipo Guia',                                            
      SUBSTRING( ProcGuia_ds,1,15 ) 'Proc Desc',                         
      g.Gui_cd 'Cod. Guia', f.Fornecedor_ds 'Desc Fornecedor',           
      CONVERT(MONEY,IsNull(FM_Valor,0)) 'Valor Fator',                   
      IsNull(FM_Qtde, 1) 'Quantidade',                                   
      CONVERT(MONEY,IsNull(FM_Valor,0)) Total,                           
      G.Gui_Operator AS Operador,                                        
      G.Gui_OBS AS OBS,                                                  
      u.Usu_TitCobranca AS 'Cód. Titular de Cobrança',                   
      TitCob.C_NOMUSU AS 'Nome Titular de Cobrança',                     
      TIT.c_CicUsu as 'CPF Titular',                                     
      ( SELECT TOP 1 gfm_ds                                              
        FROM GrupoFMItens                                                
          INNER JOIN GRUPOFM ON gfm_cd = gfi_GrupoFM                     
        WHERE gfi_Versao = c_TipAmb                                      
              and gfi_Procedimento = c_CodAMB ) 'Grupo FM',              
        FM_DtPagto 'Dt.Pagto',                                           
        FM_Duplicata 'Duplicata',                                        
       it.Guitem_Local 'Cód. Local Realizacao',                          
       fLocal.Fornecedor_ds 'Local Realizacao'                           
INTO TempRelFM_".$_SESSION['usuario']."
FROM FatorModerador FM                                                                    
   INNER JOIN Guia g ON FM_Guia = g.Gui_cd                                                
   INNER JOIN t_ArqUsu u ON Gui_Usuario = c_CtrUsu                                        
   LEFT OUTER JOIN ItensGuia it ON ( g.Gui_cd=it.Guitem_guia and it.Guitem_Seq = FM_Seq ) 
   INNER JOIN Empresa ON u.c_EmpUsu = Emp_cd                                              
   INNER JOIN Amb ON FM_Amb = c_TipAmb and FM_Procedimento = c_Codamb                     
   LEFT JOIN Fornecedor f ON Guitem_Prestador = f.Fornecedor_cd                           
   LEFT JOIN Fornecedor fLocal ON it.Guitem_Local = fLocal.Fornecedor_cd                  
   LEFT JOIN Especialidade ON g.Gui_Especialidade = Especialidade_cd                      
   LEFT JOIN Proc_Guia ON g.Gui_Tipo = ProcGuia_cd                                        
   LEFT JOIN t_ArqUsu TIT ON LEFT(TIT.c_CtrUsu,13) = LEFT((CASE WHEN U.Usu_TitCobranca is Null THEN U.c_CtrUsu ELSE U.Usu_TitCobranca END),13) and TIT.C_PARUSU = 'T' 
   LEFT JOIN T_ARQUSU TitCob ON TitCob.C_CTRUSU = u.Usu_TitCobranca                       
   LEFT JOIN Contapagar CP ON CP.PG_CD = FM.FM_Duplicata                                  
             and CP.PG_sequencia = FM.FM_SeqDup and CP.Pg_ContaCrr = FM.FM_ContaCrrDup       
WHERE FM_Guia is not null ";
}

    if ($_POST['tipo_cobranca'] != "todas") {
        $sql .= "and FM_forma = '".$_POST['tipo_cobranca']."' ";
    }

    if ($_POST['status_fm'] != "todas") {
        $sql .= "And FM_Status = '".$_POST['status_fm']."' ";
    }

    if ($_POST['empresa_vl'] != "Todas" and strlen($_POST['empresa_ini_cod']) >= 1) {
        $sql .= "and u.c_EmpUsu BETWEEN ".$_POST['empresa_ini_cod']." and ".$_POST['empresa_fim_cod']." ";
    }

    if ($_POST['empresa'] != "Todas" and strlen($_POST['emp_cod_grup']) >= 1) {
        $sql .= "AND Emp_cd in (SELECT gpi_empresa FROM GrupoEmpresaItens WHERE gpi_grupo = ".$_POST['emp_cod_grup'].") ";
    }

    if ($_POST['usu'] != "Todos" and strlen($_POST['nome_usu']) >= 1) {
        $sql .=  "and ( U.C_NOMUSU LIKE '%".$_POST['nome_usu']."%') ";
    }

    if ($_POST['usu'] != "Todos" and strlen($_POST['live_search']) >= 1) {
        $sql .=  "and ( U.C_CTRUSU LIKE '%".$_POST['live_search']."%') ";
    }

    if (strlen($_SESSION['valor_atual']) >= 1) {
        $sql .= "And Proc_Guia.ProcGuia_cd in(".$_SESSION['valor_atual'].") ";
    }

    if (strlen($_POST['regiao']) >= 1) {
        $sql .= "and Emp_Regiao = ".$_POST['regiao']." ";
    }

    if ($_POST['dia_vencimento_usu'] != "Todos") {
        $sql .= "and u.c_vctusu BETWEEN '".$_POST['dia_ini']."' and '".$_POST['dia_fim']."' ";
    }

    if ($_POST['tipo_empresa'] != "todas") {
        $sql .= "and Emp_Pessoa = '".$_POST['tipo_empresa']."' ";
    }

    $lib_guia_ini = explode("-", $_POST['ldti']);
    $lib_guia_fim = explode("-", $_POST['ldtf']);
    $lib_guia = $lib_guia_ini[1]."/".$lib_guia_ini[2]."/".$lib_guia_ini[0];
    $lib_guia_end = $lib_guia_fim[1]."/".$lib_guia_fim[2]."/".$lib_guia_fim[0];

    if (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) >= 1 and strlen($_POST['ldtf']) >= 1){
        $sql .= "and g.Gui_Liberacao BETWEEN '".$lib_guia."' and '".$lib_guia_end."' ";
    }elseif (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) >= 1 and strlen($_POST['ldtf']) <= 0){
        $sql .= "and g.Gui_Liberacao BETWEEN '".$lib_guia."' and '12/30/1899' ";
    }elseif (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) <= 0 and strlen($_POST['ldtf']) >= 1){
        $sql .= "and g.Gui_Liberacao BETWEEN '12/30/1899' and '".$lib_guia_end."' ";
    }elseif (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) <= 0 and strlen($_POST['ldtf']) <= 0){
        $sql .= "and g.Gui_Liberacao BETWEEN '12/30/1899' and '12/30/1899' ";
    }

    $dia_guia_ini = explode("-", $_POST['emgi']);
    $dia_guia_fim = explode("-", $_POST['emgf']);
    $dia_guia = $dia_guia_ini[1]."/".$dia_guia_ini[2]."/".$dia_guia_ini[0];
    $dia_guia_end = $dia_guia_fim[1]."/".$dia_guia_fim[2]."/".$dia_guia_fim[0];

    if (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) >= 1 and strlen($_POST['emgf']) >= 1){
        $sql .= "and Gui_Emissao BETWEEN '".$dia_guia."' and '".$dia_guia_end."' ";
    }elseif (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) >= 1 and strlen($_POST['emgf']) <= 0){
        $sql .= "and Gui_Emissao BETWEEN '".$dia_guia."' and '12/30/1899' ";
    }elseif (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) <= 0 and strlen($_POST['emgf']) >= 1){
        $sql .= "and Gui_Emissao BETWEEN '12/30/1899' and '".$dia_guia_end."' ";
    }elseif (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) <= 0 and strlen($_POST['emgf']) <= 0){
        $sql .= "and Gui_Emissao BETWEEN '12/30/1899' and '12/30/1899' ";
    }

    $fechamento_ini = explode("-", $_POST['fechamento_ini']);
    $fechamento_fim = explode("-", $_POST['fechamento_fim']);
    $fec_ini = $fechamento_ini[1]."/".$fechamento_ini[2]."/".$fechamento_ini[0];
    $fec_fim = $fechamento_fim[1]."/".$fechamento_fim[2]."/".$fechamento_fim[0];

    if (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) >= 1 and strlen($_POST['fechamento_fim']) >= 1){
        $sql .= "and FM_DtPagto BETWEEN '".$fec_ini."' and '".$fec_fim."' ";
    }elseif (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) >= 1 and strlen($_POST['fechamento_fim']) <= 0){
        $sql .= "and FM_DtPagto BETWEEN '".$fec_ini."' and '12/30/1899' ";
    }elseif (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) <= 0 and strlen($_POST['fechamento_fim']) >= 1){
        $sql .= "and FM_DtPagto BETWEEN '12/30/1899' and '".$fec_fim."' ";
    }elseif (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) <= 0 and strlen($_POST['fechamento_fim']) <= 0){
        $sql .= "and FM_DtPagto BETWEEN '12/30/1899' and '12/30/1899' ";
    }

    if ($_POST['st_usuario'] == "A") {
        $sql .=  "and (dbo.fStatusUsuario(u.C_IncUsu, u.C_DteUsu, '".$data."') = '".$_POST['st_usuario']."') ";
    }elseif ($_POST['st_usuario'] == "C") {
        $sql .= "and u.c_IncUSU <= '".$data."' and  ( u.c_dteUSU <= '".$data."') ";
    }elseif ($_POST['st_usuario'] == "todas") {
        $sql .= " ";
    }

    if (strlen($_POST['operator']) >= 1) {
        $sql .= "and g.Gui_operator = '".$_POST['operator']."' ";
    }

    if ($_POST['tipo_co'] != "Todas"){
        $sql .= "AND Emp_TipoCobranca = '".$_POST['tipo_co']."' ";
    }

    if ($_POST['fm_vinculada'] = "S") {
        $sql .= "AND FM_Duplicata IS NOT NULL ";
    }elseif ($_POST['fm_vinculada'] = "N") {
        $sql .= "AND FM_Duplicata IS NULL ";
    }
    
    if (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) >= 1 and strlen($_POST['canc_fim']) >= 1){
        $sql .= "and U.c_DteUsu between '".$_POST['canc_ini']."' and '".$_POST['canc_fim']."' ";
    }elseif (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) >= 1 and strlen($_POST['canc_fim']) <= 0){
        $sql .= "and U.c_DteUsu between '".$_POST['canc_ini']."' and '1899-12-30' ";
    }elseif (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) <= 0 and strlen($_POST['canc_fim']) >= 1){
        $sql .= "and U.c_DteUsu between '1899-12-30' and '".$_POST['canc_fim']."' ";
    }elseif (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) <= 0 and strlen($_POST['canc_fim']) <= 0){
        $sql .= "and U.c_DteUsu between '1899-12-30' and '1899-12-30' ";
    }

    if (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) >= 1 and strlen($_POST['movi_can_fim']) >= 1){
        $sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '".$_POST['movi_can_ini']."' and '".$_POST['movi_can_fim']."') > 0 ";
    }elseif (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) >= 1 and strlen($_POST['movi_can_fim']) <= 0){
        $sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '".$_POST['movi_can_ini']."' and '1899-12-30') > 0 ";
    }elseif (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) <= 0 and strlen($_POST['movi_can_fim']) >= 1){
        $sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '1899-12-30' and '".$_POST['movi_can_fim']."') > 0 ";
    }elseif (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) <= 0 and strlen($_POST['movi_can_fim']) <= 0){
        $sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '1899-12-30' and '1899-12-30') > 0 ";
    }   


if ($_POST['tipo_relatorio'] != 'A') {
    if ($_POST['scales'] == 'on' and $_POST['horns'] == 'on') {
        $sql .= "GROUP BY Emp_cd, Emp_ds, FM.FM_Guia, FM.FM_Procedimento 
                select [Código Empresa],      
                [Desc Empresa],            
                SUM(Quantidade) Quantidade,
                Unit, 
                SUM(Total) Total
                FROM TempRelFM_".$_SESSION['usuario']."
                GROUP BY [Código Empresa]     
                              ,[Desc Empresa]          
                          ,Unit                  
                ORDER BY 1";
    }elseif ($_POST['tipo_relatorio'] != 'A' and $_POST['horns'] == 'on' and $_POST['scales'] != 'on' ) {
        $sql .= "GROUP BY Emp_cd, Emp_ds, FM.FM_Guia, FM.FM_Procedimento 
                select [Código Empresa],      
                [Desc Empresa],            
                SUM(Quantidade) Quantidade,
                0.0 Unit, 
                SUM(Total) Total
                FROM TempRelFM_".$_SESSION['usuario']."
                GROUP BY [Código Empresa]     
                              ,[Desc Empresa]          
                          ,Unit                  
                ORDER BY 1";
    }elseif ($_POST['tipo_relatorio'] != 'A' and $_POST['horns'] != 'on' and $_POST['scales'] == 'on' ) {
        $sql .= "GROUP BY Emp_cd, Emp_ds, FM.FM_Guia, FM.FM_Procedimento 
                select [Código Empresa],      
                [Desc Empresa],            
                SUM(Quantidade) Quantidade,
                Unit, 
                SUM(Total) Total
                FROM TempRelFM_".$_SESSION['usuario']."
                GROUP BY [Código Empresa]     
                              ,[Desc Empresa]          
                          ,Unit                  
                ORDER BY 1";
    }
}

if ($_POST['tipo_relatorio'] = 'A') {
    $sql .= "ORDER BY Emp_cd, u.c_NomUsu";
}

sqlsrv_query($conn, "IF object_id('tempdb.. TempRelFM_".$_SESSION['valor_atual']."') IS NOT NULL DROP TABLE TempRelFM_".$_SESSION['valor_atual']."");

$sql_full = sqlsrv_query($conn, $sql);
if( $sql === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$select_sql = sqlsrv_query($conn, "select * FROM TempRelFM_".$_SESSION['usuario']."");
if ("select * FROM TempRelFM_".$_SESSION['usuario']."" === false) {
    die( print_r( sqlsrv_errors(), true) ); 
}

?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="fator_moderado_empresa.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Relatório - Fator Moderado</h3>
    </div>
    <br>

    <?php include 'list_form_fat_mod.php';

#    
    if (!is_null($_POST['Cd_Empresa'])) {
        $var1 = $_POST['Cd_Empresa'];
    }else{
        $var1 = "";
    }
#
    if (!is_null($_POST['Desc_Empresa']) and !is_null($_POST['Cd_Empresa'])) {
        $var2 = ", ".$_POST['Desc_Empresa'];
    }elseif (!is_null($_POST['Desc_Empresa']) and is_null($_POST['Cd_Empresa'])){
        $var2 = $_POST['Desc_Empresa'];
    }else{
        $var2 = "";
    }
#
    if (!is_null($_POST['Desc_Empresa']) and !is_null($_POST['Cd_Empresa'])) {
        $var3 = ", ".$_POST['Desc_Empresa'];
    }elseif (!is_null($_POST['Desc_Empresa']) and is_null($_POST['Cd_Empresa'])){
        $var3 = $_POST['Cod_Titular'];
    }else{
        $var3 = "";
    }

    if (!is_null($_POST['Nome_Titular']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) )) {
        $var4 = ", ".$_POST['Nome_Titular'];
    }elseif (!is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa'])){
        $var4 = $_POST['Nome_Titular'];
    }else{
        $var4 = "";
    }

    if (!is_null($_POST['Codigo']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) )) {
        $var5 = ", ".$_POST['Codigo'];
    }elseif (!is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa'])){
        $var5 = $_POST['Codigo'];
    }else{
        $var5 = "";
    }

    if (!is_null($_POST['Vencimento']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) )) {
        $var6 = ", ".$_POST['Vencimento'];
    }elseif (!is_null($_POST['Vencimento']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa'])){
        $var6 = $_POST['Vencimento'];
    }else{
        $var6 = "";
    }

    if (!is_null($_POST['Nome_Usuario']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) )) {
        $var7 = ", ".$_POST['Nome_Usuario'];
    }elseif (!is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) ){
        $var7 = $_POST['Nome_Usuario'];
    }else{
        $var7 = "";
    }

    if (!is_null($_POST['Codigo_Interno']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) )) {
        $var8 = ", ".$_POST['Codigo_Interno'];
    }elseif (!is_null($_POST['Codigo_Interno']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario'])){
        $var8 = $_POST['Codigo_Interno'];
    }else{
        $var8 = "";
    }
#
    if (!is_null($_POST['AMB']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']))) {
        $var9 = ", ".$_POST['AMB'];
    }elseif (!is_null($_POST['AMB']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno'])){
        $var9 = $_POST['AMB'];
    }else{
        $var9 = "";
    }
#
    if (!is_null($_POST['FM_Procedimento']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or 
        !is_null($_POST['AMB']) )) {
        $var10 = ", ".$_POST['FM_Procedimento'];
    }elseif (!is_null($_POST['FM_Procedimento']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB'])){
        $var10 = $_POST['FM_Procedimento'];
    }else{
        $var10 = "";
    }
#
    if (!is_null($_POST['Descricao_AMB']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) )) {
        $var11 = ", ".$_POST['Descricao_AMB'];
    }elseif (!is_null($_POST['Descricao_AMB']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento'])){
        $var11 = $_POST['Descricao_AMB'];
    }else{
        $var11 = "";
    }
#
    if (!is_null($_POST['Atendimento']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) )) {
        $var12 = ", ".$_POST['Atendimento'];
    }elseif (!is_null($_POST['Atendimento']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB'])){
        $var12 = $_POST['Atendimento'];
    }else{
        $var12 = "";
    }
#
    if (!is_null($_POST['Liberacao']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) )) {
        $var13 = ", ".$_POST['Liberacao'];
    }elseif (!is_null($_POST['Liberacao']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento'])){
        $var13 = $_POST['Liberacao'];
    }else{
        $var13 = "";
    }
#
    if (!is_null($_POST['Emissao']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) )) {
        $var14 = ", ".$_POST['Emissao'];
    }elseif (!is_null($_POST['Emissao']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) ){
        $var14 = $_POST['Emissao'];
    }else{
        $var14 = "";
    }
#
    if (!is_null($_POST['Especialidade']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) )) {
        $var15 = ", ".$_POST['Especialidade'];
    }elseif (!is_null($_POST['Especialidade']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) ){
        $var15 = $_POST['Especialidade'];
    }else{
        $var15 = "";
    }
#
    if (!is_null($_POST['Especialidade_Desc']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) )) {
        $var16 = ", ".$_POST['Especialidade_Desc'];
    }elseif (!is_null($_POST['Especialidade_Desc']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and 
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) ){
        $var16 = $_POST['Especialidade_Desc'];
    }else{
        $var16 = "";
    }
#
    if (!is_null($_POST['Tipo_Guia']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) )) {
        $var17 = ", ".$_POST['Tipo_Guia'];
    }elseif (!is_null($_POST['Tipo_Guia']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc'])){
        $var17 = $_POST['Tipo_Guia'];
    }else{
        $var17 = "";
    }
#
    if (!is_null($_POST['Proc_Desc']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) )) {
        $var18 = ", ".$_POST['Proc_Desc'];
    }elseif (!is_null($_POST['Proc_Desc']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia'])){
        $var18 = $_POST['Proc_Desc'];
    }else{
        $var18 = "";
    }
#
    if (!is_null($_POST['Cod_Guia']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) )) {
        $var19 = ", ".$_POST['Cod_Guia'];
    }elseif (!is_null($_POST['Cod_Guia']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc'])){
        $var19 = $_POST['Cod_Guia'];
    }else{
        $var19 = "";
    }
#
    if (!is_null($_POST['Desc_Fornecedor']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) )) {
        $var20 = ", ".$_POST['Desc_Fornecedor'];
    }elseif (!is_null($_POST['Desc_Fornecedor']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia'])){
        $var20 = $_POST['Desc_Fornecedor'];
    }else{
        $var20 = "";
    }
#
    if (!is_null($_POST['Valor_Fator']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) )) {
        $var21 = ", ".$_POST['Valor_Fator'];
    }elseif (!is_null($_POST['Valor_Fator']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])){
        $var21 = $_POST['Valor_Fator'];
    }else{
        $var21 = "";
    }
#
    if (!is_null($_POST['Quantidade']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) )) {
        $var22 = ", ".$_POST['Quantidade'];
    }elseif (!is_null($_POST['Quantidade']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) ){
        $var22 = $_POST['Quantidade'];
    }else{
        $var22 = "";
    }
#
    if (!is_null($_POST['Total']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) )) {
        $var23 = ", ".$_POST['Total'];
    }elseif (!is_null($_POST['Total']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) ){
        $var23 = $_POST['Total'];
    }else{
        $var23 = "";
    }
#
    if (!is_null($_POST['Operador']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']))) {
        $var24 = ", ".$_POST['Operador'];
    }elseif (!is_null($_POST['Operador']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) ){
        $var24 = $_POST['Operador'];
    }else{
        $var24 = "";
    }
#
    if (!is_null($_POST['OBS']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) )) {
        $var25 = ", ".$_POST['OBS'];
    }elseif (!is_null($_POST['OBS']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador'])){
        $var25 = $_POST['OBS'];
    }else{
        $var25 = "";
    }
#
    if (!is_null($_POST['Cd_Tit_Cob']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) )) {
        $var26 = ", ".$_POST['Cd_Tit_Cob'];
    }elseif (!is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) ){
        $var26 = $_POST['Cd_Tit_Cob'];
    }else{
        $var26 = "";
    }
#
    if (!is_null($_POST['Nm_Tit_Cob']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) )) {
        $var27 = ", ".$_POST['Nm_Tit_Cob'];
    }elseif (!is_null($_POST['Nm_Tit_Cob']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) ){
        $var27 = $_POST['Nm_Tit_Cob'];
    }else{
        $var27 = "";
    }
#
    if (!is_null($_POST['CPF_titular']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) or !is_null($_POST['Nm_Tit_Cob']) )) {
        $var28 = ", ".$_POST['CPF_titular'];
    }elseif (!is_null($_POST['CPF_titular']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Nm_Tit_Cob'])){
        $var28 = $_POST['CPF_titular'];
    }else{
        $var28 = "";
    }
#
    if (!is_null($_POST['Grupo_FM']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) or !is_null($_POST['Nm_Tit_Cob']) or !is_null($_POST['CPF_titular']) )) {
        $var29 = ", ".$_POST['Grupo_FM'];
    }elseif (!is_null($_POST['Grupo_FM']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Nm_Tit_Cob']) and is_null($_POST['CPF_titular'])){
        $var29 = $_POST['Grupo_FM'];
    }else{
        $var29 = "";
    }
#
    if (!is_null($_POST['Dt_Pagto']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) or !is_null($_POST['Nm_Tit_Cob']) or !is_null($_POST['CPF_titular']) or !is_null($_POST['Grupo_FM']))) {
        $var30 = ", ".$_POST['Dt_Pagto'];
    }elseif (!is_null($_POST['Dt_Pagto']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Nm_Tit_Cob']) and is_null($_POST['CPF_titular']) and is_null($_POST['Grupo_FM'])){
        $var30 = $_POST['Dt_Pagto'];
    }else{
        $var30 = "";
    }
#
    if (!is_null($_POST['Duplicata']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) or !is_null($_POST['Nm_Tit_Cob']) or !is_null($_POST['CPF_titular']) or !is_null($_POST['Grupo_FM']) or !is_null($_POST['Dt_Pagto']) )) {
        $var31 = ", ".$_POST['Duplicata'];
    }elseif (!is_null($_POST['Duplicata']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Nm_Tit_Cob']) and is_null($_POST['CPF_titular']) and is_null($_POST['Grupo_FM']) and is_null($_POST['Dt_Pagto'])){
        $var31 = $_POST['Duplicata'];
    }else{
        $var31 = "";
    }
#
    if (!is_null($_POST['Cd_Local_Realizacao']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) or !is_null($_POST['Nm_Tit_Cob']) or !is_null($_POST['CPF_titular']) or !is_null($_POST['Grupo_FM']) or !is_null($_POST['Dt_Pagto']) or !is_null($_POST['Duplicata']) )) {
        $var32 = ", ".$_POST['Cd_Local_Realizacao'];
    }elseif (!is_null($_POST['Cd_Local_Realizacao']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Nm_Tit_Cob']) and is_null($_POST['CPF_titular']) and is_null($_POST['Grupo_FM']) and is_null($_POST['Dt_Pagto']) and is_null($_POST['Duplicata'])){
        $var32 = $_POST['Cd_Local_Realizacao'];
    }else{
        $var32 = "";
    }
#
    if (!is_null($_POST['Local_Realizacao']) and (!is_null($_POST['Cd_Empresa']) or !is_null($_POST['Desc_Empresa']) or !is_null($_POST['Cod_Titular']) or !is_null($_POST['Nome_Titular']) or !is_null($_POST['Codigo']) or is_null($_POST['Vencimento']) or !is_null($_POST['Nome_Usuario']) or !is_null($_POST['Codigo_Interno']) or !is_null($_POST['AMB']) or !is_null($_POST['FM_Procedimento']) or !is_null($_POST['Descricao_AMB']) or !is_null($_POST['Atendimento']) or !is_null($_POST['Liberacao']) or !is_null($_POST['Emissao']) or !is_null($_POST['Especialidade']) or !is_null($_POST['Especialidade_Desc']) or !is_null($_POST['Tipo_Guia']) or !is_null($_POST['Proc_Desc']) or !is_null($_POST['Cod_Guia']) or !is_null($_POST['Desc_Fornecedor']) or !is_null($_POST['Valor_Fator']) or !is_null($_POST['Quantidade']) or !is_null($_POST['Total']) or !is_null($_POST['Operador']) or !is_null($_POST['OBS']) or !is_null($_POST['Cd_Tit_Cob']) or !is_null($_POST['Nm_Tit_Cob']) or !is_null($_POST['CPF_titular']) or !is_null($_POST['Grupo_FM']) or !is_null($_POST['Dt_Pagto']) or !is_null($_POST['Duplicata']) or !is_null($_POST['Cd_Local_Realizacao']))) {
        $var33 = ", ".$_POST['Cd_Local_Realizacao'];
    }elseif (!is_null($_POST['Cd_Local_Realizacao']) and is_null($_POST['Codigo']) and is_null($_POST['Nome_Titular']) and is_null($_POST['Cod_Titular']) and is_null($_POST['Cd_Empresa']) and is_null($_POST['Desc_Empresa']) and
        is_null($_POST['Vencimento']) and is_null($_POST['Nome_Usuario']) and is_null($_POST['Codigo_Interno']) and
        is_null($_POST['AMB']) and is_null($_POST['FM_Procedimento']) and is_null($_POST['Descricao_AMB']) and
        is_null($_POST['Atendimento']) and is_null($_POST['Liberacao']) and is_null($_POST['Emissao']) and 
        is_null($_POST['Especialidade']) and is_null($_POST['Especialidade_Desc']) and is_null($_POST['Tipo_Guia']) 
        and is_null($_POST['Proc_Desc']) and !is_null($_POST['Cod_Guia']) and is_null($_POST['Desc_Fornecedor'])
        and !is_null($_POST['Valor_Fator']) and is_null($_POST['Quantidade']) and is_null($_POST['Total']) and 
        is_null($_POST['Operador']) and is_null($_POST['OBS']) and is_null($_POST['Cd_Tit_Cob']) and is_null($_POST['Nm_Tit_Cob']) and is_null($_POST['CPF_titular']) and is_null($_POST['Grupo_FM']) and is_null($_POST['Dt_Pagto']) and is_null($_POST['Duplicata']) and is_null($_POST['Cd_Local_Realizacao'])){
        $var33 = $_POST['Local_Realizacao'];
    }else{
        $var33 = "";
    }
#

    $_SESSION['usu_cancelamento'] = "select ".$var1.$var2.$var3.$var4.$var5.$var6.$var7.$var8.$var9.$var10.$var11.$var12.$var13.$var14.$var15.$var16.$var17.$var18.$var19.$var20.$var21.$var22.$var23.$var24.$var25.$var26.$var27.$var28.$var29.$var30.$var31.$var32.$var33." FROM TempRelFM_".$_SESSION['usuario']."";

    $_SESSION['var1'] = $var1;
    $_SESSION['var2'] = $var2;
    $_SESSION['var3'] = $var3;
    $_SESSION['var4'] = $var4;
    $_SESSION['var5'] = $var5;
    $_SESSION['var6'] = $var6;
    $_SESSION['var7'] = $var7;
    $_SESSION['var8'] = $var8;
    $_SESSION['var9'] = $var9;
    $_SESSION['var10'] = $var10;
    $_SESSION['var11'] = $var11;
    $_SESSION['var12'] = $var12;
    $_SESSION['var13'] = $var13;
    $_SESSION['var14'] = $var14;
    $_SESSION['var15'] = $var15;
    $_SESSION['var16'] = $var16;
    $_SESSION['var17'] = $var17;
    $_SESSION['var18'] = $var18;
    $_SESSION['var19'] = $var19;
    $_SESSION['var20'] = $var20;
    $_SESSION['var21'] = $var21;
    $_SESSION['var22'] = $var22;
    $_SESSION['var23'] = $var23;
    $_SESSION['var24'] = $var24;
    $_SESSION['var25'] = $var25;
    $_SESSION['var26'] = $var26;
    $_SESSION['var27'] = $var27;
    $_SESSION['var28'] = $var28;
    $_SESSION['var29'] = $var29;
    $_SESSION['var30'] = $var30;
    $_SESSION['var31'] = $var31;
    $_SESSION['var32'] = $var32;
    $_SESSION['var33'] = $var33;

    $_SESSION['usu_cancelamento_sql'] = sqlsrv_query($conn, $_SESSION['usu_cancelamento']);
    if( $_SESSION['usu_cancelamento'] === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
 
    ?>

    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <?php
                            if(!empty($var1)){
                                echo("<th>".trim($var1, '[]')."</th>");
                            }if(!empty($var2)){
                                echo("<th>".str_replace(array(', [',']'),'',$var2)."</th>");
                            }if(!empty($var3)){
                                echo("<th>".str_replace(array(', [',']'),'',$var3)."</th>");
                            }if(!empty($var4)){
                                echo("<th>".str_replace(array(', [',']'),'',$var4)."</th>");
                            }if(!empty($var5)){
                                echo("<th>".str_replace(array(', [',']'),'',$var5)."</th>");
                            }if(!empty($var6)){
                                echo("<th>".str_replace(array(', [',']'),'',$var6)."</th>");
                            }if(!empty($var7)){
                                echo("<th>".str_replace(array(', [',']'),'',$var7)."</th>");
                            }if(!empty($var8)){
                                echo("<th>".str_replace(array(', [',']'),'',$var8)."</th>");
                            }if(!empty($var9)){
                                echo("<th>".str_replace(array(', [',']'),'',$var9)."</th>");
                            }if(!empty($var10)){
                                echo("<th>".str_replace(array(', [',']'),'',$var10)."</th>");
                            }if(!empty($var11)){
                                echo("<th>".str_replace(array(', [',']'),'',$var11)."</th>");
                            }if(!empty($var12)){
                                echo("<th>".str_replace(array(', [',']'),'',$var12)."</th>");
                            }if(!empty($var13)){
                                echo("<th>".str_replace(array(', [',']'),'',$var13)."</th>");
                            }if(!empty($var14)){
                                echo("<th>".str_replace(array(', [',']'),'',$var14)."</th>");
                            }if(!empty($var15)){
                                echo("<th>".str_replace(array(', [',']'),'',$var15)."</th>");
                            }if(!empty($var16)){
                                echo("<th>".str_replace(array(', [',']'),'',$var16)."</th>");
                            }if(!empty($var17)){
                                echo("<th>".str_replace(array(', [',']'),'',$var17)."</th>");
                            }if(!empty($var18)){
                                echo("<th>".str_replace(array(', [',']'),'',$var18)."</th>");
                            }if(!empty($var19)){
                                echo("<th>".str_replace(array(', [',']'),'',$var19)."</th>");
                            }if(!empty($var20)){
                                echo("<th>".str_replace(array(', [',']'),'',$var20)."</th>");
                            }if(!empty($var21)){
                                echo("<th>".str_replace(array(', [',']'),'',$var21)."</th>");
                            }if(!empty($var22)){
                                echo("<th>".str_replace(array(', [',']'),'',$var22)."</th>");
                            }if(!empty($var23)){
                                echo("<th>".str_replace(array(', [',']'),'',$var23)."</th>");
                            }if(!empty($var24)){
                                echo("<th>".str_replace(array(', [',']'),'',$var24)."</th>");
                            }if(!empty($var25)){
                                echo("<th>".str_replace(array(', [',']'),'',$var25)."</th>");
                            }if(!empty($var26)){
                                echo("<th>".str_replace(array(', [',']'),'',$var26)."</th>");
                            }if(!empty($var27)){
                                echo("<th>".str_replace(array(', [',']'),'',$var27)."</th>");
                            }if(!empty($var28)){
                                echo("<th>".str_replace(array(', [',']'),'',$var28)."</th>");
                            }if(!empty($var29)){
                                echo("<th>".str_replace(array(', [',']'),'',$var29)."</th>");
                            }if(!empty($var30)){
                                echo("<th>".str_replace(array(', [',']'),'',$var30)."</th>");
                            }if(!empty($var31)){
                                echo("<th>".str_replace(array(', [',']'),'',$var31)."</th>");
                            }if(!empty($var32)){
                                echo("<th>".str_replace(array(', [',']'),'',$var32)."</th>");
                            }if(!empty($var33)){
                                echo("<th>".str_replace(array(', [',']'),'',$var33)."</th>");
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 

                        while( $row = sqlsrv_fetch_array($_SESSION['usu_cancelamento_sql'], SQLSRV_FETCH_ASSOC) ) {

                        if(is_null($row['Vencimento'])){
                            $Vencimento = '';
                        }else{
                            $Vencimento = date_format($row['Vencimento'], 'd/m/Y');
                        }

                        if(is_null($row['Atendimento'])){
                            $Atendimento = '';
                        }else{
                            $Atendimento = date_format($row['Atendimento'], 'd/m/Y');
                        }

                        if(is_null($row['Liberação'])){
                            $Liberação = '';
                        }else{
                            $Liberação = date_format($row['Liberação'], 'd/m/Y');
                        }

                        if(is_null($row['Emissão'])){
                            $Emissao = '';
                        }else{
                            $Emissao = date_format($row['Emissão'], 'd/m/Y');
                        }

                        if(is_null($row['Dt.Pagto'])){
                            $DtPagto = '';
                        }else{
                            $DtPagto = date_format($row['Dt.Pagto'], 'd/m/Y');
                        }

                        

                        echo "<tr>";?>
                        <?php 
                            if(!empty($var1)){
                                echo("<td>".$row['Codigo Empresa'].              "</td>");
                            }if(!empty($var2)){
                                echo("<td>".$row['Desc Empresa'].                "</td>");
                            }if(!empty($var3)){
                                echo("<td>".$row['Cod.Titular'].                 "</td>");
                            }if(!empty($var4)){
                                echo("<td>".$row['Nome Titular'].                "</td>");
                            }if(!empty($var5)){
                                echo("<td>".$row['Código'].                      "</td>");
                            }if(!empty($var6)){
                                echo("<td>".$Vencimento.                         "</td>");
                            }if(!empty($var7)){
                                echo("<td>".$row['Nome Usuário'].                "</td>");
                            }if(!empty($var8)){
                                echo("<td>".$row['Código Interno'].              "</td>");
                            }if(!empty($var9)){
                                echo("<td>".$row['AMB'].                         "</td>");
                            }if(!empty($var10)){
                                echo("<td>".$row['FM Procedimento'].             "</td>");
                            }if(!empty($var11)){
                                echo("<td>".$row['Descrição AMB'].               "</td>");
                            }if(!empty($var12)){
                                echo("<td>".$Atendimento.                        "</td>");
                            }if(!empty($var13)){
                                echo("<td>".$Liberação.                          "</td>");
                            }if(!empty($var14)){
                                echo("<td>".$Emissao.                            "</td>");
                            }if(!empty($var15)){
                                echo("<td>".$row['Especialidade'].               "</td>");
                            }if(!empty($var16)){
                                echo("<td>".$row['Especialidade Desc'].          "</td>");
                            }if(!empty($var17)){
                                echo("<td>".$row['Tipo Guia'].                   "</td>");
                            }if(!empty($var18)){
                                echo("<td>".$row['Proc Desc'].                   "</td>");
                            }if(!empty($var19)){
                                echo("<td>".$row['Cod. Guia'].                   "</td>");
                            }if(!empty($var20)){
                                echo("<td>".$row['Desc Fornecedor'].             "</td>");
                            }if(!empty($var21)){
                                echo("<td>".$row['Valor Fator'].                "</td>");
                            }if(!empty($var22)){
                                echo("<td>".$row['Quantidade'].                  "</td>");
                            }if(!empty($var23)){
                                echo("<td>".$row['Total'].                       "</td>");
                            }if(!empty($var24)){
                                echo("<td>".$row['Operador'].                    "</td>");
                            }if(!empty($var25)){
                                echo("<td>".$row['OBS'].                         "</td>");
                            }if(!empty($var26)){
                                echo("<td>".$row['Cód. Titular de Cobrança'].    "</td>");
                            }if(!empty($var27)){
                                echo("<td>".$row['Nome Titular de Cobrança'].    "</td>");
                            }if(!empty($var28)){
                                echo("<td>".$row['CPF Titular'].                 "</td>");
                            }if(!empty($var29)){
                                echo("<td>".$row['Grupo FM'].                    "</td>");
                            }if(!empty($var30)){
                                echo("<td>".$DtPagto.                            "</td>");
                            }if(!empty($var31)){
                                echo("<td>".$row['Duplicata'].                   "</td>");
                            }if(!empty($var32)){
                                echo("<td>".$row['Cód. Local Realizacao'].       "</td>");
                            }if(!empty($var33)){
                                echo("<td>".$row['Local Realizacao'].            "</td>");
                            }"
                        </tr>";}
                      ?>
                        </tbody>
                    </table>
                    <div class="col-12 d-flex col-md-6 order-md-1">
                        <a href="gera_planilha_fat_mod.php" style="color: white;">
                            <button type="submit" class="btn btn-primary me-1 mb-1">Salvar</a></button>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include '../html/footer.html' ?>

