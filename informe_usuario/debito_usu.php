<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/debito_usuario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-1 order-last">
                <h3>Débitos Usuário</h3>
                <?php 
                  echo"
                        <div style='margin-bottom: 10px;'>
                            <a href='informe_usuarios.php'><button class='btn btn-primary'>Formulário</button></a>

                            <a href='exibe_informe_usuario.php?cod=".$_GET['cod']."'>
                                <button class='btn btn-primary'>Informações Usuário</button>
                            </a>

                            <a href='debito_usu.php?cod=".$_GET['cod']."'>
                                <button class='btn btn-primary'>Débitos Usuário</button>
                            </a>

                            <a href='dados_plano.php?cod=".$_GET['cod']."'><button class='btn btn-primary'>Dados do Plano</button></a>
                        </div>"; 
                ?>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="card">
            <div class="card-header">
                Simple Datatable
            </div>
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                          <th>Documento</th>
                          <th>SEQ</th>
                          <th>Controle</th>
                          <th>NFSE</th>
                          <th>Cod Ver NFSE</th>
                          <th>Dt Emissão</th>
                          <th>Dt Vencimento</th>
                          <th>Referência</th>
                          <th>Dias Atraso</th>
                      </tr>
                  </thead>
                  <tbody>
                  
                    <?php 
                    while( $row = sqlsrv_fetch_array( $consulta, SQLSRV_FETCH_ASSOC) ) {
                        $_SESSION['cod'] = $row['C_CTRUSU'];
                        echo "
                        <tr>
                        <td>".$row['Pg_cd'].                                    "</td>
                        <td>".$row['Pg_sequencia'].                             "</td>
                        <td>".$row['ControleInadimplencia'].                    "</td>
                        <td> ".$row['NFSE'].                                    "</td>
                        <td> ".$row['CodiVerificNFSE'].                         "</td>
                        <td> ".$row['deb_pg_emissao'].                          "</td>
                        <td> ".$row['deb_vencimento'].                          "</td>
                        <td> ".$row['Referencia'].                              "</td>
                        <td> ".$row['DiasAtraso'].                              "</td>
                        <td><a href='full_debito_usu.php?cod=".$row['Pg_cd']."'><button class='btn btn-primary'>Visualizar tudo</button></a></td>
                        </tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</section>

</div>
<?php include '../html/footer.html'?>