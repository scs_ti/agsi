<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/informe_usuario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>


<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Informações Usuário</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">DataTable</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <?php $full_status = explode('=', $status);
    $sta = substr($full_status[1], 2,-2);
    ?>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Empresa</th>
                            <th>Plano</th>
                            <th>Sexo</th>
                            <th>Endereco</th>
                            <th>Número</th>
                            <th>Complemente</th>
                            <th>UF</th>
                            <th>CEP</th>
                            <th>Mensalidade</th>
                            <th>Dt. Inclusão</th>
                            <th>Dt. Término</th>
                            <th>Dt. Carência</th>
                            <th>SAB</th>
                            <th>Data Bloqueio</th>
                            <th>Operador Bloqueio</th>
                            <th>Bloqueio</th>
                            <th>Gera Combrança</th>
                            <th>Em carência</th>
                            <th>Cód Composto</th>
                            <th>Operador de Suspensão</th>
                            <th>Informações Usuário</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                       while( $row = sqlsrv_fetch_array( $quarto, SQLSRV_FETCH_ASSOC) ) {
                        $_SESSION['cod'] = $row['C_CTRUSU'];
                        $_SESSION['C_NOMUSU'] = $row['C_NOMUSU'];
                        $_SESSION['Pla_ds'] = $row['Pla_ds'];

                        if($sta == 'I'){
                            echo "<tr style= 'color: #dc3545';>
                            <th scope='row'> ".$row['C_CTRUSU']." </th>
                            <td> ".$row['C_NOMUSU'].            " </td>
                            <td> ".$row['EMP_ds'].              " </td>
                            <td> ".$row['Pla_ds'].              " </td>
                            <td> ".$row['C_SEXUSU'].            " </td>
                            <td> ".$row['C_ENDUSU'].            " </td>
                            <td> ".$row['Usu_EndNum'].          " </td>
                            <td> ".$row['Usu_EndComp'].         " </td>
                            <td> ".$row['C_ESTUSU'].            " </td>
                            <td> ".$row['C_CEPUSU'].            " </td>
                            <td> ".$row['C_VlmUsu'].            " </td>
                            <td> ".$row['dt_inclusao'].         " </td>
                            <td> ".$row['dt_termino'].          " </td>
                            <td> ".$row['dt_carencia'].         " </td>
                            <td> ".$row['SAB'].                 " </td>
                            <td> ".$row['dt_bloqueio'].         " </td>
                            <td> ".$row['Usu_OperadorBloqueio']." </td>
                            <td> ".$row['Usu_Bloqueio'].        " </td>
                            <td> ".$row['Usu_GeraCob'].         " </td>
                            <td> ".$row['EmCarencia'].          " </td>
                            <td> ".$row['CodigoComposto'].      " </td>
                            <td> ".$row['OperadorSusp'].        " </td>
                            <td><a href='exibe_informe_usuario.php?cod=".$row['C_CTRUSU']."'>
                                    <button class='btn btn-primary'>Informações Usuário</button>
                                </a>
                            </td>
                            </tr>"
                            ;}
                            elseif($sta == 'A'){
                                echo "<tr style= 'color: #198754';>
                                <th scope='row'> ".$row['C_CTRUSU']." </th>
                                <td> ".$row['C_NOMUSU'].            " </td>
                                <td> ".$row['EMP_ds'].              " </td>
                                <td> ".$row['Pla_ds'].              " </td>
                                <td> ".$row['C_SEXUSU'].            " </td>
                                <td> ".$row['C_ENDUSU'].            " </td>
                                <td> ".$row['Usu_EndNum'].          " </td>
                                <td> ".$row['Usu_EndComp'].         " </td>
                                <td> ".$row['C_ESTUSU'].            " </td>
                                <td> ".$row['C_CEPUSU'].            " </td>
                                <td> ".$row['C_VlmUsu'].            " </td>
                                <td> ".$row['dt_inclusao'].         " </td>
                                <td> ".$row['dt_termino'].          " </td>
                                <td> ".$row['dt_carencia'].         " </td>
                                <td> ".$row['SAB'].                 " </td>
                                <td> ".$row['dt_bloqueio'].         " </td>
                                <td> ".$row['Usu_OperadorBloqueio']." </td>
                                <td> ".$row['Usu_Bloqueio'].        " </td>
                                <td> ".$row['Usu_GeraCob'].         " </td>
                                <td> ".$row['EmCarencia'].          " </td>
                                <td> ".$row['CodigoComposto'].      " </td>
                                <td> ".$row['OperadorSusp'].        " </td>
                                <td><a href='exibe_informe_usuario.php?cod=".$row['C_CTRUSU']."'>
                                    <button class='btn btn-primary'>Informações Usuário</button>
                                    </a>
                                </td>
                                </tr>"
                                ;}
                                else{
                                    echo "<tr>
                                    <th scope='row'> ".$row['C_CTRUSU']." </th>
                                    <td> ".$row['C_NOMUSU'].            " </td>
                                    <td> ".$row['EMP_ds'].              " </td>
                                    <td> ".$row['Pla_ds'].              " </td>
                                    <td> ".$row['C_SEXUSU'].            " </td>
                                    <td> ".$row['C_ENDUSU'].            " </td>
                                    <td> ".$row['Usu_EndNum'].          " </td>
                                    <td> ".$row['Usu_EndComp'].         " </td>
                                    <td> ".$row['C_ESTUSU'].            " </td>
                                    <td> ".$row['C_CEPUSU'].            " </td>
                                    <td> ".$row['C_VlmUsu'].            " </td>
                                    <td> ".$row['dt_inclusao'].         " </td>
                                    <td> ".$row['dt_termino'].          " </td>
                                    <td> ".$row['dt_carencia'].         " </td>
                                    <td> ".$row['SAB'].                 " </td>
                                    <td> ".$row['dt_bloqueio'].         " </td>
                                    <td> ".$row['Usu_OperadorBloqueio']." </td>
                                    <td> ".$row['Usu_Bloqueio'].        " </td>
                                    <td> ".$row['Usu_GeraCob'].         " </td>
                                    <td> ".$row['EmCarencia'].          " </td>
                                    <td> ".$row['CodigoComposto'].      " </td>
                                    <td> ".$row['OperadorSusp'].        " </td>
                                    <td><a href='exibe_informe_usuario.php?cod=".$row['C_CTRUSU']."'>
                                        <button class='btn btn-primary'>Informações Usuário</button>
                                        </a>
                                    </td>
                                    </tr>";
                                }
                            }
                            echo"
                            <div style='margin-bottom: 10px;'>
                            <a href='informe_usuarios.php'><button class='btn btn-primary'>Formulário</button></a>

                            <a href='debito_usu.php?cod=".$_SESSION['cod']."'>
                            <button class='btn btn-primary'>Débitos Usuário</button>
                            </a>

                            <a href='dados_plano.php?cod=".$_SESSION['cod']."'><button class='btn btn-primary'>Dados do Plano</button></a>
                            </div>"; 
                            ?>    
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>


    <?php include '../html/footer.html' ?>

