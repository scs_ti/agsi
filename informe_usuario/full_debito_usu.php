<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/debito_usuario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Débitos Usuário</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                <div class="row">
                                <?php 
                                    while( $row = sqlsrv_fetch_array( $consulta_all, SQLSRV_FETCH_ASSOC) ) {
                                    echo "
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Documento</label>
                                            <input type='text' class='form-control' value='".$row['Pg_cd']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>SEQ</label>
                                            <input type='text' class='form-control' value='".$row['Pg_sequencia']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Controle</label>
                                            <input type='text' class='form-control' value='".$row['ControleInadimplencia']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>NFSE</label>
                                            <input type='text' class='form-control' value='".$row['NFSE']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Cód Ver NFSE</label>
                                            <input type='text' class='form-control' value='".$row['CodiVerificNFSE']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Dt Emissão</label>
                                            <input type='text' class='form-control' value='".$row['deb_pg_emissao']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Dt Vencimento</label>
                                            <input type='text' class='form-control' value='".$row['deb_vencimento']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Referência</label>
                                            <input type='text' class='form-control' value='".$row['Referencia']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Dias atraso</label>
                                            <input type='text' class='form-control' value='".$row['DiasAtraso']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Recebimento</label>
                                            <input type='text' class='form-control' value='".$row['Trec_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Valor Mens</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Pg_vlMensalidade'], 0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Valor Op.</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Pg_vlOpcionais'], 0 , -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Comp. Duplicata</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Pg_vlCoParticipacao'], 0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Juros</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Pg_Juros'],0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Descontos</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Brec_juro'],0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Valor Total</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Brec_desconto'],0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Valor Pago</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['Pg_Valor'],0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Dt Baixa</label>
                                            <input type='text' class='form-control' value='".$row['deb_BRec_DtBaixa']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Status</label>
                                            <input type='text' class='form-control' value='".$row['Pg_StatusBaixa']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Valor Aberto</label>
                                            <input type='text' class='form-control' value='R$ ".substr($row['VlAberto'], 0, -2)."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Banco</label>
                                            <input type='text' class='form-control' value='".$row['CC_Banco']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Agência</label>
                                            <input type='text' class='form-control' value='".$row['CC_Agencia']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Conta Corrente</label>
                                            <input type='text' class='form-control' value='".$row['CC_Cd']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Nosso Número</label>
                                            <input type='text' class='form-control' value='".$row['Pg_NossoNumero']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>OBS baixa</label>
                                            <input type='text' class='form-control' value='".$row['ObservacaoBaixa']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>OBS Juros/Descontos</label>
                                            <input type='text' class='form-control' value='".$row['BRec_ObsJuros']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Cod. Tipo Movimento</label>
                                            <input type='text' class='form-control' value='".$row['Brec_MovFinanceiro']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Movimento</label>
                                            <input type='text' class='form-control' value='".$row['Movimento_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo de Recebimento da Duplicata</label>
                                            <input type='text' class='form-control' value='".$row['RecebeDupl']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Vigência (Risco)</label>
                                            <input type='text' class='form-control' value='".$row['Vigencia (Risco)']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Valor Líquido</label>
                                            <input type='text' class='form-control' value='".$row['VlrLiquido']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Dt. Referência</label>
                                            <input type='text' class='form-control' value='".$row['C_VCTUSU']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Número Folha Comissão</label>
                                            <input type='text' class='form-control' value='".$row['NroFolhaComissao']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Dt Venc Comissão</label>
                                            <input type='text' class='form-control' value='".$row['DtVenctoComissao']."' disabled>
                                            </select>
                                        </div>
                                    </div>";
                                }?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="section">
            <div class="card">
                <div class="card-header">
                    Composição (Família)
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                            <th>Código</th>
                            <th>Valor Mensalidade</th>
                            <th>VL FM</th>
                            <th>VL Opcional</th>
                            <th>Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        while( $row = sqlsrv_fetch_array( $composicao, SQLSRV_FETCH_ASSOC) ) {
                            echo "<tr>
                                <td>".$row['Codigo'].                           "</td>
                                <td>".$row['Nome'].                             "</td>
                                <td>".$row['VlMensalidade'].                    "</td>
                                <td>".$row['VlFM'].                             "</td>
                                <td>".$row['VlOpcional'].                       "</td>
                            </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html'?>