<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php';
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}

?>


<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Informe Usuários</h3>
            </div>
        </div>
    </div>

    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                <!--<form class="form" action="consultas/informe_usuario.php" method="POST">-->
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label>Nome</label>
                                            <input type="text" id="first-name-column" name="nome" class="form-control"
                                            placeholder="Insira o Nome" name="fname-column">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label>Código</label>
                                            <input type="text" onkeypress="mascara(this,cod)" maxlength="19" name="codigo" class="form-control" placeholder="Entre com o código" name="lname-column">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label>Contrato</label>
                                            <input type="text" class="form-control" placeholder="" name="contrato">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label>Cod. Abrange</label>
                                            <input type="text" name="cod_abrange" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label>Empresa</label>
                                            <input type="text"name="empresa" class="form-control" placeholder="Entre com o nome da Empresa">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label>Selecione o Plano</label>
                                            <select class="form-control" name="plano">
                                                <option></option>
                                                <?php 
                                                while( $row = sqlsrv_fetch_array( $quarto, SQLSRV_FETCH_ASSOC) ) {
                                                    echo "<option>".$row['PLA_cd']." - ".$row['PLA_ds']."</option>";}
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>CPF</label>
                                                <input type="text" name="cpf" onBlur="validaCPF(this)" maxlength="11" class="form-control" placeholder="xxx.xxx.xxx-xx">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Data de Nascimento</label>
                                                <input type="date" name="data_nasc" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Nome Responsável Financeiro</label>
                                                <input type="text" name="resp" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>CPF Responsável Financeiro</label>
                                                <input type="text"  name="cpf_resp" onBlur="validaCPF(this)" maxlength="11" placeholder="xxx.xxx.xxx-xx" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Telefone Residencial</label>
                                                <input type="text"  name="tel_resi" type="varchar" id="telefone" maxlength="14" placeholder="(12) 1234-5678" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Telefone Comercial</label>
                                                <input type="text"  name="tel_com" type="varchar" id="telefone2" maxlength="14" placeholder="(12) 1234-5678" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Celular</label>
                                                <input type="text"  name="cel" type="varchar" id="celular" maxlength="15" placeholder="(12) 91234-5678" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>CCO Beneficiario</label>
                                                <input type="text"  name="cco" type="varchar" maxlength="14" placeholder="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option>Ativo</option>
                                                    <option>Inativo</option>
                                                    <option>Ambos</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Pessoa</label>
                                                <select class="form-control" name="pessoa">
                                                    <option>Física</option>
                                                    <option>Juridica</option>
                                                    <option>Todos</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Somente Bloqueados</label>
                                                <select class="form-control" name="bloqueados">
                                                    <option>Não</option>
                                                    <option>Sim</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic multiple Column Form section end -->
    </div>

    <?php include '../html/footer.html'?>