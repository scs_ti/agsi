<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/dados_plano.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Débitos Usuário(a) <?php echo $_GET['C_NOMUSU'] ?></h3>
            </div>
        </div>
    </div>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <?php 
                echo"
                        <div style='margin-bottom: 10px;'>
                            <a href='informe_usuarios.php'><button class='btn btn-primary'>Formulário</button></a>
                            <a href='exibe_informe_usuario.php?cod=".$_GET['cod']."'>
                                <button class='btn btn-primary'>Informações Usuário</button>
                            </a>
                            <a href='debito_usu.php?cod=".$_GET['cod']."'>
                                <button class='btn btn-primary'>Débitos Usuário</button>
                            </a>
                            <a href='dados_plano.php?cod=".$_GET['cod']."'><button class='btn btn-primary'>Dados do Plano</button></a>
                        </div>";    
                ?>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                <div class="row">
                                    <?php while( $row = sqlsrv_fetch_array( $consulta, SQLSRV_FETCH_ASSOC) ) {
                                            $_SESSION['cod'] = $row['C_CTRUSU'];
                                            echo " <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Código Plano</label>
                                                            <input type='text' value='".$row['PLA_cd']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4 col-12'>
                                                        <div class='form-group'>
                                                            <label>Nome Plano</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['PLA_ds']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4 col-12'>
                                                        <div class='form-group'>
                                                            <label>Descrição Resumida</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['PLA_DescResumo']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>SIMPAS</label>
                                                            <input type='text' value='".$row['PLA_simpas']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Acomodação</label>
                                                            <input type='text' value='".$row['PLA_acomodacao']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4 col-12'>
                                                        <div class='form-group'>
                                                            <label>Acomodação TISS COD</label>
                                                            <input type='text' value='".$row['Pla_AcomodacaoTISS']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-6 col-12'>
                                                        <div class='form-group'>
                                                            <label>Acomodação TISS Nome</label>
                                                            <input type='text' value='".$row['TAc_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tipo Plano</label>
                                                            <input type='text' value='".$row['Pla_TipoPlano']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                             name='fname-column'>
                                                        </div>
                                                    </div>  
                                                    <div class='col-md-5 col-12'>
                                                        <div class='form-group'>
                                                            <label></label>
                                                            <input type='text' value='".$row['PrecoPlano_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-5 col-12'>
                                                        <div class='form-group'>
                                                            <label>Código SCPA</label>
                                                            <input type='text' value='' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tipo Cobertura</label>
                                                            <input type='text' value='".$row['Pla_TipoCobertura']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4 col-12'>
                                                        <div class='form-group'>
                                                            <label></label>
                                                            <input type='text' value='".$row['PrecoCobertura_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Abrangência Geográfica</label>
                                                            <input type='text' value='".$row['Pla_AbranGeog']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4 col-12'>   
                                                        <div class='form-group'>
                                                            <label></label>
                                                            <input type='text' value='".$row['Abr_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Fator Moderador</label>
                                                            <input type='text' value='".$row['PLA_FatorModerador']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-10 col-12'>
                                                        <div class='form-group'>
                                                            <label>Observação</label>
                                                            <input type='text' value='".$row['PLA_obs']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tabela de Preço</label>
                                                            <input type='text' value='".$TabPreco_cd."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-10 col-12'>
                                                        <div class='form-group'>
                                                            <label></label>
                                                            <input type='text' value='".$TabPreco_ds."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Grupo Código</label>
                                                            <input type='text' value='".$row['Pla_Grupo']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-10 col-12'>
                                                        <div class='form-group'>
                                                            <label>Grupo Nome:</label>
                                                            <input type='text' value='".$row['gp_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control' name='fname-column'>
                                                        </div>
                                                    </div>                                                    
                                                  ";
                                          }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Faixas</th>
                            <th>Titular (%)</th>
                            <th>Empresa</th>
                            <th>Plano</th>
                        </tr>
                    </thead>
                    <tbody>
                     <?php
                     while( $row = sqlsrv_fetch_array( $reajuste, SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                                <th> ".$row['Faixas']           ."</th>
                                <td> ".$row['Titular (%)']      ."</td>
                                <td> ".$row['Dependente (%)']   ."</td>
                                <td> ".$row['Agregado (%)']     ."</td>
                              </tr>"
                        ;}?>    
                    </tbody>
                </table>
            </div>
        </div>

    </section>



</div>
<?php include '../html/footer.html'?>