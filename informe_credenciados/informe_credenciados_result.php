<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/informe_credenciados_desc.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Informe Credênciados</h3>
    </div><br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Credenciado</th>
                            <th>Razão Social</th>
                            <th>Endereço</th>
                            <th>Bairro</th>
                            <th>CEP</th>
                            <th>Visualisar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $completo, SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                        <td>".$row['Fornecedor_cd'].                                    "</td>
                        <td>".$row['Fornecedor_ds'].                                    "</td>
                        <td>".$row['Fornecedor_RazSocial'].                              "</td>
                        <td>".$row['Fornecedor_end'].                                "</td>
                        <td>".$row['Fornecedor_bairro'].                                "</td>
                        <td>".$row['Fornecedor_cep'].                                   "</td>
                        
                        <td><a href='informe_credenciados_comp.php?cod=".$row['Fornecedor_cd']."'>
                        <button class='btn btn-primary'>Visualizar</button></a></td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

