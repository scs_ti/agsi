<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';
include '../consultas/consulta_informe_credenciados.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Informe Credenciados</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="informe_credenciados_result.php" method="POST">
                                    <div class="row">
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Código</label>
                                                <input type="text" name="cod" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Nome</label>
                                                <input type="text" name="nome" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Região</label>
                                                <select class="form-control" name="regiao">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $regioes, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['Reg_cd'].">".$row['Reg_cd']." - ".$row['Reg_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Razão Social</label>
                                                <input type="text" name="rz_social" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Cidade</label>
                                                <input type="text" name="cidade" maxlength="19" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Categoria</label>
                                                <select class="form-control" name="categoria">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $categoria, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option>".$row['Tfornece_cd']." - ".$row['Tfornece_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>CNPJ</label>
                                                <input type="text" name="cnpj" maxlength="19" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>CPF</label>
                                                <input type="text" name="cpf" maxlength="19" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" name="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Local Atendimento</label>
                                                <select class="form-control" name="lc_atendimento">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $local_atendimento, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['Fornecedor_cd'].">".$row['Fornecedor_cd']." - ".$row['Fornecedor_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select id="options" class="form-control" name="status" onchange="verifica_desc(this.value)">
                                                    <option value="Todas">Todos</option>
                                                    <option value="A">Ativos</option>
                                                    <option value="I">Inativos</option>
                                                    <option value="S">Suspensos</option>
                                                    <option value="E">Em Análise</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Emite Guia</label>
                                                <select id="options" class="form-control" name="emite_guia" onchange="verifica_desc(this.value)">
                                                    <option value="T">Todas</option>
                                                    <option value="S">Sim</option>
                                                    <option value="N">Não</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Pessoa</label>
                                                <select id="options" class="form-control" name="pessoa" onchange="verifica_desc(this.value)">
                                                    <option value="T">Todas</option>
                                                    <option value="F">Fisíca</option>
                                                    <option value="J">Jurídica</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>N° Registro</label>
                                                <input type="text" name="nr_registro" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Envio Guia Médico</label>
                                                <select id="options" class="form-control" name="env_guia_med" onchange="verifica_desc(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="S">Sim</option>
                                                    <option value="N">Não</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Especialidade</label>
                                                <select class="form-control" name="especialidade">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $especialidade, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['Especialidade_ds'].">".$row['Especialidade_cd']." - ".$row['Especialidade_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Qualidade da Especialidade</label>
                                                <select class="form-control" name="quali_esp">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $qualificacao, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['Qualificacao_cd'].">".$row['Qualificacao_cd']." - ".$row['Qualificacao_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <select class="form-control" name="empresa">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!--<div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Plano</label>
                                                <select class="form-control" name="plano">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $plano, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option value=".$row['PLA_cd'].">".$row['PLA_cd']." - ".$row['PLA_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>-->
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Procedimento</label>
                                                <input type="text" name="procedimento" class="form-control">
                                            </div>
                                        </div>
                                                                                
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>