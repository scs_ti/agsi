<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/informe_credenciados_id.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 d-flex col-md-6 order-md-1">
                <a href="javascript:history.back()" style="color: white;"><button class="btn btn-primary me-1 mb-1">Voltar</a></button>
                <h3>Informe Credênciados</h3>
            </div>
        </div>      
    </div>
    <br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                <div class="row">
                                    <?php while( $row = sqlsrv_fetch_array( $completo_id, SQLSRV_FETCH_ASSOC) ) {


                                            if($row['DtIniContrato'] != ''){
                                                $dic = date_format($row['DtIniContrato'], 'd/m/Y');
                                            }else{
                                                $dic = 'N/A';
                                            }

                                            if($row['DtFinContrato'] != ''){
                                                $dfc = date_format($row['DtFinContrato'], 'd/m/Y');
                                            }else{
                                                $dfc = 'N/A';
                                            }

                                            if($row['UltimoDiaReajuste'] != ''){
                                                $dur = date_format($row['UltimoDiaReajuste'], 'd/m/Y');
                                            }else{
                                                $dur = 'N/A';
                                            }

                                            

                                            $_SESSION['cod'] = $row['C_CTRUSU'];
                                            echo " <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Código</label>
                                                            <input type='text' value='".$row['Fornecedor_cd']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-5 col-12'>
                                                        <div class='form-group'>
                                                            <label>Credênciado</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['Fornecedor_ds']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-5 col-12'>
                                                        <div class='form-group'>
                                                            <label>Endereço</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['Fornecedor_end']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-5 col-12'>
                                                        <div class='form-group'>
                                                            <label>Bairro</label>
                                                            <input type='text' value='".$row['Fornecedor_bairro']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>CEP</label>
                                                            <input type='text' value='".$row['Fornecedor_cep']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Cidade</label>
                                                            <input type='text' value='".$row['Cid_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>IBGE</label>
                                                            <input type='text' value='".$row['Cid_CodIBGE']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>UF</label>
                                                            <input type='text' value='".$row['Fornecedor_uf']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>CNPJ</label>
                                                            <input type='text' value='".$row['Fornecedor_cnpj']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>CPF</label>
                                                            <input type='text' value='".$row['Fornecedor_cpf']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-1 col-12'>
                                                        <div class='form-group'>
                                                            <label>Imposto</label>
                                                            <input type='text' value='".$row['Fornecedor_TipoIrrf']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Telefone</label>
                                                            <input type='text' value='".$row['Fornecedor_Fone1']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>RG</label>
                                                            <input type='text' value='".$row['Fornecedor_RG']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>E-mail</label>
                                                            <input type='text' value='".$row['Fornecedor_email']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Banco</label>
                                                            <input type='text' value='".$row['Fornecedor_Banco']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Agência</label>
                                                            <input type='text' value='".$row['Fornecedor_Agencia']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Conta</label>
                                                            <input type='text' value='".$row['Fornecedor_Conta']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                     <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Emite Cheque</label>
                                                            <input type='text' value='".$row['Fornecedor_EmiteCheque']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tipo Fornecedor</label>
                                                            <input type='text' value='".$row['Fornecedor_TipoFor']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4 col-12'>
                                                        <div class='form-group'>
                                                            <label>Razão Social</label>
                                                            <input type='text' value='".$row['Fornecedor_RazSocial']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-1 col-12'>
                                                        <div class='form-group'>
                                                            <label>Status</label>
                                                            <input type='text' value='".$row['Fornecedor_Status']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tipo Credênciamento</label>
                                                            <input type='text' value='".$row['Fornecedor_TipoCredenciamento']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Conta Contabíl</label>
                                                            <input type='text' value='".$row['Fornecedor_CtaCtabil']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tipo Pessoa</label>
                                                            <input type='text' value='".$row['Fornecedor_Pessoa']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Desconto</label>
                                                            <input type='text' value='".$row['Fornecedor_Desconto']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Contato</label>
                                                            <input type='text' value='".$row['Fornecedor_Contato']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Logradouro</label>
                                                            <input type='text' value='".$row['Fornecedor_Logradouro']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Envia p/Web</label>
                                                            <input type='text' value='".$row['For_EnviaWEB']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Tipo Conta</label>
                                                            <input type='text' value='".$row['Fornecedor_TipoContra']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-1 col-12'>
                                                        <div class='form-group'>
                                                            <label>Sexo</label>
                                                            <input type='text' value='".$row['Fornecedor_sexo']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-1 col-12'>
                                                        <div class='form-group'>
                                                            <label>Status</label>
                                                            <input type='text' value='".$row['Fornecedor_GuiaMedico']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Data ini Contrato</label>
                                                            <input type='text' value='".$dic."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Data final Contrato</label>
                                                            <input type='text' value='".$dfc."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Ultimo Dia Reajuste</label>
                                                            <input type='text' value='".$dur."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='' name='fname-column'>
                                                        </div>
                                                    </div>
                                                  ";
                                          }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <h3>Histórico de Alteração</h3>
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Dt. Operação</th>
                            <th>Operação</th>
                            <th>Operador</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                        while( $row = sqlsrv_fetch_array( $historico, SQLSRV_FETCH_ASSOC) ) {

                        if($row['LData'] != ''){
                            $LData = date_format($row['LData'], 'd/m/Y');
                        }else{
                            $LData = 'N/A';
                        }

                        echo "
                        <tr>
                            <td>".$LData.                                               "</td>
                            <td>".$row['LStatus'].                                               "</td>
                            <td>".$row['LOperador'].                                               "</td>
                        </tr>";}
                      ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
</div>
<?php include '../html/footer.html'?>