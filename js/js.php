<script>
  function validaCPF(elemento)
  {
    cpf = elemento.value;
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf == '') return elemento.style.backgroundColor = "red";
          // Elimina CPFs invalidos conhecidos    
          if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return elemento.style.backgroundColor = "red";
          // Valida 1o digito 
          add = 0;
          for (i = 0; i < 9; i++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
          rev = 11 - (add % 11);
          if (rev == 10 || rev == 11)
            rev = 0;
          if (rev != parseInt(cpf.charAt(9)))
            return elemento.style.backgroundColor = "red";
          // Valida 2o digito 
          add = 0;
          for (i = 0; i < 10; i++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
          rev = 11 - (add % 11);
          if (rev == 10 || rev == 11)
            rev = 0;
          if (rev != parseInt(cpf.charAt(10)))
           return elemento.style.backgroundColor = "red";
         return elemento.style.backgroundColor = "white";
       }

       /* Máscaras ER */
      function mascara(o,f)
       {
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
      }
      function execmascara(){
        v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
        v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
        return v;
      }
      function id( el ){
        return document.getElementById( el );
      }
      window.onload = function(){
        id('telefone').onkeyup = function(){
          mascara( this, mtel );
        }
        id('telefone2').onkeyup = function(){
          mascara( this, mtel );
        }
        id('celular').onkeyup = function(){
          mascara( this, mtel );
        }
      }

      function cod(v){
        v=v.replace(/(\d{4})(\d{2})(\d{5})(\d{2})(\d{1})/gi,"$1.$2.$3.$4-$5")
        return v
      }

      function verifica(value){
        var input = document.getElementById("input");

        if(value == 'Todos'){
          input.disabled = true;
        }else if(value == 'Igual' || value == 'Maior Igual' || value == 'Menor Igual' || value == 'Diferente'){
          input.disabled = false;
        }
      };

      function verifica2(value){
        var input = document.getElementById("input0");

        if(value == 'Todos'){
          input.disabled = true;
        }else if(value == '=' || value == '>=' || value == '<=' || value == '<>'){
          input.disabled = false;
        }
      };


      function verifica_desc(value){
        var input = document.getElementById("input2");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Começa com' || value == 'Palavra Chave' || value == 'Igual'){
          input.disabled = false;
        }
      };

      function verifica_cidade(value){
        var input = document.getElementById("input3");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Começa com' || value == 'Palavra Chave' || value == 'Igual'){
          input.disabled = false;
        }
      };

      function verifica_fantasia(value){
        var input = document.getElementById("input4");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Começa com' || value == 'Palavra Chave' || value == 'Igual'){
          input.disabled = false;
        }
      };

      function grupo_empresa(value){
        var input = document.getElementById("gp_emp");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
        }
      };

       function mot_cancelamento(value){
        var input = document.getElementById("input5");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Começa com' || value == 'Palavra Chave' || value == 'Igual'){
          input.disabled = false;
        }
      };

      function data_emissao(value){
        var input = document.getElementById("input6");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == '=' || value == '>=' || value == '<=' || value == '<>'){
          input.disabled = false;
        }
      };

      function verifica_emp(value){
        var input1 = document.getElementById("input2");
        var input2 = document.getElementById("input3");

        if(value == 'Todas'){
          input1.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          document.getElementById("input2").required = true;
          input2.disabled = false;
        }
      };

      function libera_guia(value){
        var input1 = document.getElementById("guia_ini");
        var input2 = document.getElementById("guia_fim");

        if(value == 'Todas'){
          input1.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          input2.disabled = false;
        }
      };

      function emissao_guia(value){
        var input1 = document.getElementById("emit_guia_ini");
        var input2 = document.getElementById("emit_guia_fim");

        if(value == 'Todas'){
          input1.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          input2.disabled = false;
        }
      };

      function can_termino(value){
        var input1 = document.getElementById("dt_can_u_ini");
        var input2 = document.getElementById("dt_can_u_fim");

        if(value == 'Todas'){
          input1.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          input2.disabled = false;
        }
      };

      function mov_cancelamento(value){
        var input1 = document.getElementById("mov_can_ini");
        var input2 = document.getElementById("mov_can_fim");

        if(value == 'Todas'){
          input1.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          input2.disabled = false;
        }
      };

      function fechamento(value){
        var input1 = document.getElementById("fechamento_ini");
        var input2 = document.getElementById("fechamento_fim");

        if(value == 'Todas'){
          input1.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          input2.disabled = false;
        }
      };


      function opeexiste(value){
        var input = document.getElementById("operator");

        if(value == 'Todos'){
          input.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
        }
      };

      function seleRegiao(value){
        var input = document.getElementById("regiao");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
        }
      };

      function verifica_dia(value){
        var dia_ini = document.getElementById("dia_ini");
        var dia_fim = document.getElementById("dia_fim");

        if(value == 'Todos'){
          dia_ini.disabled = true;
          dia_fim.disabled = true;
        }else if(value == 'Selecionar'){
          dia_ini.disabled = false;
          dia_fim.disabled = false;
        }
      };

      function usuexiste(value){
        var input = document.getElementById("live_search");
        var input2 = document.getElementById("nome_u");

        if(value == 'Todos'){
          input.disabled = true;
          input2.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
          input2.disabled = false;
        }
      };
      
      function toggle_tusr(value){
        var input = document.getElementById("selec_tusr");

        if(value == 'Todos'){
          input.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
        }
      };
      
      function toggle_fetaria(value){
        var input = document.getElementById("selec_fetaria");

        if(value == 'Todos'){
          input.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
        }
      };

      function selecionadata(value){
        var input1 = document.getElementById("date1");
        var input2 = document.getElementById("date2");

        if(value == 'Todos'){
          input1.disabled = true;
          input2.disabled = true;
          input1.required = false;
          input2.required = false;
        }else if(value == 'Selecionar'){
          input1.disabled = false;
          input1.required = true;
          input2.disabled = false;
          input2.required = true;
        }
      };

      function insereFaixa(value){
        var input = document.getElementById("faixa01");
        var input2 = document.getElementById("faixa02");
        var i = document.getElementById("num_faixas");
        var x = document.getElementById("metod");

        if(value == 'P'){
          i.disabled = true;
          x.style.visibility  = "hidden";
        }else if(value == 'F'){
          i.disabled = false;
          x.style.visibility  = "visible";
        }
      };

      function numero_faixa(value) {
        var input1 = document.getElementById("faixa01");
        var input2 = document.getElementById("faixa02");
        var input3 = document.getElementById("faixa03");
        var input4 = document.getElementById("faixa04");
        var input5 = document.getElementById("faixa05");
        var input6 = document.getElementById("faixa06");
        var input7 = document.getElementById("faixa07");
        var input8 = document.getElementById("faixa08");
        var input9 = document.getElementById("faixa09");
        var input10 = document.getElementById("faixa10");
        

        if(value == '0'){
          input1.style.visibility  = "hidden";
          input1.value = '';
          input2.style.visibility  = "hidden";
          input2.value = '';
          input3.style.visibility  = "hidden";
          input3.value = '';
          input4.style.visibility  = "hidden";
          input4.value = '';
          input5.style.visibility  = "hidden";
          input5.value = '';
          input6.style.visibility  = "hidden";
          input6.value = '';
          input7.style.visibility  = "hidden";
          input7.value = '';
          input8.style.visibility  = "hidden";
          input8.value = '';
          input9.style.visibility  = "hidden";
          input9.value = '';
          input10.style.visibility = "hidden";
          input10.value = '';
        }

        if(value == '1'){
          input1.style.visibility  = "visible";
          input2.style.visibility  = "visible";
          input3.style.visibility  = "hidden";
          input4.style.visibility  = "hidden";
          input5.style.visibility  = "hidden";
          input6.style.visibility  = "hidden";
          input7.style.visibility  = "hidden";
          input8.style.visibility  = "hidden";
          input9.style.visibility  = "hidden";
          input10.style.visibility = "hidden";
        }

        if(value == '2'){
          input1.style.visibility  = "visible";
          input2.style.visibility  = "visible";
          input3.style.visibility  = "visible";
          input4.style.visibility  = "visible";
          input5.style.visibility  = "hidden";
          input6.style.visibility  = "hidden";
          input7.style.visibility  = "hidden";
          input8.style.visibility  = "hidden";
          input9.style.visibility  = "hidden";
          input10.style.visibility = "hidden";
        }

        if(value == '3'){
          input1.style.visibility  = "visible";
          input2.style.visibility  = "visible";
          input3.style.visibility  = "visible";
          input4.style.visibility  = "visible";
          input5.style.visibility  = "visible";
          input6.style.visibility  = "visible";
          input7.style.visibility  = "hidden";
          input8.style.visibility  = "hidden";
          input9.style.visibility  = "hidden";
          input10.style.visibility = "hidden";
        }

        if(value == '4'){
          input1.style.visibility  = "visible";
          input2.style.visibility  = "visible";
          input3.style.visibility  = "visible";
          input4.style.visibility  = "visible";
          input5.style.visibility  = "visible";
          input6.style.visibility  = "visible";
          input7.style.visibility  = "visible";
          input8.style.visibility  = "visible";
          input9.style.visibility  = "hidden";
          input10.style.visibility = "hidden";
        }

        if(value == '5'){
          input1.style.visibility  = "visible";
          input2.style.visibility  = "visible";
          input3.style.visibility  = "visible";
          input4.style.visibility  = "visible";
          input5.style.visibility  = "visible";
          input6.style.visibility  = "visible";
          input7.style.visibility  = "visible";
          input8.style.visibility  = "visible";
          input9.style.visibility  = "visible";
          input10.style.visibility = "visible";
        }

      }

    function tipo_guia(value){
        var input = document.getElementById("nome");

        if(value == 'Todas'){
          input.disabled = true;
        }else if(value == 'Selecionar'){
          input.disabled = false;
        }
      };

       
    </script>



