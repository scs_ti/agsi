<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Cobertura Empresa</h3>
    </div>
    <br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <div class="alert alert-primary" role="alert">
                      Somente são vísiveis as empresas com o parametro de Cobertura Diferenciada ativo.
                  </div>
                  <thead>
                    <tr>
                        <th>Código</th>
                        <th>Empresa</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  while( $row = sqlsrv_fetch_array( $consulta_cobertura_empresa, SQLSRV_FETCH_ASSOC) ) {
                    echo "<tr>
                    <td>".$row['Emp_cd'].                                    "</td>
                    <td>".$row['Emp_ds'].                                    "</td></tr>";}
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</section>
</div>


<?php include '../html/footer.html' ?>

