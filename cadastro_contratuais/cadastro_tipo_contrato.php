<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>


<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Tipo de Contrato</h3>
    </div>
    <br>
    
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Tipo preço do Contrato</th>
                            <th>ANS</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $consulta_tipo_cobertura, SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                        <td>".$row['PrecoContrato_cd'].                                    "</td>
                        <td>".$row['PrecoContrato_ds'].                                    "</td>
                        <td>".$row['PrecoContrato_RefANS'].                                    "</td></tr>";}
                      ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>


    <?php include '../html/footer.html' ?>

