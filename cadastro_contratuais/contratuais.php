<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>


<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Cadastro Empresas</h3>
            </div>
        </div>
    </div>

    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <table class="table">

                                          <tbody>
                                            <tr>
                                              <th scope="row">1</th>
                                              <td>Empresa</td>
                                              <td ><a href="cadastro_empresas.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">2</th>
                                              <td>Grupo de Empresa</td>
                                              <td><a href="grupos_empresa.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">3</th>
                                              <td>Cobertura de Empresa</td>
                                              <td><a href="cobertura_empresa.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">4</th>
                                              <td>Observações de Empresa</td>
                                              <td><a href="observacao_empresa.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">5</th>
                                              <td>Setores de Empresa</td>
                                              <td><a href="cadastro_setor_empresa.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">6</th>
                                              <td>Grupo de Cobertura</td>
                                              <td><a href="grupo_cobertura.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">7</th>
                                              <td>Tabela de Preços</td>
                                              <td><a href="cadastro_tabela_preco.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">8</th>
                                              <td>Tipo Contrato</td>
                                              <td><a href="cadastro_tipo_contrato.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">9</th>
                                              <td>Operadoras de Repasse / ADM. Benefícios</td>
                                              <td><a href="cadastro_operadora_repasse.php"><button class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">10</th>
                                              <td>Região</td>
                                              <td><a href="cadastro_regioes.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <!--<tr>
                                              <th scope="row">11</th>
                                              <td>Replicação de Limites de Uso</td>
                                              <td><a href="cadastro_repli_lim_uso.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>-->
                                          <tr>
                                              <th scope="row">11</th>
                                              <td>Forma de Pagamento por serviço prestado</td>
                                              <td><a href="cadastro_cob_sev_prestados.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- // Basic multiple Column Form section end -->
</div>



<?php include '../html/footer.html'?>