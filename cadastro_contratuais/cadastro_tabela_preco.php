<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php';
include '../consultas/grupo_empresa.php';
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}

?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Tabela Preço</h3>
    </div>
    <br>
    <!-- // Basic multiple Column Form section start -->
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Cógigo</th>
                            <th>Descrição</th>
                            <th>Valor Mensal</th>
                            <th>Tipo Ajuste</th>
                            <th>Tipo Remuneração</th>
                            <th>Codigo Plano</th>
                            <th>Nova Faixa</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                        while( $row = sqlsrv_fetch_array( $consulta_tp_contrato, SQLSRV_FETCH_ASSOC) ) {
                        echo "
                        <tr>
                            <td>".$row['TAB_Cd'].                                               "</td>
                            <td>".$row['TAB_ds'].                                               "</td>
                            <td>".$row['TAB_VlrMensal'].                                        "</td>
                            <td>".$row['TAB_TipoAjuste'].                                       "</td>
                            <td>".$row['TAB_TipoRemuneracao'].                                  "</td>
                            <td>".$row['TAB_CodPlano'].                                         "</td>
                            <td>".$row['Tab_NovaFaixa'].                                        "</td>
                            <td>".$row['TAB_Status'].                                           "</td>
                            <td><a href='exibe_info_tabela_preco.php?cod=".$row['TAB_Cd']."'>
                            <button class='btn btn-primary me-1 mb-1'>Visualizar</button></a>   </td>
                        </tr>";}
                      ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- // Basic multiple Column Form section end -->
    </div>

    

    <?php include '../html/footer.html'?>