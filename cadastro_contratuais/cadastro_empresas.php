<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Cadastro Empresa</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                    <div class="row">
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Pessoa</label>
                                                <select id="options" name="pessoa" class="form-control" onchange="verifica(this.value)">
                                                    <option value="Todos">Todos</option>
                                                    <option value="Igual">Igual</option>
                                                    <option value="Maior Igual">Maior Igual</option>
                                                    <option value="Menor Igual">Menor Igual</option>
                                                    <option value="Diferente">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" name="cod_pessoa" id="input" onkeypress="mascara(this,cod)" maxlength="19"  class="form-control" placeholder="Entre com o código" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Desc Empresa</label>
                                                <select id="options" class="form-control" name="descricao" onchange="verifica_desc(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Começa com</option>
                                                    <option value="Palavra Chave">Palavra Chave</option>
                                                    <option value="Igual">Igual</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" id="input2" maxlength="19" name="cod_descricao" class="form-control" placeholder="Entre com o código" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Mês de reajuste</label>
                                                <select class="form-control" name="mes_reajuste">
                                                    <option> </option>
                                                    <option value="1">01 - Janeiro</option>
                                                    <option value="2">02 - Fevereiro</option>
                                                    <option value="3">03 - Março</option>
                                                    <option value="4">04 - Abril</option>
                                                    <option value="5">05 - Maio</option>
                                                    <option value="6">06 - Junho</option>
                                                    <option value="7">07 - Setembro</option>
                                                    <option value="8">08 - Agosto</option>
                                                    <option value="9">09 - Setembro</option>
                                                    <option value="10">10 - Outubro</option>
                                                    <option value="11">11 - Novembro</option>
                                                    <option value="12">12 - Dezembro</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Cidade Empresa</label>
                                                <select class="form-control" name="cidade" onchange="verifica_cidade(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Começa com</option>
                                                    <option value="Palavra Chave">Palavra Chave</option>
                                                    <option value="Igual">Igual</option>
                                                    <option value="Diferente">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" name="cidade_nome" id="input3" maxlength="19" class="form-control" placeholder="Entre com a cidade" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Fantasia Empresa</label>
                                                <select class="form-control" name="fantasia" onchange="verifica_fantasia(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Começa com</option>
                                                    <option value="Palavra Chave">Palavra Chave</option>
                                                    <option value="Igual">Igual</option>
                                                    <option value="Diferente">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" name="fantasia_nome" id="input4" maxlength="19" class="form-control" placeholder="Entre com o nome Fantasia" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Contrato</label>
                                                <input type="text" class="form-control" placeholder="" name="contrato">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option>Todas</option>
                                                    <option>Ativas</option>
                                                    <option>Inativas</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Ordenar</label>
                                                <select class="form-control" name="ordenar">
                                                    <option>Código</option>
                                                    <option>Empresa</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Tipo</label>
                                                <select class="form-control" name="tipo_pessoa">
                                                    <option value="">Todas</option>
                                                    <option value="F">Física</option>
                                                    <option value="J">Jurídica</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Tipo Cobrança</label>
                                                <select class="form-control" name="tipo_cobranca">
                                                    <option value="">Todos</option>
                                                    <option value="E">Empresa</option>
                                                    <option value="U">Usuario</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

 
        <?php include '../consultas/consulta_cadastro_empresa.php';?>

        <section class="section">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Empresa</th>
                                <th>Fantasia</th>
                                <th>Endereço</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                if (!empty($consulta_cad_emp)) {
                                    while( $row = sqlsrv_fetch_array( $consulta_cad_emp, SQLSRV_FETCH_ASSOC) ) {
                                        echo "<tr>
                                        <td>".$row['EMP_cd'].                                       "</td>
                                        <td>".$row['EMP_ds'].                                       "</td>
                                        <td>".$row['Emp_Fantasia'].                                 "</td>
                                        <td>".$row['EMP_endereco'].                                 "</td>
                                        <td><a href='cadastro_empresa_completo.php?cod=".$row['EMP_cd']."'><button class='btn btn-primary me-1 mb-1'>Visualizar</button></a></td></tr>";
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    

    <?php include '../html/footer.html'?>