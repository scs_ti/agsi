<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php';
include '../consultas/consulta_cadastro_empresa_full.php';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Cadastro Empresa</h3>
    </div><br>

    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                
                            <?php
                                while( $row = sqlsrv_fetch_array($consulta_empresa_full, SQLSRV_FETCH_ASSOC) ) {
                                echo "
                                <div class='row'>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Código</label>
                                        <input type='text' class='form-control' disabled value=".$row['EMP_cd'].">
                                    </div>
                                    <div class='form-group col-md-5 col-12'>
                                        <label>Nome Empresa</label>
                                        <input type='text' value='".$row['EMP_ds']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-5 col-12'>
                                        <label>Nome Fantasia</label>
                                        <input type='text' value='".$row['Emp_Fantasia']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-5 col-12'>
                                        <label>Endereço</label>
                                        <input type='text' value='".$row['EMP_endereco']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-3 col-12'>
                                        <label>CEP</label>
                                        <input type='text' value='".$row['EMP_CEP']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Bairro</label>
                                        <input type='text' value='".$row['EMP_bairro']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Cidade</label>
                                        <input type='text' value='".$row['EMP_cidade']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Estado</label>
                                        <input type='text' value='".$row['EMP_UF']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>CNPJ</label>
                                        <input type='text' value='".$row['EMP_CGC']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>CPF</label>
                                        <input type='text' value='".$row['EMP_CPF']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Fone 1</label>
                                        <input type='text' value='".$row['EMP_fone1']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Fone 2</label>
                                        <input type='text' value='".$row['EMP_fone2']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Email</label>
                                        <input type='text' value='".$row['EMP_mail']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Descrição Complementar</label>
                                        <input type='text' value='".$row['Emp_DescricaoComp']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Pessoa</label>
                                        <input type='text' value='".$row['EMP_PESSOA']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Fator Moderado</label>
                                        <input type='text' value='".$row['EMP_CobFatorModerado']."' readonly='readonly' class='form-control'>
                                    </div>
                                    
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Data Início</label>
                                        <input type='text' value='".$row['EMP_dtInicio']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Data Cadastro</label>
                                        <input type='text' value='".$row['EMP_DtCadastro']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Região</label>
                                        <input type='text' value='".$row['EMP_Regiao']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Tipo Contrato</label>
                                        <input type='text' value='".$row['EMP_TipoContrato']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-4 col-12'>
                                        <label>Cobrança</label>
                                        <input type='text' value='".$row['EMP_TipoCobranca']."' readonly='readonly' class='form-control'>
                                    </div>

                                </div>";}
                            ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



<?php include '../html/footer.html' ?>