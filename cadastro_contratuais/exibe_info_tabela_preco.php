<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 d-flex col-md-6 order-md-1">
                <a href="javascript:history.back()" style="color: white;"><button class="btn btn-primary me-1 mb-1">Voltar</a></button>
                <h3>Tabela Preço</h3>
            </div>
        </div>      
    </div>
    <br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                <div class="row">
                                    <?php while( $row = sqlsrv_fetch_array( $sql_info_tab, SQLSRV_FETCH_ASSOC) ) {
                                            $_SESSION['cod'] = $row['C_CTRUSU'];
                                            echo " <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Código</label>
                                                            <input type='text' value='".$row['TAB_Cd']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Descrição</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['Tab_ds']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Plano</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['TAB_CodPlano']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Descrição</label>
                                                            <input type='text' value='".$row['PLA_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Status</label>
                                                            <input type='text' value='".$row['TAB_Status']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>   
                                                  ";
                                          }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                <div class="row">
                                    <?php while( $row = sqlsrv_fetch_array( $sql_info_preco, SQLSRV_FETCH_ASSOC) ) {
                                            $_SESSION['cod'] = $row['C_CTRUSU'];
                                            echo " 
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Faixa</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['FAI_Cd']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-3 col-12'>
                                                        <div class='form-group'>
                                                            <label>Intervalo</label>
                                                            <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                            value='".$row['FAI_Intervalo']."' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Valor Titular</label>
                                                            <input type='text' value='".$row['FAI_VlrTitular']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Valor Agregado</label>
                                                            <input type='text' value='".$row['FAI_VlrAgregado']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-2 col-12'>
                                                        <div class='form-group'>
                                                            <label>Valor Dependente</label>
                                                            <input type='text' value='".$row['FAI_VlrDependente']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                            placeholder='Insira o Nome' name='fname-column'>
                                                        </div>
                                                    </div>    
                                                  ";
                                          }
                                    ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html'?>