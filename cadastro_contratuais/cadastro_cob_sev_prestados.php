<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../consultas/grupo_empresa.php';
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}

?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><div><button class="btn btn-primary me-1 mb-1">Voltar</a></button></div>
        <h3>Formas de Pagamento por Serviço Prestado</h3>
    </div>
    <br>
    <!-- // Basic multiple Column Form section start -->
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Forma de Cobrança</th>
                            <th>Descrição</th>
                            <th>Código Empresa</th>
                            <th>Empresa</th>
                            <th>Tipo</th>
                            <th>Calculo</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                        while( $row = sqlsrv_fetch_array( $sql_form_cob_serv, SQLSRV_FETCH_ASSOC) ) {
                        echo "
                        <tr>
                            <td>".$row['Dif_ServPrestFormas'].                                               "</td>
                            <td>".$row['SPF_ds'].                                               "</td>
                            <td>".$row['SPF_Empresa'].                                        "</td>
                            <td>".$row['SPF_Tipo'].                                       "</td>
                            <td>".$row['SPF_Calculo'].                                  "</td>
                           
                            <td><a href='exibe_cad_cob_sevico.php?cod=".$row['Dif_ServPrestFormas']."'>
                            <button class='btn btn-primary me-1 mb-1'>Visualizar</button></a>   </td>
                        </tr>";}
                      ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- // Basic multiple Column Form section end -->
    </div>

    

    <?php include '../html/footer.html'?>