<?php
$cod			=	$_POST['cod'];
$nome			=	$_POST['nome'];
$regiao			=	$_POST['regiao'];
$rz_social		=	$_POST['rz_social'];
$cidade			=	$_POST['cidade'];
$categoria		=	explode(' - ', $_POST['categoria']);
$cnpj			=	$_POST['cnpj'];
$cpf			=	$_POST['cpf'];
$email			=	$_POST['email'];
$lc_atendimento	=	$_POST['lc_atendimento'];
$status			=	$_POST['status'];
$emite_guia		=	$_POST['emite_guia'];
$pessoa			=	$_POST['pessoa'];
$nr_registro	=	$_POST['nr_registro'];
$env_guia_med	=	$_POST['env_guia_med'];
$quali_esp		=	$_POST['quali_esp'];
$empresa		=	$_POST['empresa'];
#$plano			=	$_POST['plano'];
$procedimento	=	$_POST['procedimento'];
$especialidade	=	$_POST['especialidade'];


if (strlen($cod) > 0) {
	$vl_cod = "and Forn.Fornecedor_cd = ".$cod."";
}else{
	$vl_cod = "";
}

if (strlen($especialidade) > 0) {
	$vl_especialidade = "and E.Especialidade_ds LIKE  '".$especialidade."%'";
}else{
	$vl_especialidade = ""; 
}

if (strlen($nome) > 0) {
	$vl_nome = "and Forn.Fornecedor_ds LIKE UPPER('%".$nome."%')";
}else{
	$vl_nome = "";
}

if (strlen($regiao) > 0) {
	$vl_regiao = "and Fornecedor_Regiao = ".$regiao."";
}else{
	$vl_regiao = "";
}

if (strlen($rz_social) > 0) {
	$vl_rz_social = "and Fornecedor_RazSocial LIKE '%".$rz_social."%'";
}else{
	$vl_rz_social = "";
}

if (strlen($cidade) > 0) {
	$vl_cidade = "and CCred.Cid_ds LIKE UPPER('%".$cidade."%')";
}else{
	$vl_cidade = "";
}

if (isset($categoria[1])) {
	$vl_categoria = "and Tipo.TFornece_ds LIKE '%".$categoria[1]."%'";
}else{
	$vl_categoria = "";
}

if (strlen($cnpj) > 0) {
	$vl_cnpj = "and Fornecedor_CNPJ = '".$cnpj."'";
}else{
	$vl_cnpj = "";
}

if (strlen($cpf) > 0) {
	$vl_cpf = "and Fornecedor_CPF = '".$cpf."'";
}else{
	$vl_cpf = "";
}

if (strlen($email) > 0) {
	$vl_email = "and Fornecedor_email LIKE UPPER('".$email."')";
}else{
	$vl_email = "";
}

if (strlen($lc_atendimento) > 0) {
	$vl_lc_atendimento = "and ( Select count(*) from Forn_LocalAtendto where Local_Fornecedor = Forn.Fornecedor_cd and Local_CodLocal = ".$lc_atendimento.") > 0";
}else{
	$vl_lc_atendimento = "";
}

if ($status == 'A' or $status == 'I' or $status == 'S' or $status == 'E') {
	$vl_status = "and Forn.Fornecedor_Status = '".$status."'";
}else{
	$vl_status = "";
}

if ($emite_guia == 'S' or $emite_guia == 'N') {
	$vl_emite_guia = "and Forn.Fornecedor_EmiteGuia = '".$emite_guia."'";
}else{
	$vl_emite_guia = "";
}

if ($pessoa == 'F' or $pessoa == 'J') {
	$vl_pessoa = "and Forn.Fornecedor_Pessoa = '".$pessoa."'";
}else{
	$vl_pessoa = "";
}

if (strlen($nr_registro) > 0) {
	$vl_nr_registro = "and FCred.Cre_CRM = '".$nr_registro."'";
}else{
	$vl_nr_registro = "";
}

if (strlen($empresa) > 0) {
	$vl_empresa = "and fe.FornEmpresa_Empresa = ".$empresa."";
}else{
	$vl_empresa = "";
}

#if (strlen($plano) > 0) {
#	$vl_plano = "and fp.Pla_Plano = ".$plano."";
#}else{
#	$vl_plano = "";
#}

if (strlen($procedimento) > 0) {
	$vl_procedimento = "
	and ( ( SELECT COUNT(*) FROM Forn_ProcAmb WHERE ProcAmb_Fornecedor = Fornecedor_cd and Procamb_Amb 
		= UPPER('".$procedimento."') ) > 0
	or ( SELECT COUNT(*) FROM Forn_ProcAmbSolicitar WHERE FSol_Fornecedor = Fornecedor_cd and FSol_ProcAmb 
		= UPPER('".$procedimento."') ) > 0
	or ( SELECT COUNT(*) FROM Forn_ProcAmbAutorizar WHERE FAut_Fornecedor = Fornecedor_cd  and FAut_ProcAmb
		=  UPPER('".$procedimento."') ) > 0 )
	";
}else{
	$vl_procedimento = "";
}


if ($env_guia_med == 'S' or $env_guia_med == 'N') {
	$vl_env_guia_med = "and Fornecedor_GuiaMedico = '".$env_guia_med."'";
}else{
	$vl_env_guia_med = "";
}

if (strlen($quali_esp) > 0 and strlen($especialidade) < 0) {
	$vl_quali_esp = "and Fornecedor_cd in ( 
 select distinct(FE.Esp_Fornecedor) from Forn_Especialidade FE 
 inner join Forn_Qualificacao FQ on FE.Esp_Especialidade = FQ.Qua_Especialidade and FE.Esp_Fornecedor = FQ.Qua_Fornecedor 
 inner join Qualificacao Q on FQ.Qua_Qualificacao = Q.Qualificacao_cd 
 and Qualificacao_cd = ".$quali_esp.")";
}elseif(strlen($quali_esp) > 0 and strlen($especialidade) > 0) {
	$vl_quali_esp = "and  E.Especialidade_cd in ( 
 select distinct(Qua_Especialidade) from Forn_Qualificacao FQ 
 inner join Qualificacao Q on FQ.Qua_Qualificacao = Q.Qualificacao_cd 
 and Qualificacao_cd = ".$quali_esp."
 and Qua_Fornecedor = Forn.Fornecedor_cd)";
}elseif(strlen($quali_esp) <= 0) {
	$vl_quali_esp = " ";
}

$sql_completo = "SELECT DISTINCT                                                                                                                       
			Fornecedor_cd,                                                                                                                 
			Fornecedor_ds,                                                                                                                 
			Fornecedor_end,                                                                                                                
			Fornecedor_bairro,                                                                                                             
			Fornecedor_cep,                                                                                                                
			CCred.Cid_ds,                                                                                                                  
			CCred.Cid_CodIBGE,                                                                                                             
			Fornecedor_uf,                                                                                                                 
			Fornecedor_cnpj,                                                                                                               
			Fornecedor_cpf,                                                                                                                
			Fornecedor_TipoIrrf,                                                                                                           
			Fornecedor_Fone1,                                                                                                              
			Fornecedor_Fone2,                                                                                                              
			Fornecedor_Celular,                                                                                                            
			Fornecedor_Fax,                                                                                                                
			Fornecedor_RG,                                                                                                                 
			Fornecedor_email,                                                                                                              
			CC.CC_Banco,                                                                                                                   
			Fornecedor_Banco,                                                                                                              
			CC.CC_Agencia,                                                                                                                 
			Fornecedor_Agencia,                                                                                                            
			CC.CC_Cd,                                                                                                                      
			Fornecedor_Conta,                                                                                                              
			Fornecedor_EmiteCheque,                                                                                                        
			Fornecedor_TipoFor,                                                                                                            
			Fornecedor_InsMunic,                                                                                                           
			Fornecedor_RazSocial,                                                                                                          
			Fornecedor_Status,                                                                                                             
			CASE WHEN CONVERT(VARCHAR,Fornecedor_TipoCredenciamento) = '1' THEN 'Próprio'                                              
				  WHEN CONVERT(VARCHAR,Fornecedor_TipoCredenciamento) = '2' THEN 'Cooperado'                                            
			     WHEN CONVERT(VARCHAR,Fornecedor_TipoCredenciamento) = '3' THEN 'Conveniado' END Fornecedor_TipoCredenciamento,        
			Fornecedor_CtaCtabil,                                                                                                          
			Fornecedor_Pessoa,                                                                                                             
			Fornecedor_Desconto,                                                                                                           
			Fornecedor_VlrLimiteDesconto,                                                                                                  
			Fornecedor_Inss,                                                                                                               
			Fornecedor_Pis,                                                                                                                
			Fornecedor_Contato,                                                                                                            
			Fornecedor_Logradouro,                                                                                                         
			Fornecedor_IE,                                                                                                                 
			Fornecedor_DtNasc,                                                                                                             
			CASE WHEN Fornecedor_EstCivil = 'C' THEN 'CASADO'                                                                          
			     WHEN Fornecedor_EstCivil = 'S' THEN 'SOLTEIRO'                                                                        
			     WHEN Fornecedor_EstCivil = 'V' THEN 'VIÚVO'                                                                           
			     WHEN Fornecedor_EstCivil = 'A' THEN 'AMASIADO'                                                                        
			     WHEN Fornecedor_EstCivil = 'D' THEN 'DIVORCIADO'                                                                      
			     WHEN Fornecedor_EstCivil = 'E' THEN 'DESQUITADO' END  Fornecedor_EstCivil,                                            
			Fornecedor_LiminarCofins,                                                                                                      
			For_EnviaWEB,                                                                                                                  
			FCred.Cre_Inss,                                                                                                                
			FCred.Cre_Iss,                                                                                                                 
			Fornecedor_ManualPortal,                                                                                                       
			Fornecedor_TipoContra,                                                                                                         
			Cus_cd,                                                                                                                        
			Cus_ds,                                                                                                                        
			Fornecedor_EndComp,                                                                                                            
			Fornecedor_sexo,                                                                                                               
			Fornecedor_Banco Banco,                                                                                                        
			Fornecedor_Agencia Agencia,                                                                                                    
			Fornecedor_Conta Conta                                                                                                         
 ,CONVERT(INTEGER,NULL) CodEsp, CONVERT(VARCHAR(40),NULL) Especialidade,CONVERT(char(5),NULL) Qualificacao                           
			,FCred.Cre_DtIniContrato DtIniContrato,                                                                                        
			FCred.Cre_DtFinContrato DtFinContrato,                                                                                          
       Fornecedor_GuiaMedico,                                                                                                            
  (SELECT TOP 1 ForRe_DiaReaj FROM Fornecedor_Reajuste WHERE ForRe_Credenciado = Fornecedor_cd ORDER BY ForRe_Data DESC) UltimoDiaReajuste, 
  (SELECT TOP 1 ForRe_Percentual FROM Fornecedor_Reajuste WHERE ForRe_Credenciado = Fornecedor_cd ORDER BY ForRe_Data DESC) Percentual,     
  (SELECT TOP 1 ForRe_Operador FROM Fornecedor_Reajuste WHERE ForRe_Credenciado = Fornecedor_cd ORDER BY ForRe_Data DESC) Operador          
 FROM Fornecedor Forn                                                                                                                  
  LEFT OUTER JOIN TipoFornece Tipo ON Tipo.TFornece_cd = Forn.Fornecedor_TipoFor                                                       
  LEFT OUTER JOIN Conta_Corr CC ON CC.CC_Codigo = Forn.Fornecedor_ContaCrr                                                             
  LEFT OUTER JOIN Forn_Credenciados FCred ON Forn.Fornecedor_cd = FCred.Cre_Fornecedor                                                 
  LEFT OUTER JOIN Cidade CCred ON Forn.Fornecedor_CodCidade = CCred.Cid_cd                                                             
  LEFT OUTER JOIN CentroCusto ON Forn.Fornecedor_CentroCusto = Cus_cd                                                                  
   LEFT OUTER JOIN Forn_Especialidade ON Forn_Especialidade.Esp_Fornecedor = Forn.Fornecedor_cd                                      
   LEFT OUTER JOIN Especialidade E ON E.Especialidade_cd = Forn_Especialidade.Esp_Especialidade                                      
   LEFT OUTER JOIN Forn_Qualificacao on Esp_Especialidade = Qua_Especialidade and Esp_Fornecedor = Qua_Fornecedor                    
   LEFT OUTER JOIN Qualificacao Q on Qua_Qualificacao = Q.Qualificacao_cd                                                            
   LEFT OUTER JOIN Forn_Empresa Fe ON Forn.Fornecedor_cd = Fe.FornEmpresa_Fornecedor                                                 
   LEFT OUTER JOIN Empresa Emp ON Fe.FornEmpresa_Empresa = Emp.Emp_cd                                                                
WHERE Forn.Fornecedor_cd is not null  ".$vl_empresa." ".$vl_cod." ".$vl_cidade." ".$vl_nome." ".$vl_categoria." ".$vl_nr_registro." ".$vl_especialidade." ".$vl_regiao." ".$vl_status." ".$vl_pessoa." ".$vl_emite_guia." ".$vl_procedimento." ".$vl_lc_atendimento." ".$vl_email." ".$vl_quali_esp." ".$vl_cnpj." ".$vl_cpf." ".$vl_rz_social." ".$vl_env_guia_med."ORDER BY Forn.Fornecedor_ds
";


$completo = sqlsrv_query($conn, $sql_completo);
    if( $sql_completo === false) {
        die( print_r( sqlsrv_errors(), true) );
    }  





?>



                                                                                                   
