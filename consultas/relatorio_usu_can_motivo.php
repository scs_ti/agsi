<?php

include '../assets/conn.php';

$tipo_relatorio 	= $_POST['tipo_relatorio'];
$empresa			= $_POST['empresa'];
$empresa_ini_cod 	= $_POST['empresa_ini_cod'];
$empresa_fim_cod	= $_POST['empresa_fim_cod'];
$usu 				= $_POST['usu'];
$cod_usuario 		= $_POST['live_search'];
$cancelamento 		= $_POST['cancelamento'];
$mot_cancela 		= $_POST['mot_cancela'];
$periodo 			= $_POST['periodo'];
$cancelamento_ini 	= $_POST['cancelamento_ini'];
$cancelamento_fim 	= $_POST['cancelamento_fim'];
$empresa_pessoa 	= $_POST['empresa_pessoa'];
$tipo_usuario 		= $_POST['tipo_usuario'];
$parentesco 		= $_POST['parentesco'];

if (strlen($empresa_ini_cod) > 0) {
	$vl_empresa = "and c_EmpUsu between ".$empresa_ini_cod. " and " .$empresa_fim_cod."";
}else{
	$vl_empresa = "";
}

if (strlen($cod_usuario) > 0) {
	$vl_cod_usuario = "and C_CtrUsu = '".$cod_usuario."'";
}else{
	$vl_cod_usuario = "";
}

if (strlen($cancelamento_ini) > 0){
	if (strlen($cancelamento_fim) <= 0) {
		$vl_data = "and c_DteUsu BETWEEN '".$cancelamento_ini."' and '1899-12-30' ";
	}else{
		$vl_data = "and c_DteUsu BETWEEN '".$cancelamento_ini."' and '".$cancelamento_fim."' ";	
	}
}else{
	$vl_data = "";
}

if ($empresa_pessoa != "Todas") {
	$vl_emp_pessoa = "and Emp_Pessoa = '".$empresa_pessoa."'";
}

if ($tipo_usuario == "T") {
	$vl_tipo_usuario = "and c_TipUsu = 'T'";
}elseif ($tipo_usuario == "U") {
	$vl_tipo_usuario = "and c_TipUsu <> 'T'";
}

if ($parentesco == "T") {
	$vl_parentesco = "and C_ParUsu = 'T'";
}elseif ($parentesco == "D") {
	$vl_parentesco = "and C_TipUsu = 'F'";
}elseif ($parentesco == "A") {
	$vl_parentesco = "and C_ParUsu = 'A'";
}

if (strlen($mot_cancela) > 0) {
	$vl_mot_cancela = "and c_McaUsu = ".$mot_cancela."";
}else{
	$vl_mot_cancela = "";
}

$sql = "
	SELECT Distinct C_CtrUsu,    
     ( SELECT COUNT(*)           
       FROM contapagar           
          INNER JOIN T_ArqUsu T ON PG_USU_cd = T.C_CtrUsu  
       WHERE PG_statusBaixa = 'T'
             and T.C_EmpUsu = U.C_EmpUsu  
             and T.C_PlaUsu = U.C_PlaUsu   
             and T.C_CodUsu = U.C_CodUsu ) NroPar,   
     ( SELECT COUNT(*) 
       FROM contapagar 
          INNER JOIN T_ArqUsu T ON PG_USU_cd = T.C_CtrUsu    
       WHERE PG_statusBaixa = 'A'     
             and T.C_EmpUsu = U.C_EmpUsu    
             and T.C_PlaUsu = U.C_PlaUsu    
             and T.C_CodUsu = U.C_CodUsu ) NroParAbr,   
     ( SELECT COUNT(*)        
       FROM contapagar 
          INNER JOIN T_ArqUsu T ON PG_USU_cd = T.C_CtrUsu 
       WHERE PG_statusBaixa = 'C'    
             and T.C_EmpUsu = U.C_EmpUsu 
             and T.C_PlaUsu = U.C_PlaUsu 
             and T.C_CodUsu = U.C_CodUsu ) NroParCanc   
INTO TB_USERS        
FROM t_ArqUsu U  
     INNER JOIN Empresa ON c_Empusu = Emp_cd    
WHERE EMP_TipoCobranca = 'U'
   and c_DteUsu IS NOT NULL 
   ".$vl_data." ".$vl_mot_cancela." ".$vl_empresa." ".$vl_cod_usuario." ".$vl_emp_pessoa." ".$vl_tipo_usuario." 
UNION                                                       
SELECT Distinct  C_CtrUsu,                                   
            ( SELECT COUNT(*)                                
              FROM contapagar                                
              WHERE PG_statusBaixa = 'T'                   
                    and Pg_Empresa = C_EmpUsu ),            
            ( SELECT COUNT(*)                               
              FROM contapagar                                
              WHERE PG_statusBaixa = 'A'                  
                    and Pg_Empresa = C_EmpUsu ),             
            ( SELECT COUNT(*)                  
              FROM contapagar                 
              WHERE PG_statusBaixa = 'C'       
                    and Pg_Empresa = C_EmpUsu )
FROM T_ArqUsu                                  
     INNER JOIN Empresa ON c_Empusu = Emp_cd   
WHERE EMP_TipoCobranca = 'E'                   
   and c_DteUsu IS NOT NULL ".$vl_data." ".$vl_mot_cancela." ".$vl_empresa." ".$vl_cod_usuario." ".$vl_emp_pessoa." ".$vl_tipo_usuario." ".$vl_parentesco;


$sql_select_itens="
CREATE VIEW relat_cancelamento_usu AS
SELECT Titular.C_CtrUSU 'cod_titular',                                  
       Titular.C_nomUSU 'titular',                                          
       U.C_McaUsu 'cod_motivo',                                           
       MCan_ds 'descr_motivo',                                         
       U.C_Empusu 'cod_empresa',                                          
       Emp_ds 'descr_empresa',                                         
       U.C_ABRUSU 'cod_abrange',                                          
       U.C_Ctrusu 'codigo',                                             
       U.c_NomUsu 'nome',                                                 
       U.c_dteusu 'dt_termino',                                           
       U.c_FonUsu 'fone',                                                 
       U.c_EndUsu 'Endereco',                                             
       U.c_CidUsu 'cidade',                                               
       U.c_incUsu 'dt_inclusao',                                          
       REP_ds 'vendedor',                                               
       ( ( ( Year( U.c_dteusu ) - Year( U.c_incUsu ) ) * 12 ) +           
       ( Month( U.c_dteusu ) - Month( U.c_incUsu ) ) ) + 1 'n_meses',  
       NroPar 'n_parcelas_Pagas',                                      
       NroParAbr 'n_parcelas_aberto',                                  
       NroParCanc 'n_parcelas_canceladas',                             
       Convert( Money,U.c_VlmUsu ) 'valor',                               
       U.c_TipUsu 'Tipo',                                                 
        Emp_OperadorRepasse 'cod_op_repasse',                            
        ORep_ds 'op_repasse',                                             
        U.Usu_OperadorCanc 'op_cancelamento'                              
FROM t_ArqUsu U                                                           
     INNER JOIN TB_USERS T ON T.C_CtrUsu = U.C_CtrUsu                         
     INNER JOIN Empresa ON U.c_Empusu = Emp_cd                              
     LEFT  JOIN t_ArqUsu Titular ON Titular.c_CtrUsu = dbo.fTitular(U.c_CtrUsu) 
     LEFT  JOIN Mot_Cancelamento ON U.C_McaUsu = MCan_cd                    
     LEFT  JOIN representante ON rep_cd = U.usu_vendedor                    
     LEFT JOIN OperadorRepasse ON ORep_cd = Emp_OperadorRepasse               
";

sqlsrv_query($conn, $sql);
    if( $sql === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

sqlsrv_query($conn, $sql_select_itens);
    if( $sql_select_itens === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

?>