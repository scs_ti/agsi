<?php

$sql_contabilidade = 
"
SELECT cob_cd, C.Cob_CtaCtabil, x.Cta_ds as ct_contabil, Cob_CtaCtabilFM as ct_contabil_fm
, a.Cta_ds as ct_PPCNG, Cob_CtaCtabilPPCNG, y.Cta_ds as ct_contabil_receb,
Cob_CtaCtabilRecAnt, e.Cta_ds as tip_pessoa
FROM  cobrancaEMP C
INNER JOIN SipsSCSJC.dbo.PlanoContas a ON c.Cob_CtaCtabilFM = a.Cta_Cd
INNER JOIN SipsSCSJC.dbo.PlanoContas x ON c.Cob_CtaCtabil = x.Cta_Cd
INNER JOIN SipsSCSJC.dbo.PlanoContas y ON c.Cob_CtaCtabilPPCNG = y.Cta_Cd
INNER JOIN SipsSCSJC.dbo.PlanoContas e ON c.Cob_CtaCtabilRecAnt = e.Cta_Cd
WHERE C.cob_emp = '".$_GET['cod']."'
ORDER BY c.cob_emp
";

$sql_hist_at_preco =
"
SELECT DISTINCT
      emp_cd,
      emp_ds,
       PLA_CD,
       PLA_DS,
       HIST_NATUREZA,
       HIST_DATA,
       HIST_USUSISTEMA  
FROM USU_HISTPRECOS
  INNER JOIN T_ARQUSU ON C_CTRUSU = HIST_USUARIO
  INNER JOIN EMPRESA ON EMP_CD = C_EMPUSU
  INNER JOIN PLANO ON PLA_CD = C_PLAUSU
WHERE EMP_CD = '".$_GET['cod']."'
ORDER BY HIST_DATA DESC
";

$sql_grupo_empresa = 
"
SELECT grp_codigo Codigo, grp_descricao Grupo
FROM GrupoEmpresa
  INNER JOIN GrupoEmpresaItens ON gpi_grupo = grp_codigo
WHERE gpi_empresa= '".$_GET['cod']."'
";

$sql_dados_cobranca = 
"
SELECT 
       Age.Age_cd , 
       Age.Age_ds , 
       Ban.Ban_cd ,
       Ban.Ban_ds ,
       CC.CC_cd  ,
       CC.CC_ds 
FROM  cobrancaEMP CE
INNER JOIN Conta_Corr CC ON CE.Cob_ContaCrr = CC.CC_Codigo
INNER JOIN agencia Age ON Age.Age_cd = CC.cc_Agencia
INNER JOIN banco Ban ON Ban.Ban_cd = CC.cc_Banco 
WHERE cob_emp = '".$_GET['cod']."'
";

$sql_desc_cobranca = 
"
Select *
from EmpresaVencto
WHERE EVencto_Empresa = '".$_GET['cod']."'
";

$contabilidade = sqlsrv_query($conn, $sql_contabilidade);
if( $sql_contabilidade === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$hist_at_preco = sqlsrv_query($conn, $sql_hist_at_preco);
if( $sql_hist_at_preco === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$grupo_empresa = sqlsrv_query($conn, $sql_grupo_empresa);
if( $sql_grupo_empresa === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$dados_cobranca = sqlsrv_query($conn, $sql_dados_cobranca);
if( $sql_dados_cobranca === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$desc_cobranca = sqlsrv_query($conn, $sql_desc_cobranca);
if( $sql_desc_cobranca === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>