<?php

$sql_motivo_carteirinha = "Select UCM_CD, UCM_DS 
from Usu_MotivoCarteirinha
Where UCM_CD is not null
";

$sql_vinculo_beneficiario = "
SELECT *
FROM VinculoBenef
WHERE VB_cd is not null
ORDER BY CONVERT(INT, VB_cd), VB_ds
";

$sql_motivo_cancelamento =
"
SELECT *
FROM Mot_Cancelamento
WHERE MCan_cd is not null
";

$sql_motivo_bloqueio = "
SELECT *
FROM Motivo_Bloqueio
WHERE Bloqueamento is not null
ORDER BY Bloqueamento, Blo_Motivo
";

$sql_profissao = "
SELECT Profissao_cd,Profissao_ds
FROM Profissao
Where Profissao_cd is not null
ORDER BY Profissao_cd
";


$sql_indicacao_usuario_full = 
"
SELECT                                                                             
   Guitem_AMB,                                                                     
   Guitem_procedimento,                                                            
   c_descAMB ,                                                                     
   SUM(Guitem_qtde) ,                                                              
   GUI_CID ,                                                                       
   Gui_emissao,                                                                    
   Gui_Atendto,                                                                    
   P.fornecedor_ds,                                                                
   Especialidade_ds                                                                
FROM T_ARQUSU                                                                      
    LEFT JOIN GUIA ON GUI_USUARIO = C_CTRUSU                                       
    LEFT JOIN ITENSGUIA ON GUITEM_GUIA = GUI_CD                                    
    LEFT JOIN Proc_Guia ON ProcGuia_cd = Gui_tipo                                  
    LEFT JOIN AMB A ON GUITEM_PROCEDIMENTO = A.C_CODAMB AND GUITEM_AMB = A.C_TIPAMB
    LEFT OUTER JOIN CLASSEAMB ON A.C_CLAAMB = CLASSE_CD                            
    LEFT JOIN GRUPO ON Amb_Grupo = GRU_cd                                          
    LEFT JOIN fornecedor P on fornecedor_cd = Guitem_Prestador                     
    LEFT JOIN especialidade ON Especialidade_cd = Gui_especialidade                
WHERE (dbo.fStatusUsuario(C_IncUsu, C_DteUsu, GETDATE()) = 'A')                  
   AND C_CTRUSU = '".$_GET['cod']."'
GROUP BY                                       
   Guitem_procedimento,       
   Guitem_AMB,                
   c_descAMB,                 
   GUI_CID,                   
   Gui_emissao,               
   Gui_Atendto,               
   P.fornecedor_ds,           
   Especialidade_ds           
HAVING SUM(Guitem_qtde)  > 0
";


$sql_obs_usu_full = "
SELECT Aler_cd, Aler_Usuario, Aler_Emp, Aler_Pla, Aler_Alerta, Aler_DtInicial,
              Aler_DtFinal, Aler_Eterno, Aler_Tipo,  Aler_Bloquear 
FROM Usu_Alerta
Where Aler_cd = '".$_GET['cod']."'
ORDER BY Aler_Usuario
";

$motivo_carteirinha = sqlsrv_query($conn, $sql_motivo_carteirinha);
if( $sql_motivo_carteirinha === false) {
die( print_r( sqlsrv_errors(), true) );
}

$vinculo_beneficiario = sqlsrv_query($conn, $sql_vinculo_beneficiario);
if( $sql_vinculo_beneficiario === false) {
die( print_r( sqlsrv_errors(), true) );
}

$motivo_cancelamento = sqlsrv_query($conn, $sql_motivo_cancelamento);
if( $sql_motivo_cancelamento === false) {
die( print_r( sqlsrv_errors(), true) );
}

$motivo_bloqueio = sqlsrv_query($conn, $sql_motivo_bloqueio);
if( $sql_motivo_bloqueio === false) {
die( print_r( sqlsrv_errors(), true) );
}

$profissao = sqlsrv_query($conn, $sql_profissao);
if( $sql_profissao === false) {
die( print_r( sqlsrv_errors(), true) );
}

$indicacao_usuario_full = sqlsrv_query($conn, $sql_indicacao_usuario_full);
if( $sql_indicacao_usuario_full === false) {
die( print_r( sqlsrv_errors(), true) );
}


$obs_usu_full = sqlsrv_query($conn, $sql_obs_usu_full);
if( $sql_obs_usu_full === false) {
die( print_r( sqlsrv_errors(), true) );
}
?>

