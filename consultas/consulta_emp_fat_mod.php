<?php
error_reporting(E_ERROR | E_PARSE);
include '../assets/conn.php';

$dia = date("d");
$mes = date("m");
$ano = date("y");
$data = $mes."/".$dia."/20".$ano;

if ($_POST['tipo_relatorio'] != 'A') {
	$sql = "SELECT Emp_cd 'Código Empresa',                                    
			       Emp_ds 'Desc Empresa',                                      
			       Sum(Nullif( FM_Qtde, 1)) 'Quantidade',                      
			       Sum(Nullif(FM.FM_Valor,0)) / Sum(Nullif(FM.FM_Qtde, 1)) Unit, 
			       SUM(IsNull( FM_Valor, 0)) Total                               
			INTO TempRelFM_".$_SESSION['usuario']."
			FROM FatorModerador FM                                                                    
			   INNER JOIN Guia g ON FM_Guia = g.Gui_cd                                                
			   INNER JOIN t_ArqUsu u ON Gui_Usuario = c_CtrUsu                                        
			   LEFT OUTER JOIN ItensGuia it ON ( g.Gui_cd=it.Guitem_guia and it.Guitem_Seq = FM_Seq ) 
			   INNER JOIN Empresa ON u.c_EmpUsu = Emp_cd                                              
			   INNER JOIN Amb ON FM_Amb = c_TipAmb and FM_Procedimento = c_Codamb                     
			   LEFT JOIN Fornecedor f ON Guitem_Prestador = f.Fornecedor_cd                           
			   LEFT JOIN Fornecedor fLocal ON it.Guitem_Local = fLocal.Fornecedor_cd                  
			   LEFT JOIN Especialidade ON g.Gui_Especialidade = Especialidade_cd                      
			   LEFT JOIN Proc_Guia ON g.Gui_Tipo = ProcGuia_cd                                        
			   LEFT JOIN t_ArqUsu TIT ON LEFT(TIT.c_CtrUsu,13) = LEFT((CASE WHEN U.Usu_TitCobranca is Null THEN U.c_CtrUsu ELSE U.Usu_TitCobranca END),13) and TIT.C_PARUSU = 'T' 
			   LEFT JOIN T_ARQUSU TitCob ON TitCob.C_CTRUSU = u.Usu_TitCobranca                       
			   LEFT JOIN Contapagar CP ON CP.PG_CD = FM.FM_Duplicata                                  
			             and CP.PG_sequencia = FM.FM_SeqDup and CP.Pg_ContaCrr = FM.FM_ContaCrrDup       
			WHERE FM_Guia is not null  ";
}

if ($_POST['tipo_relatorio'] = 'A') {
	$sql = "SELECT Emp_cd 'Codigo Empresa', Emp_ds 'Desc Empresa',                   
       TIT.C_ctrusu as 'Cod.Titular',                                    
       TIT.c_NomUsu as 'Nome Titular',                                   
      u.C_CtrUsu 'Código',                                               
      dbo.fDataVencto(CP.PG_Vencto,CP.Pg_Mes) Vencimento,                
      SUBSTRING( u.c_NomUsu,1,30 ) 'Nome Usuário',                       
      u.Usu_Interno 'Código Interno',                                    
      FM_Amb 'AMB',                                                      
      FM_Procedimento 'FM Procedimento',                                 
      SUBSTRING( c_DescAmb,1,40 ) 'Descrição AMB',                       
      g.Gui_Atendto 'Atendimento',                                       
      g.Gui_Liberacao 'Liberação',                                       
      g.Gui_Emissao 'Emissão',                                           
      g.Gui_Especialidade 'Especialidade',                               
      SUBSTRING( Especialidade_ds,1,20 ) 'Especialidade Desc',           
      g.Gui_Tipo 'Tipo Guia',                                            
      SUBSTRING( ProcGuia_ds,1,15 ) 'Proc Desc',                         
      g.Gui_cd 'Cod. Guia', f.Fornecedor_ds 'Desc Fornecedor',           
      CONVERT(MONEY,IsNull(FM_Valor,0)) 'Valor Fator',                   
      IsNull(FM_Qtde, 1) 'Quantidade',                                   
      CONVERT(MONEY,IsNull(FM_Valor,0)) Total,                           
      G.Gui_Operator AS Operador,                                        
      G.Gui_OBS AS OBS,                                                  
      u.Usu_TitCobranca AS 'Cód. Titular de Cobrança',                   
      TitCob.C_NOMUSU AS 'Nome Titular de Cobrança',                     
      TIT.c_CicUsu as 'CPF Titular',                                     
      ( SELECT TOP 1 gfm_ds                                              
        FROM GrupoFMItens                                                
          INNER JOIN GRUPOFM ON gfm_cd = gfi_GrupoFM                     
        WHERE gfi_Versao = c_TipAmb                                      
              and gfi_Procedimento = c_CodAMB ) 'Grupo FM',              
        FM_DtPagto 'Dt.Pagto',                                           
        FM_Duplicata 'Duplicata',                                        
       it.Guitem_Local 'Cód. Local Realizacao',                          
       fLocal.Fornecedor_ds 'Local Realizacao'                           
INTO TempRelFM_".$_SESSION['usuario']."
FROM FatorModerador FM                                                                    
   INNER JOIN Guia g ON FM_Guia = g.Gui_cd                                                
   INNER JOIN t_ArqUsu u ON Gui_Usuario = c_CtrUsu                                        
   LEFT OUTER JOIN ItensGuia it ON ( g.Gui_cd=it.Guitem_guia and it.Guitem_Seq = FM_Seq ) 
   INNER JOIN Empresa ON u.c_EmpUsu = Emp_cd                                              
   INNER JOIN Amb ON FM_Amb = c_TipAmb and FM_Procedimento = c_Codamb                     
   LEFT JOIN Fornecedor f ON Guitem_Prestador = f.Fornecedor_cd                           
   LEFT JOIN Fornecedor fLocal ON it.Guitem_Local = fLocal.Fornecedor_cd                  
   LEFT JOIN Especialidade ON g.Gui_Especialidade = Especialidade_cd                      
   LEFT JOIN Proc_Guia ON g.Gui_Tipo = ProcGuia_cd                                        
   LEFT JOIN t_ArqUsu TIT ON LEFT(TIT.c_CtrUsu,13) = LEFT((CASE WHEN U.Usu_TitCobranca is Null THEN U.c_CtrUsu ELSE U.Usu_TitCobranca END),13) and TIT.C_PARUSU = 'T' 
   LEFT JOIN T_ARQUSU TitCob ON TitCob.C_CTRUSU = u.Usu_TitCobranca                       
   LEFT JOIN Contapagar CP ON CP.PG_CD = FM.FM_Duplicata                                  
             and CP.PG_sequencia = FM.FM_SeqDup and CP.Pg_ContaCrr = FM.FM_ContaCrrDup       
WHERE FM_Guia is not null ";
}

	if ($_POST['tipo_cobranca'] != "todas") {
		$sql .= "and FM_forma = '".$_POST['tipo_cobranca']."' ";
	}

	if ($_POST['status_fm'] != "todas") {
		$sql .= "And FM_Status = '".$_POST['status_fm']."' ";
	}

	if ($_POST['empresa_vl'] != "Todas" and strlen($_POST['empresa_ini_cod']) >= 1) {
		$sql .= "and u.c_EmpUsu BETWEEN ".$_POST['empresa_ini_cod']." and ".$_POST['empresa_fim_cod']." ";
	}

	if ($_POST['empresa'] != "Todas" and strlen($_POST['emp_cod_grup']) >= 1) {
		$sql .= "AND Emp_cd in (SELECT gpi_empresa FROM GrupoEmpresaItens WHERE gpi_grupo = ".$_POST['emp_cod_grup'].") ";
	}

	if ($_POST['usu'] != "Todos" and strlen($_POST['nome_usu']) >= 1) {
		$sql .=  "and ( U.C_NOMUSU LIKE '%".$_POST['nome_usu']."%') ";
	}

	if ($_POST['usu'] != "Todos" and strlen($_POST['live_search']) >= 1) {
		$sql .=  "and ( U.C_CTRUSU LIKE '%".$_POST['live_search']."%') ";
	}

	if (strlen($_SESSION['valor_atual']) >= 1) {
		$sql .= "And Proc_Guia.ProcGuia_cd in(".$_SESSION['valor_atual'].") ";
	}

	if (strlen($_POST['regiao']) >= 1) {
		$sql .= "and Emp_Regiao = ".$_POST['regiao']." ";
	}

	if ($_POST['dia_vencimento_usu'] != "Todos") {
		$sql .= "and u.c_vctusu BETWEEN '".$_POST['dia_ini']."' and '".$_POST['dia_fim']."' ";
	}

	if ($_POST['tipo_empresa'] != "todas") {
		$sql .= "and Emp_Pessoa = '".$_POST['tipo_empresa']."' ";
	}

	$lib_guia_ini = explode("-", $_POST['ldti']);
	$lib_guia_fim = explode("-", $_POST['ldtf']);
	$lib_guia = $lib_guia_ini[1]."/".$lib_guia_ini[2]."/".$lib_guia_ini[0];
	$lib_guia_end = $lib_guia_fim[1]."/".$lib_guia_fim[2]."/".$lib_guia_fim[0];

	if (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) >= 1 and strlen($_POST['ldtf']) >= 1){
		$sql .= "and g.Gui_Liberacao BETWEEN '".$lib_guia."' and '".$lib_guia_end."' ";
	}elseif (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) >= 1 and strlen($_POST['ldtf']) <= 0){
		$sql .= "and g.Gui_Liberacao BETWEEN '".$lib_guia."' and '12/30/1899' ";
	}elseif (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) <= 0 and strlen($_POST['ldtf']) >= 1){
		$sql .= "and g.Gui_Liberacao BETWEEN '12/30/1899' and '".$lib_guia_end."' ";
	}elseif (($_POST['lib_guia'] != "Todas") and strlen($_POST['ldti']) <= 0 and strlen($_POST['ldtf']) <= 0){
		$sql .= "and g.Gui_Liberacao BETWEEN '12/30/1899' and '12/30/1899' ";
	}

	$dia_guia_ini = explode("-", $_POST['emgi']);
	$dia_guia_fim = explode("-", $_POST['emgf']);
	$dia_guia = $dia_guia_ini[1]."/".$dia_guia_ini[2]."/".$dia_guia_ini[0];
	$dia_guia_end = $dia_guia_fim[1]."/".$dia_guia_fim[2]."/".$dia_guia_fim[0];

	if (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) >= 1 and strlen($_POST['emgf']) >= 1){
		$sql .= "and Gui_Emissao BETWEEN '".$dia_guia."' and '".$dia_guia_end."' ";
	}elseif (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) >= 1 and strlen($_POST['emgf']) <= 0){
		$sql .= "and Gui_Emissao BETWEEN '".$dia_guia."' and '12/30/1899' ";
	}elseif (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) <= 0 and strlen($_POST['emgf']) >= 1){
		$sql .= "and Gui_Emissao BETWEEN '12/30/1899' and '".$dia_guia_end."' ";
	}elseif (($_POST['emiss_guia'] != "Todas") and strlen($_POST['emgi']) <= 0 and strlen($_POST['emgf']) <= 0){
		$sql .= "and Gui_Emissao BETWEEN '12/30/1899' and '12/30/1899' ";
	}

	$fechamento_ini = explode("-", $_POST['fechamento_ini']);
	$fechamento_fim = explode("-", $_POST['fechamento_fim']);
	$fec_ini = $fechamento_ini[1]."/".$fechamento_ini[2]."/".$fechamento_ini[0];
	$fec_fim = $fechamento_fim[1]."/".$fechamento_fim[2]."/".$fechamento_fim[0];

	if (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) >= 1 and strlen($_POST['fechamento_fim']) >= 1){
		$sql .= "and FM_DtPagto BETWEEN '".$fec_ini."' and '".$fec_fim."' ";
	}elseif (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) >= 1 and strlen($_POST['fechamento_fim']) <= 0){
		$sql .= "and FM_DtPagto BETWEEN '".$fec_ini."' and '12/30/1899' ";
	}elseif (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) <= 0 and strlen($_POST['fechamento_fim']) >= 1){
		$sql .= "and FM_DtPagto BETWEEN '12/30/1899' and '".$fec_fim."' ";
	}elseif (($_POST['fech'] != "Todas") and strlen($_POST['fechamento_ini']) <= 0 and strlen($_POST['fechamento_fim']) <= 0){
		$sql .= "and FM_DtPagto BETWEEN '12/30/1899' and '12/30/1899' ";
	}

	if ($_POST['st_usuario'] == "A") {
		$sql .=  "and (dbo.fStatusUsuario(u.C_IncUsu, u.C_DteUsu, '".$data."') = '".$_POST['st_usuario']."') ";
	}elseif ($_POST['st_usuario'] == "C") {
		$sql .= "and u.c_IncUSU <= '".$data."' and  ( u.c_dteUSU <= '".$data."') ";
	}elseif ($_POST['st_usuario'] == "todas") {
		$sql .= " ";
	}

	if (strlen($_POST['operator']) >= 1) {
		$sql .= "and g.Gui_operator = '".$_POST['operator']."' ";
	}

	if ($_POST['tipo_co'] != "Todas"){
		$sql .= "AND Emp_TipoCobranca = '".$_POST['tipo_co']."' ";
	}

	if ($_POST['fm_vinculada'] = "S") {
		$sql .= "AND FM_Duplicata IS NOT NULL ";
	}elseif ($_POST['fm_vinculada'] = "N") {
		$sql .= "AND FM_Duplicata IS NULL ";
	}
	
	if (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) >= 1 and strlen($_POST['canc_fim']) >= 1){
		$sql .= "and U.c_DteUsu between '".$_POST['canc_ini']."' and '".$_POST['canc_fim']."' ";
	}elseif (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) >= 1 and strlen($_POST['canc_fim']) <= 0){
		$sql .= "and U.c_DteUsu between '".$_POST['canc_ini']."' and '1899-12-30' ";
	}elseif (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) <= 0 and strlen($_POST['canc_fim']) >= 1){
		$sql .= "and U.c_DteUsu between '1899-12-30' and '".$_POST['canc_fim']."' ";
	}elseif (($_POST['canc'] != "Todas") and strlen($_POST['canc_ini']) <= 0 and strlen($_POST['canc_fim']) <= 0){
		$sql .= "and U.c_DteUsu between '1899-12-30' and '1899-12-30' ";
	}

	if (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) >= 1 and strlen($_POST['movi_can_fim']) >= 1){
		$sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '".$_POST['movi_can_ini']."' and '".$_POST['movi_can_fim']."') > 0 ";
	}elseif (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) >= 1 and strlen($_POST['movi_can_fim']) <= 0){
		$sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '".$_POST['movi_can_ini']."' and '1899-12-30') > 0 ";
	}elseif (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) <= 0 and strlen($_POST['movi_can_fim']) >= 1){
		$sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '1899-12-30' and '".$_POST['movi_can_fim']."') > 0 ";
	}elseif (($_POST['movi_can'] != "Todas") and strlen($_POST['movi_can_ini']) <= 0 and strlen($_POST['movi_can_fim']) <= 0){
		$sql .= "and (SELECT COUNT(*) FROM UsuHistReinclusao THR WHERE THR.Rei_Usuario = U.c_CtrUsu and THR.Rei_Operacao = 'E' and dbo.fFormatDate('AAAA-MM-DD',THR.Rei_DtOperacao)  between '1899-12-30' and '1899-12-30') > 0 ";
	}	


if ($_POST['tipo_relatorio'] != 'A') {
	if ($_POST['scales'] == 'on' and $_POST['horns'] == 'on') {
		$sql .= "GROUP BY Emp_cd, Emp_ds, FM.FM_Guia, FM.FM_Procedimento 
				select [Código Empresa],      
				[Desc Empresa],            
				SUM(Quantidade) Quantidade,
	 			Unit, 
				SUM(Total) Total
				FROM TempRelFM_".$_SESSION['usuario']."
				GROUP BY [Código Empresa]     
							  ,[Desc Empresa]          
						  ,Unit                  
				ORDER BY 1";
	}elseif ($_POST['tipo_relatorio'] != 'A' and $_POST['horns'] == 'on' and $_POST['scales'] != 'on' ) {
		$sql .= "GROUP BY Emp_cd, Emp_ds, FM.FM_Guia, FM.FM_Procedimento 
				select [Código Empresa],      
				[Desc Empresa],            
				SUM(Quantidade) Quantidade,
	 			0.0 Unit, 
				SUM(Total) Total
				FROM TempRelFM_".$_SESSION['usuario']."
				GROUP BY [Código Empresa]     
							  ,[Desc Empresa]          
						  ,Unit                  
				ORDER BY 1";
	}elseif ($_POST['tipo_relatorio'] != 'A' and $_POST['horns'] != 'on' and $_POST['scales'] == 'on' ) {
		$sql .= "GROUP BY Emp_cd, Emp_ds, FM.FM_Guia, FM.FM_Procedimento 
				select [Código Empresa],      
				[Desc Empresa],            
				SUM(Quantidade) Quantidade,
	 			Unit, 
				SUM(Total) Total
				FROM TempRelFM_".$_SESSION['usuario']."
				GROUP BY [Código Empresa]     
							  ,[Desc Empresa]          
						  ,Unit                  
				ORDER BY 1";
	}
}

if ($_POST['tipo_relatorio'] = 'A') {
	$sql .= "ORDER BY Emp_cd, u.c_NomUsu";
}


sqlsrv_query($conn, "IF object_id('tempdb.. TempRelFM_".$_SESSION['valor_atual']."') IS NOT NULL DROP TABLE TempRelFM_".$_SESSION['valor_atual']."");

$sql_full = sqlsrv_query($conn, $sql);
if( $sql === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$select_sql = sqlsrv_query($conn, "select * FROM TempRelFM_".$_SESSION['usuario']."");
if ("select * FROM TempRelFM_".$_SESSION['usuario']."" === false) {
	die( print_r( sqlsrv_errors(), true) );	
}

?>