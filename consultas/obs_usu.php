<?php

$cod_emp 	=	$_POST['cod_emp'];
$emp 		=	$_POST['emp'];
$cod_usu	=	$_POST['cod_usu'];
$usu 		=	$_POST['usu'];
$alerta 	=	$_POST['alerta'];
$status 	=	$_POST['status'];
$tipo	 	=	$_POST['tipo'];


if (strlen($cod_emp) > 0) {
	$vl_cod_emp = "and (e.Emp_cd = ".$cod_emp.")";
}else{
	$vl_cod_emp = "";
}

if (strlen($emp) > 0) {
	$vl_emp = "and e.Emp_ds LIKE UPPER('%".$emp."%')";
}else{
	$vl_emp = "";
}

if (strlen($cod_usu) > 0) {
	$vl_cod_usu = "and a.Aler_Usuario LIKE '%".$cod_usu."%'";
}else{
	$vl_cod_usu = "";
}

if (strlen($usu) > 0) {
	$vl_usu = "and u.c_NomUsu LIKE UPPER('%".$usu."%')";
}else{
	$vl_usu = "";
}

if (strlen($alerta) > 0) {
	$vl_alerta = "and a.Aler_Alerta LIKE UPPER('%".$alerta."%')";
}else{
	$vl_alerta = "";
}

if ($tipo == 'A') {
	$vl_tipo = "And a.Aler_Tipo = '".$tipo."'";
}elseif ($tipo == 'B') {
	$vl_tipo = "And a.Aler_Bloquear in ('B','W')";
}else{
	$vl_tipo = "";
}

if ($status == 'E' or $status == 'U') {
	$vl_status = "And a.Aler_Tipo = '".$status."'";
}else{
	$vl_status = "";
}


$sql_full =
"
SELECT Case when a.Aler_Tipo='U' then 'USUÁRIO' 
            else Case when a.Aler_Tipo= 'E' then 'EMPRESA' else 'EMP/PLANO' end end as Tipo, 
       Case when a.Aler_Bloquear='B' then 'BLOQUEAR' 
            else Case when a.Aler_Bloquear='W' then 'WEB BLOQ' 
            else 'AVISAR' end end as Aviso, 
       a.Aler_Emp, e.Emp_ds,a.Aler_Usuario, u.c_NomUsu, 
       Substring(a.Aler_Alerta,1,50) Aler_Alerta, a.Aler_DtInicial,
       a.Aler_DtFinal, a.Aler_Eterno, a.Aler_cd, a.Aler_Pla Plano
FROM Usu_Alerta a 
   LEFT OUTER JOIN t_ArqUsu u ON u.c_CtrUsu = a.Aler_Usuario
   LEFT OUTER JOIN Empresa e ON e.Emp_cd = IsNull(a.Aler_Emp,u.c_EmpUsu) 
WHERE a.Aler_cd IS NOT NULL ".$vl_cod_emp." ".$vl_emp." ".$vl_cod_usu." ".$vl_usu." ".$vl_alerta." ".$vl_tipo." ".$vl_status;

$full = sqlsrv_query($conn, $sql_full);
    if( $sql_full === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
?>