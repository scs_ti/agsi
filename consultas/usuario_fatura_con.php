<?php
error_reporting(E_ERROR | E_PARSE);

$tipo_pessoa 			= $_POST['tipo_pessoa'];
$tip_doc 				= $_POST['tip_doc'];
$n_doc 					= $_POST['n_doc'];
$n_doc_cod 				= $_POST['n_doc_cod'];
$tipo_cob 				= $_POST['tipo_cob'];
$tipo_boleto 			= $_POST['tipo_boleto'];
$empresa 				= $_POST['empresa'];
$empresa_ini_cod 		= $_POST['empresa_ini_cod'];
$empresa_fim_cod 		= $_POST['empresa_fim_cod'];
$grupo_emp 				= $_POST['grupo_emp'];
$grupo_emp_cod			= $_POST['grupo_emp_cod'];
$centro_custo 			= $_POST['centro_custo'];
$data_emicao 			= $_POST['data_emicao'];
$data_emicao_desc		= $_POST['data_emicao_desc'];
$quebra_titu			= $_POST['quebra_titu'];
$imprimir_oco			= $_POST['imprimir_oco'];
$exceto_cancelados		= $_POST['exceto_cancelados'];

     
################################################################
 
if (strlen($tipo_pessoa) > 0) {
	$vl_tipo_pessoa = "AND EMP_PESSOA ='".$tipo_pessoa."'";
}else{
	$vl_tipo_pessoa = "";
}

################################################################

if (strlen($tip_doc) > 0 and $n_doc != 'Todos') {
	$vl_tip_doc = "AND (pg_nf ".$n_doc." '".$n_doc_cod."')";
}else{
	$vl_tip_doc = "";
}

################################################################

if ($tipo_cob != 'Todas') {
	$vl_tipo_cob = "AND Emp_TipoCobranca = '".$tipo_cob."'";
}else{
	$vl_tipo_cob = "";
}

################################################################

if ($tipo_boleto != 'Ambos') {
	$vl_tipo_boleto = "AND (SELECT COUNT(*)    FROM FatorModerador     WHERE FM_Duplicata = PG_cd      AND FM_SeqDup = PG_sequencia 	    AND FM_ContaCrrDup = Pg_ContaCrr) ".$tipo_boleto." 0
";
}else{
	$vl_tipo_boleto = "";
}

################################################################

if ($empresa == "Selecionar") {
	$vl_empresa = "AND ISNULL(EMP_EmpRepasse, EMP_CD) BETWEEN '".$empresa_ini_cod."' AND '".$empresa_fim_cod."'";
}else{
	$vl_empresa = "";
}

################################################################

if ($grupo_emp == "Começa com") {
	$vl_gp_emp = "AND Emp_cd IN (SELECT gpi_empresa FROM GrupoEmpresaItens WHERE gpi_grupo = ".$grupo_emp_cod.")";
}else{
	$vl_gp_emp = "";
}

################################################################

if (strlen($centro_custo) > 0) {
	$vl_centro_custo = "AND CC.CUS_cd = ".$centro_custo."";
}else{
	$vl_centro_custo = "";
}

################################################################

if (!is_null($data_emicao_desc)) {
	$data_completa = explode("-", $data_emicao_desc);
	$ano = $data_completa[0];
	$mes = $data_completa[1];
	$dia = $data_completa[2];
	$data_correta = $mes."/".$dia."/".$ano;
}


if ($data_emicao != "Todas" and !is_null($data_correta)) {
	$vl_data_emicao = "AND Pg_emissao ".$data_emicao." '".$data_correta."'";
}else{
	$vl_data_emicao = "";
}

################################################################

if (strlen($quebra_titu)) {
	$vl_quebra_titu = $quebra_titu.",";
}else{
	$vl_quebra_titu = "";
}
$orderby = "ORDER BY Emp_Ds, ".$vl_quebra_titu." C_NomUsu";
################################################################

if (strlen($imprimir_oco)) {
	$vl_imprimir_oco = "
	UNION ALL                                                                                              
	 SELECT NULL,NULL,NULL,Fatura_Nota,Fatura_Emissao,Emp_cd,Emp_ds,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0, 
	        NULL,NULL, NULL, 0, NULL, OEmp_valor,Tipo, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL  
	FROM #tmpOcorrencias";
}else{
	$vl_imprimir_oco = "";
}
################################################################

if (strlen($exceto_cancelados)) {
	$vl_exceto_cancelados = "AND Pg_StatusBaixa <> 'C'";
}else{
	$vl_exceto_cancelados = "";
}


$sql_full =
"
CREATE VIEW view_name as
SELECT                                                                                                          
     (SELECT TOP 1 C_CtrUsu                                                                                     
      FROM T_ArqUsu T                                                                                           
      WHERE T.C_PlaUsu = T_ArqUsu.C_PlaUsu                                                                      
            AND T.C_EmpUsu = T_ArqUsu.C_EmpUsu                                                                  
            AND T.C_CodUsu = T_ArqUsu.C_CodUsu                                                                  
            AND T.C_ParUsu = 'T' ) CodTitular,                                                                
     (SELECT TOP 1 C_NomUsu                                                                                     
      FROM T_ArqUsu T                                                                                           
      WHERE T.C_PlaUsu = T_ArqUsu.C_PlaUsu                                                                      
            AND T.C_EmpUsu = T_ArqUsu.C_EmpUsu                                                                  
            AND T.C_CodUsu = T_ArqUsu.C_CodUsu                                                                  
            AND T.C_ParUsu = 'T' ) Titular,                                                                   
      Usu_Interno, URe_Doc, pg_emissao, Emp_Cd, Emp_Ds, Emp_CGC, Emp_CPF, EMP_EmpRepasse,                       
      (SELECT EMP.EMP_DS                                                                                        
         FROM EMPRESA EMP                                                                                       
         WHERE EMP.EMP_CD = E.EMP_EmpRepasse ) Emp_Faturamento,                                                 
      URe_Usuario, C_NomUsu, BRec_DtBaixa,                                                                      
      CONVERT( DateTime, SUBSTRING( Pg_Mes,1,2 )+'/'+Pg_Vencto+'/'+SUBSTRING( Pg_Mes,4,4 ) ) DtVencto,      
      URe_VlMensalidade, URe_VlFM, URe_VlOpcional, C_NASUSU, C_CICUSU,                                          
      CASE Usu_VinculoBenef                                                                                     
         WHEN '01' THEN 'B'                                                                                 
         WHEN '03' THEN 'C'                                                                                 
         WHEN '04' THEN 'F'                                                                                 
         WHEN '05' THEN 'F'                                                                                 
         WHEN '06' THEN 'E'                                                                                 
         WHEN '07' THEN 'E'                                                                                 
         WHEN '08' THEN 'P'                                                                                 
         WHEN '09' THEN 'M'                                                                                 
         WHEN '10' THEN 'A'                                                                                 
         WHEN '11' THEN 'TM'                                                                                
      END Usu_VinculoBenef,                                                                                     
      CC.CUS_cd,                                                                                                
      CC.CUS_ds,                                                                                                
      convert(decimal(18,2),0) as Ocorrencias,                                                                  
      'Usuários' as Tipo,                                                                                     
      Fatura_NFSE NFSE,                                                                                         
      CASE                                                                                                      
         WHEN (SELECT SUBSTRING(RIGHT(URe_Usuario, 4),1,2)) =  '01' THEN                                      
            'Titular'                                                                                         
         ELSE                                                                                                   
            'Dependente'                                                                                      
      END TipoBeneficiario,                                                                                     
      C_Plausu 'Cód Plano',                                                                                   
      Pla_ds 'Plano',                                                                                         
      C_SEXUSU 'Sexo',                                                                                        
      C_PARUSU 'Parentesco',                                                                                  
      C_ABRUSU 'ABRANGE',                                                                                     
      C_IncUsu 'Inclusão',                                                                                    
      dbo.CalculaIdade(C_NasUsu, GETDATE()) 'Idade',                                                          
      Usu_TitCobranca ,                                                                                         
      C_CODUSU                                                                                                  
FROM UsuReceber                                                                                                 
   INNER JOIN ContaPagar on pg_cd = ure_doc and Ure_COntaCrr = Pg_ContaCrr and Ure_Seq = Pg_Sequencia           
   INNER JOIN t_ArqUsu ON C_CtrUsu = Ure_Usuario                                                                
   INNER JOIN Plano ON Pla_Cd = C_PlaUsu                                                                        
   INNER JOIN Empresa E ON Emp_Cd = C_EmpUsu                                                                    
   INNER JOIN Usu_Complemento UC ON t_ArqUsu.C_CtrUsu = UC.C_CtrUsu                                             
   LEFT JOIN Fatura ON Fatura_nota = pg_nf
   LEFT JOIN CentroCusto CC ON UC.C_CUSUSU = CC.CUS_cd
LEFT JOIN baixareceber br ON br.BRec_Documento = Pg_cd 
                         AND br.BRec_Sequencia = Pg_Sequencia
                         AND br.BRec_ContaCrr = Pg_ContaCrr
                       AND BRec_seqBaixa = 1 
WHERE URe_Doc IS NOT NULL ".$vl_tipo_pessoa." ".$vl_gp_emp." ".$vl_tip_doc." ".$vl_empresa." ".$vl_exceto_cancelados." ".$vl_data_emicao." ".$vl_centro_custo." ".$vl_tipo_cob." ".$vl_tipo_boleto." ".$vl_imprimir_oco;


sqlsrv_query($conn, $sql_full);
    if( $sql_full === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

?>





