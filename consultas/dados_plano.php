  <?php 

  $sql = "
  SELECT sip_tiss.TAc_cd, sip_tiss.TAc_ds, 
         p.PLA_cd, 
         p.PLA_ds, 
         p.PLA_simpas, 
         p.PLA_acomodacao, 
         p.PLA_carencia,
               p.PLA_obs, 
               p.PLA_encaminhar, 
               p.PLA_FatorModerador, 
               p.PLA_nroUtilizacao,
               Pla_TipoPlano,
               Pla_TipoCobertura,
               PrecoCobertura_ds,
               Pla_AbranGeog,
               pag.Abr_ds,
               Pla_LeiPlano,
               PLA_DescResumo,
               Pla_AcomodacaoTISS,
               PLA_SCPA,
               Pla_Grupo,
               x.gp_ds,
               T.C_EMPUSU,
               pp.PrecoPlano_ds,
               SIPS_e.Emp_Fantasia
FROM SipsSCSJC.dbo.TISS_TipoAcomodacao AS sip_tiss
JOIN plano AS p ON sip_tiss.TAc_cd = p.Pla_AcomodacaoTISS
INNER JOIN T_ARQUSU as T ON C_PLAUSU = PLA_cd
INNER JOIN SipsSCSJC.dbo.GrupoPlano as x on x.gp_cd = Pla_Grupo
INNER JOIN SipsSCSJC.dbo.Empresa AS SIPS_e on T.C_EMPUSU = SIPS_e.EMP_cd
INNER JOIN PrecoPlano AS pp on Pla_TipoPlano = pp.PrecoPlano_cd 
INNER JOIN PrecoAbranGeog AS pag ON Pla_AbranGeog = pag.Abr_cd
INNER JOIN PrecoCobertura AS pc ON pc.PrecoCobertura_cd = Pla_TipoCobertura
WHERE C_CtrUsu = '".$_GET['cod']."'";

$sql2 = "SELECT TOP 1 Tab_cd TabPreco_cd,
                      Tab_ds TabPreco_ds
         FROM t_ArqUsu
          LEFT JOIN Usu_TabelaPrecoIndividual ON tpi_Beneficiario = c_CtrUsu
          LEFT JOIN EmpresaPlano ON EP_Empresa = c_EmpUsu and EP_Plano = c_PlaUsu
                                                 AND C_IncUsu BETWEEN ISNULL(Ep_inicio,'1900-01-01') AND ISNULL(EP_termino,'2050-09-01')
          LEFT JOIN Tabela_Precos ON TAB_Cd = ISNULL(tpi_TabelaPreco,EP_TabPreco)
          WHERE c_CtrUsu = '".$_GET['cod']."'";

$sql3 = "SELECT  AJU_cd Faixas,
                 AJU_PerTitular 'Titular (%)',
                 AJU_PerDependente 'Dependente (%)',
                 AJU_PerAgregado 'Agregado (%)'
          FROM t_ArqUsu
            LEFT JOIN Usu_TabelaPrecoIndividual ON tpi_Beneficiario = c_CtrUsu
            LEFT JOIN EmpresaPlano ON EP_Empresa = c_EmpUsu and EP_Plano = c_PlaUsu
                                                      AND C_IncUsu BETWEEN ISNULL(Ep_inicio,'1900-01-01') AND ISNULL(EP_termino,'2050-09-01')
            LEFT JOIN TabPreco_Ajustes ON AJU_TabPreco = ISNULL(tpi_TabelaPreco,EP_TabPreco)
          WHERE c_CtrUsu = '".$_GET['cod']."'";

  
$consulta = sqlsrv_query($conn, $sql);
if( $consulta === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_preco = sqlsrv_query($conn, $sql2);
if( $consulta_preco === false) {
  die( print_r( sqlsrv_errors(), true) );
} 

while( $row = sqlsrv_fetch_array( $consulta_preco, SQLSRV_FETCH_ASSOC) ) {
  $TabPreco_cd = $row['TabPreco_cd'];
  $TabPreco_ds = $row['TabPreco_ds'];
}

$reajuste = sqlsrv_query($conn, $sql3);
if( $reajuste === false) {
  die( print_r( sqlsrv_errors(), true) );
} 
?>