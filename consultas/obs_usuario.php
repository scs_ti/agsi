<?php 

$sql_indicacao_usuario = "
SELECT                                                                             
   C_CTRUSU,                                                                       
   C_NOMUSU,                                                                       
   C_SEXUSU,                                                                       
   EMP_DS,                                                                         
   PLA_DS,                                                                         
   CID_DS,                                                                         
   dbo.calculaIdade(C_NASUSU,getdate()) Idade                                      
FROM T_ARQUSU                                                                      
    INNER JOIN EMPRESA ON EMP_CD = C_EMPUSU                                        
    INNER JOIN PLANO ON PLA_CD = C_PLAUSU                                          
    INNER JOIN CIDADE ON CID_DS = EMP_CIDADE                                       
    LEFT JOIN GUIA ON GUI_USUARIO = C_CTRUSU                                       
    LEFT JOIN ITENSGUIA ON GUITEM_GUIA = GUI_CD                                    
    LEFT JOIN Proc_Guia ON ProcGuia_cd = Gui_tipo                                  
    LEFT JOIN Local_Atendto ON Local_cd = Gui_Local                                
    LEFT JOIN AMB A ON GUITEM_PROCEDIMENTO = A.C_CODAMB AND GUITEM_AMB = A.C_TIPAMB
    LEFT OUTER JOIN CLASSEAMB ON A.C_CLAAMB = CLASSE_CD                            
    LEFT JOIN GRUPO ON Amb_Grupo = GRU_cd                                          
    LEFT JOIN fornecedor P on fornecedor_cd = Guitem_Prestador                     
    LEFT JOIN especialidade ON Especialidade_cd = Gui_especialidade                
WHERE (dbo.fStatusUsuario(C_IncUsu, C_DteUsu, GETDATE()) = 'A')                  
   AND C_CTRUSU NOT IN (SELECT C_CTRUSU FROM Usu_Preventiva )                      
GROUP BY                                       
   C_CTRUSU,                           
   C_NOMUSU ,                          
   EMP_DS ,                            
   PLA_DS ,                            
   CID_DS,                             
   C_SEXUSU,                           
   dbo.calculaIdade(C_NASUSU,getdate())
HAVING SUM(Guitem_qtde)  > 0
";

$sql_obs_usu = 
"
SELECT Case when a.Aler_Tipo='U' then 'USUÁRIO' 
            else Case when a.Aler_Tipo= 'E' then 'EMPRESA' else 'EMP/PLANO' end end as Tipo, 
       Case when a.Aler_Bloquear='B' then 'BLOQUEAR' 
            else Case when a.Aler_Bloquear='W' then 'WEB BLOQ' 
            else 'AVISAR' end end as Aviso, 
       a.Aler_Emp, e.Emp_ds,a.Aler_Usuario, u.c_NomUsu, 
       Substring(a.Aler_Alerta,1,50) Aler_Alerta, a.Aler_DtInicial,
       a.Aler_DtFinal, a.Aler_Eterno, a.Aler_cd, a.Aler_Pla Plano
FROM Usu_Alerta a 
   LEFT OUTER JOIN t_ArqUsu u ON u.c_CtrUsu = a.Aler_Usuario
   LEFT OUTER JOIN Empresa e ON e.Emp_cd = IsNull(a.Aler_Emp,u.c_EmpUsu) 
WHERE a.Aler_cd IS NOT NULL
And a.Aler_Tipo = 'U' 
And a.Aler_Bloquear = 'A' 
ORDER BY e.Emp_ds, u.c_NomUsu
";


$obs_usu = sqlsrv_query($conn, $sql_obs_usu);
if( $sql_obs_usu === false) {
die( print_r( sqlsrv_errors(), true) );
}


$indicacao_usuario = sqlsrv_query($conn, $sql_indicacao_usuario);
if( $sql_indicacao_usuario === false) {
die( print_r( sqlsrv_errors(), true) );
}

?>