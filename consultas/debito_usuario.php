  <?php 

  $sql = "
  CREATE VIEW [debito_usuario] as
  SELECT D.Pg_cd,
  D.Pg_sequencia,
  D.Pg_ContaCrr,
  HIA.hia_ds ControleInadimplencia,
  (SELECT TOP 1 NFSE_NumNFSE FROM NFSE WHERE NFSE_boletoNota = Pg_Cd and NFSE_BolSeq = PG_sequencia and NFSE_BolCCrr = Pg_ContaCrr ORDER BY NFSE_Status) NFSE, 
  (SELECT TOP 1 NFSE_CodVerific FROM NFSE WHERE NFSE_boletoNota = Pg_Cd and NFSE_BolSeq = PG_sequencia and NFSE_BolCCrr = Pg_ContaCrr ORDER BY NFSE_Status) CodiVerificNFSE, 
  D.Pg_Emissao,
  Cast((SubString(D.Pg_Mes, 1, 2) + '/' + D.PG_Vencto + '/' + SubString(D.Pg_Mes,4, 4)) AS DateTime) Vencimento,
  PG_MesReferencia Referencia, 
  CASE WHEN PG_statusBaixa = 'A' 
  THEN CASE WHEN DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),GETDATE()) <= 0 THEN Null ELSE DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),GETDATE()) END          
  ELSE CASE WHEN DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),BRec_DtBaixa) <= 0 THEN Null ELSE DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),BRec_DtBaixa) END    
  END DiasAtraso, 
  T.Trec_ds,
  TD.Trec_ds RecebeDupl,
  D.Pg_Valor,
  D.Pg_Juros,
  D.Pg_Desconto,
  D.Pg_vlMensalidade,
  D.Pg_vlOpcionais,
  D.Pg_vlCoParticipacao,
  B.BRec_DtBaixa,
  CASE WHEN PG_statusBaixa in ('C','I') THEN 0.0 ELSE ISNULL(B.BRec_Valor,0) END BRec_Valor,
  CASE WHEN D.pg_statusBaixa in ('C', 'I') THEN CONVERT(MONEY, 0)                 
  ELSE (CASE WHEN (ISNULL(D.pg_valor,0) - dbo.fValorRecebido(D.pg_cd, D.pg_sequencia, D.pg_contacrr))<0 THEN 0        
  ELSE ISNULL(D.pg_valor,0) - IsNull((SELECT SUM(ISNULL(BR.Brec_desconto,0)) 
  FROM BaixaReceber BR                   
  WHERE D.Pg_cd = BR.Brec_Documento and D.Pg_sequencia = BR.Brec_Sequencia and D.Pg_ContaCrr = BR.Brec_ContaCrr ),0) 
  - dbo.fValorRecebido(D.pg_cd, D.pg_sequencia, D.pg_contacrr) END) END VlAberto,
  ISNULL(B.Brec_juro,0) Brec_juro, 
  ISNULL(B.Brec_desconto,0) Brec_desconto, 
  CASE D.Pg_StatusBaixa
  WHEN 'A' THEN 'ABERTO'
  WHEN 'T' THEN 'B. TOTAL'
  WHEN 'P' THEN 'B. PARCIAL'
  WHEN 'I' THEN 'INATIVO'
  WHEN 'C' THEN 'CANCELADO'
  END Pg_StatusBaixa,
  C.CC_Banco,
  C.CC_Agencia,
  C.CC_Cd,
  D.Pg_NossoNumero,
  ( ISNull(PG_OBS+' - ','') + IsNull(BRec_Observacao,'') ) ObservacaoBaixa,
  Convert(VARCHAR(800), B.BRec_ObsJuros) BRec_ObsJuros,  
  B.Brec_MovFinanceiro, 
  M.Movimento_ds,    
  CONVERT(VARCHAR, D.PG_RefInicial, 103) +' a '+ CONVERT(VARCHAR, D.PG_RefFinal, 103) 'Vigencia (Risco)', 
  '".$_GET['cod']."' CodUsu,
  '".$_GET['C_NOMUSU']."' NomUsu
  ,CONVERT(MONEY,NULL) VlrLiquido 
  ,(u.C_VCTUSU +'/'+D.PG_mesreferencia) C_VCTUSU, 
  (SELECT MAX(FCI_FolhaComissao) 
  FROM FolhaComissaoItens
  WHERE D.PG_cd = FCI_Documento AND D.PG_Sequencia = FCI_Sequencia AND D.PG_Contacrr = FCI_ContaCrr AND u.C_CTRUSU = FCI_Codigo)  NroFolhaComissao, 
  (SELECT MAX(FCI_DtVencto) 
  FROM FolhaComissaoItens
  WHERE D.PG_cd = FCI_Documento AND D.PG_Sequencia = FCI_Sequencia AND D.PG_Contacrr = FCI_ContaCrr AND u.C_CTRUSU = FCI_Codigo)  DtVenctoComissao, 
  DA.Deb_CodDebAutomatico
  FROM ContaPagar D 
  LEFT OUTER JOIN BaixaReceber B ON D.Pg_cd = B.Brec_Documento and D.Pg_sequencia = B.Brec_Sequencia and D.Pg_ContaCrr = B.Brec_ContaCrr
  INNER JOIN T_ArqUsu u ON u.C_CtrUsu = D.PG_USU_cd 
  INNER JOIN Empresa e ON u.C_EmpUsu = Emp_cd 
  INNER JOIN Conta_Corr C ON D.Pg_ContaCrr = C.CC_Codigo 
  LEFT OUTER JOIN TRecebimento T ON T.Trec_cd = D.PG_tipoRecebe 
  LEFT OUTER JOIN TRecebimento TD ON TD.Trec_cd = B.BRec_TRecebimento 
  LEFT OUTER JOIN TipoFinanceiro M ON B.Brec_MovFinanceiro = M.Movimento_cd 
  LEFT JOIN Historico_Inadimplentes ON hi_Duplicata = Pg_cd and hi_DupSeq = PG_sequencia and hi_DupCC = Pg_ContaCrr 
  LEFT JOIN Historico_InadimplentesAcompanhamento HIA ON hia_cd = hi_Acompanhamento 
  LEFT JOIN DebAutomatico DA ON DA.Deb_Usuario = D.PG_USU_cd
  WHERE D.Pg_Usu_cd = '".$_GET['cod']."'
";

$sql_select_all_debito = "
CREATE VIEW [debito_usuario_full] as
SELECT D.Pg_cd,
D.Pg_sequencia,
D.Pg_ContaCrr,
HIA.hia_ds ControleInadimplencia,
(SELECT TOP 1 NFSE_NumNFSE FROM NFSE WHERE NFSE_boletoNota = Pg_Cd and NFSE_BolSeq = PG_sequencia and NFSE_BolCCrr = Pg_ContaCrr ORDER BY NFSE_Status) NFSE, 
(SELECT TOP 1 NFSE_CodVerific FROM NFSE WHERE NFSE_boletoNota = Pg_Cd and NFSE_BolSeq = PG_sequencia and NFSE_BolCCrr = Pg_ContaCrr ORDER BY NFSE_Status) CodiVerificNFSE, 
D.Pg_Emissao,
Cast((SubString(D.Pg_Mes, 1, 2) + '/' + D.PG_Vencto + '/' + SubString(D.Pg_Mes,4, 4)) AS DateTime) Vencimento,
PG_MesReferencia Referencia, 
CASE WHEN PG_statusBaixa = 'A' 
THEN CASE WHEN DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),GETDATE()) <= 0 THEN Null ELSE DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),GETDATE()) END          
ELSE CASE WHEN DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),BRec_DtBaixa) <= 0 THEN Null ELSE DATEDIFF(day,dbo.fDataVencto(PG_Vencto, Pg_Mes),BRec_DtBaixa) END    
END DiasAtraso, 
T.Trec_ds,
TD.Trec_ds RecebeDupl,
D.Pg_Valor,
D.Pg_Juros,
D.Pg_Desconto,
D.Pg_vlMensalidade,
D.Pg_vlOpcionais,
D.Pg_vlCoParticipacao,
B.BRec_DtBaixa,
CASE WHEN PG_statusBaixa in ('C','I') THEN 0.0 ELSE ISNULL(B.BRec_Valor,0) END BRec_Valor,
CASE WHEN D.pg_statusBaixa in ('C', 'I') THEN CONVERT(MONEY, 0)                 
ELSE (CASE WHEN (ISNULL(D.pg_valor,0) - dbo.fValorRecebido(D.pg_cd, D.pg_sequencia, D.pg_contacrr))<0 THEN 0        
ELSE ISNULL(D.pg_valor,0) - IsNull((SELECT SUM(ISNULL(BR.Brec_desconto,0)) 
FROM BaixaReceber BR                   
WHERE D.Pg_cd = BR.Brec_Documento and D.Pg_sequencia = BR.Brec_Sequencia and D.Pg_ContaCrr = BR.Brec_ContaCrr ),0) 
- dbo.fValorRecebido(D.pg_cd, D.pg_sequencia, D.pg_contacrr) END) END VlAberto,
ISNULL(B.Brec_juro,0) Brec_juro, 
ISNULL(B.Brec_desconto,0) Brec_desconto, 
CASE D.Pg_StatusBaixa
WHEN 'A' THEN 'ABERTO'
WHEN 'T' THEN 'B. TOTAL'
WHEN 'P' THEN 'B. PARCIAL'
WHEN 'I' THEN 'INATIVO'
WHEN 'C' THEN 'CANCELADO'
END Pg_StatusBaixa,
C.CC_Banco,
C.CC_Agencia,
C.CC_Cd,
D.Pg_NossoNumero,
( ISNull(PG_OBS+' - ','') + IsNull(BRec_Observacao,'') ) ObservacaoBaixa,
Convert(VARCHAR(800), B.BRec_ObsJuros) BRec_ObsJuros,  
B.Brec_MovFinanceiro, 
M.Movimento_ds,    
CONVERT(VARCHAR, D.PG_RefInicial, 103) +' a '+ CONVERT(VARCHAR, D.PG_RefFinal, 103) 'Vigencia (Risco)', 
'".$_GET['cod']."' CodUsu,
'".$_GET['C_NOMUSU']."' NomUsu
,CONVERT(MONEY,NULL) VlrLiquido 
,(u.C_VCTUSU +'/'+D.PG_mesreferencia) C_VCTUSU, 
(SELECT MAX(FCI_FolhaComissao) 
FROM FolhaComissaoItens
WHERE D.PG_cd = FCI_Documento AND D.PG_Sequencia = FCI_Sequencia AND D.PG_Contacrr = FCI_ContaCrr AND u.C_CTRUSU = FCI_Codigo)  NroFolhaComissao, 
(SELECT MAX(FCI_DtVencto) 
FROM FolhaComissaoItens
WHERE D.PG_cd = FCI_Documento AND D.PG_Sequencia = FCI_Sequencia AND D.PG_Contacrr = FCI_ContaCrr AND u.C_CTRUSU = FCI_Codigo)  DtVenctoComissao, 
DA.Deb_CodDebAutomatico
FROM ContaPagar D 
LEFT OUTER JOIN BaixaReceber B ON D.Pg_cd = B.Brec_Documento and D.Pg_sequencia = B.Brec_Sequencia and D.Pg_ContaCrr = B.Brec_ContaCrr
INNER JOIN T_ArqUsu u ON u.C_CtrUsu = D.PG_USU_cd 
INNER JOIN Empresa e ON u.C_EmpUsu = Emp_cd 
INNER JOIN Conta_Corr C ON D.Pg_ContaCrr = C.CC_Codigo 
LEFT OUTER JOIN TRecebimento T ON T.Trec_cd = D.PG_tipoRecebe 
LEFT OUTER JOIN TRecebimento TD ON TD.Trec_cd = B.BRec_TRecebimento 
LEFT OUTER JOIN TipoFinanceiro M ON B.Brec_MovFinanceiro = M.Movimento_cd 
LEFT JOIN Historico_Inadimplentes ON hi_Duplicata = Pg_cd and hi_DupSeq = PG_sequencia and hi_DupCC = Pg_ContaCrr 
LEFT JOIN Historico_InadimplentesAcompanhamento HIA ON hia_cd = hi_Acompanhamento 
LEFT JOIN DebAutomatico DA ON DA.Deb_Usuario = D.PG_USU_cd
WHERE D.Pg_cd =   '".$_GET['cod']."'
";

$sql_composicao =
"
SELECT U.C_CTRUSU Codigo,
U.c_NomUsu Nome,
UR.URe_VlMensalidade VlMensalidade,
UR.URe_VlFM VlFM,
UR.URe_VlOpcional VlOpcional
FROM UsuReceber UR
INNER JOIN t_ArqUsu U ON U.c_CtrUsu = UR.URe_Usuario
WHERE URe_Doc = '".$_GET['cod']."'
ORDER BY U.c_NomUsu
";

  
$primeiro = sqlsrv_query($conn, "DROP VIEW [dbo].[debito_usuario];");
  if( $primeiro === false) {
    die( print_r( sqlsrv_errors(), true) );
}

$drop2 = sqlsrv_query($conn, "DROP VIEW [dbo].[debito_usuario_full];");
  if( $primeiro === false) {
    die( print_r( sqlsrv_errors(), true) );
}

$segundo = sqlsrv_query($conn, $sql);
  if( $segundo === false) {
    die( print_r( sqlsrv_errors(), true) );
  }

$consulta = sqlsrv_query($conn, "SELECT *, convert(varchar, Pg_Emissao, 101) as deb_pg_emissao
                                           , convert(varchar, Vencimento, 101) as deb_vencimento
                                           , convert(varchar, BRec_DtBaixa, 101) as deb_BRec_DtBaixa 
                                           , convert(varchar, Pg_Emissao, 101) as deb_pg_emissao from debito_usuario ");

$terceiro = sqlsrv_query($conn, $sql_select_all_debito);
if( $terceiro === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_all = sqlsrv_query($conn, "SELECT *, convert(varchar, Pg_Emissao, 101) as deb_pg_emissao
                                           , convert(varchar, Vencimento, 101) as deb_vencimento
                                           , convert(varchar, BRec_DtBaixa, 101) as deb_BRec_DtBaixa 
                                           , convert(varchar, Pg_Emissao, 101) as deb_pg_emissao from debito_usuario_full ");

$composicao = sqlsrv_query($conn, $sql_composicao);
  if( $terceiro === false) {
    die( print_r( sqlsrv_errors(), true) );
}
?>


