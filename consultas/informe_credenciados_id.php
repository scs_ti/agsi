<?php

$sql_completo_id = 
"
SELECT DISTINCT                                                                                                                       
			Fornecedor_cd,                                                                                                                 
			Fornecedor_ds,                                                                                                                 
			Fornecedor_end,                                                                                                                
			Fornecedor_bairro,                                                                                                             
			Fornecedor_cep,                                                                                                                
			CCred.Cid_ds,                                                                                                                  
			CCred.Cid_CodIBGE,                                                                                                             
			Fornecedor_uf,                                                                                                                 
			Fornecedor_cnpj,                                                                                                               
			Fornecedor_cpf,                                                                                                                
			Fornecedor_TipoIrrf,                                                                                                           
			Fornecedor_Fone1,                                                                                                              
			Fornecedor_Fone2,                                                                                                              
			Fornecedor_Celular,                                                                                                            
			Fornecedor_Fax,                                                                                                                
			Fornecedor_RG,                                                                                                                 
			Fornecedor_email,                                                                                                              
			CC.CC_Banco,                                                                                                                   
			Fornecedor_Banco,                                                                                                              
			CC.CC_Agencia,                                                                                                                 
			Fornecedor_Agencia,                                                                                                            
			CC.CC_Cd,                                                                                                                      
			Fornecedor_Conta,                                                                                                              
			Fornecedor_EmiteCheque,                                                                                                        
			Fornecedor_TipoFor,                                                                                                            
			Fornecedor_InsMunic,                                                                                                           
			Fornecedor_RazSocial,                                                                                                          
			Fornecedor_Status,                                                                                                             
			CASE WHEN CONVERT(VARCHAR,Fornecedor_TipoCredenciamento) = '1' THEN 'Próprio'                                              
				  WHEN CONVERT(VARCHAR,Fornecedor_TipoCredenciamento) = '2' THEN 'Cooperado'                                            
			     WHEN CONVERT(VARCHAR,Fornecedor_TipoCredenciamento) = '3' THEN 'Conveniado' END Fornecedor_TipoCredenciamento,        
			Fornecedor_CtaCtabil,                                                                                                          
			Fornecedor_Pessoa,                                                                                                             
			Fornecedor_Desconto,                                                                                                           
			Fornecedor_VlrLimiteDesconto,                                                                                                  
			Fornecedor_Inss,                                                                                                               
			Fornecedor_Pis,                                                                                                                
			Fornecedor_Contato,                                                                                                            
			Fornecedor_Logradouro,                                                                                                         
			Fornecedor_IE,                                                                                                                 
			Fornecedor_DtNasc,                                                                                                             
			CASE WHEN Fornecedor_EstCivil = 'C' THEN 'CASADO'                                                                          
			     WHEN Fornecedor_EstCivil = 'S' THEN 'SOLTEIRO'                                                                        
			     WHEN Fornecedor_EstCivil = 'V' THEN 'VIÚVO'                                                                           
			     WHEN Fornecedor_EstCivil = 'A' THEN 'AMASIADO'                                                                        
			     WHEN Fornecedor_EstCivil = 'D' THEN 'DIVORCIADO'                                                                      
			     WHEN Fornecedor_EstCivil = 'E' THEN 'DESQUITADO' END  Fornecedor_EstCivil,                                            
			Fornecedor_LiminarCofins,                                                                                                      
			For_EnviaWEB,                                                                                                                  
			FCred.Cre_Inss,                                                                                                                
			FCred.Cre_Iss,                                                                                                                 
			Fornecedor_ManualPortal,                                                                                                       
			Fornecedor_TipoContra,                                                                                                         
			Cus_cd,                                                                                                                        
			Cus_ds,                                                                                                                        
			Fornecedor_EndComp,                                                                                                            
			Fornecedor_sexo,                                                                                                               
			Fornecedor_Banco Banco,                                                                                                        
			Fornecedor_Agencia Agencia,                                                                                                    
			Fornecedor_Conta Conta                                                                                                         
 ,CONVERT(INTEGER,NULL) CodEsp, CONVERT(VARCHAR(40),NULL) Especialidade,CONVERT(char(5),NULL) Qualificacao                           
			,FCred.Cre_DtIniContrato DtIniContrato,                                                                                        
			FCred.Cre_DtFinContrato DtFinContrato,                                                                                          
       Fornecedor_GuiaMedico,                                                                                                            
  (SELECT TOP 1 ForRe_DiaReaj FROM Fornecedor_Reajuste WHERE ForRe_Credenciado = Fornecedor_cd ORDER BY ForRe_Data DESC) UltimoDiaReajuste, 
  (SELECT TOP 1 ForRe_Percentual FROM Fornecedor_Reajuste WHERE ForRe_Credenciado = Fornecedor_cd ORDER BY ForRe_Data DESC) Percentual,     
  (SELECT TOP 1 ForRe_Operador FROM Fornecedor_Reajuste WHERE ForRe_Credenciado = Fornecedor_cd ORDER BY ForRe_Data DESC) Operador          
 FROM Fornecedor Forn                                                                                                                  
  LEFT OUTER JOIN TipoFornece Tipo ON Tipo.TFornece_cd = Forn.Fornecedor_TipoFor                                                       
  LEFT OUTER JOIN Conta_Corr CC ON CC.CC_Codigo = Forn.Fornecedor_ContaCrr                                                             
  LEFT OUTER JOIN Forn_Credenciados FCred ON Forn.Fornecedor_cd = FCred.Cre_Fornecedor                                                 
  LEFT OUTER JOIN Cidade CCred ON Forn.Fornecedor_CodCidade = CCred.Cid_cd                                                             
  LEFT OUTER JOIN CentroCusto ON Forn.Fornecedor_CentroCusto = Cus_cd                                                                  
   LEFT OUTER JOIN Forn_Especialidade ON Forn_Especialidade.Esp_Fornecedor = Forn.Fornecedor_cd                                      
   LEFT OUTER JOIN Especialidade E ON E.Especialidade_cd = Forn_Especialidade.Esp_Especialidade                                      
   LEFT OUTER JOIN Forn_Qualificacao on Esp_Especialidade = Qua_Especialidade and Esp_Fornecedor = Qua_Fornecedor                    
   LEFT OUTER JOIN Qualificacao Q on Qua_Qualificacao = Q.Qualificacao_cd                                                            
   LEFT OUTER JOIN Forn_Empresa Fe ON Forn.Fornecedor_cd = Fe.FornEmpresa_Fornecedor                                                 
   LEFT OUTER JOIN Empresa Emp ON Fe.FornEmpresa_Empresa = Emp.Emp_cd                                                                
WHERE Forn.Fornecedor_cd is not null and Fornecedor_cd = 
".$_GET['cod'];

$sql_historico =
"
SELECT LData,
       CASE WHEN LStatus = 'I' THEN 'Inclusão'
                 WHEN LStatus = 'A' THEN 'Alteração'
	 ELSE 'Exclusão' END LStatus,
                 LOperador 
FROM LOGFORNECEDOR
WHERE FORNECEDOR_CD = ".$_GET['cod'];

$completo_id = sqlsrv_query($conn, $sql_completo_id);
    if( $sql_completo_id === false) {
        die( print_r( sqlsrv_errors(), true) );
    }  

$historico = sqlsrv_query($conn, $sql_historico);
    if( $sql_historico === false) {
        die( print_r( sqlsrv_errors(), true) );
    } 
?>