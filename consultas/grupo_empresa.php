  <?php 

  $sql_grupos_empresa =   
  " SELECT DISTINCT grp_codigo,                                 
    grp_Descricao,                                       
    grp_operadora                                        
    FROM GrupoEmpresa                                           
    INNER JOIN GrupoEmpresaItens ON gpi_grupo = grp_codigo    
    WHERE 1=1                                                   
    ORDER BY 1 desc    
  ";

  $sql_cobertura_empresa = 
  " SELECT  Emp_cd, Emp_ds   
    FROM Empresa             
    WHERE IsNull(Emp_UtilizaCoberturaDif,'N') = 'S' 
    ORDER BY 2
  ";

  $sql_obs_empresa = 
  " SELECT 
    OE_dt,
    OE_operador,
    OE_empresa,
    (SELECT emp_ds FROM empresa WHERE emp_cd = OE_empresa) 'Empresa',
    OE_obsTitulo,
    OE_obs 
    FROM ObsEmpresas
  ";

  $sql_setor_empresa = 
  " SELECT * FROM EMPRESASETOR";

  $sql_grupo_cobertura = 
  " SELECT * FROM GrupoCobertura";

  $sql_tipo_contrato = 
  " SELECT *
    FROM PrecoContrato
    WHERE PrecoContrato_cd is not null
    ORDER BY PrecoContrato_cd, PrecoContrato_ds
  ";

  $sql_operadora_repasse = 
  " SELECT *             
    FROM OperadorRepasse 
    WHERE ORep_cd IS NOT NULL
    ORDER BY ORep_cd ASC 
  ";

  $sql_regiao =
  " SELECT Reg_cd Código, Reg_ds Região
    FROM Regiao
    Where Reg_cd is not null
    ORDER BY Reg_cd
  ";

  $sql_tp_contrato = "
    SELECT * 
    FROM Tabela_Precos 
    WHERE Tab_cd IS NOT NULL 
    ORDER BY TAB_cd
  ";

  $sql_info_tb_preco = 
  "
    SELECT *,
      CAST(SUBSTRING(FAI_CD,1,3) AS INT) IDINI,
       CAST(SUBSTRING(FAI_CD,5,3) AS INT) IDFIM
    FROM TabPreco_Faixas
    WHERE FAI_TabPreco = ".$_GET['cod']."
    ORDER BY FAI_cd
  ";

  $sql_info_tb = 
  "
    SELECT tp.TAB_Cd, tp.Tab_ds, tp.TAB_CodPlano, p.PLA_ds, tp.TAB_Status
    from SipsSCSJC.dbo.Tabela_Precos as tp
    INNER JOIN SipsSCSJC.dbo.Plano as p on tp.TAB_CodPlano = p.PLA_cd
    WHERE TAB_Cd =  ".$_GET['cod'].";
  ";

  $sql_form_cob_serv_prest = 
  "
    SELECT spd.Dif_ServPrestFormas, spf.SPF_ds, spf.SPF_Empresa, spf.SPF_Tipo, spf.SPF_Calculo
    FROM ServPrestDiferenciados spd 
    INNER JOIN ServPrestFormas spf on spd.Dif_ServPrestFormas = spf.SPF_ServPrestFormas 
  ";

  $sql_forma_cobrancas=
  "
    SELECT DISTINCT sps.Ser_VersaoAMB, sps.Ser_ProcAMB, amb.c_descAMB, sps.Ser_PorcAMB, sps.Ser_VlServico, sps.SER_ESPECIALIDADE 
    FROM ServPrestServico sps
    INNER JOIN SipsSCSJC.dbo.AMB amb on amb.c_codAMB = sps.Ser_ProcAMB 
    where sps.Ser_ServPrestFormas = ".$_GET['cod'].";
  ";

  $sql_replica_limite_uso = 
  "
    SELECT DISTINCT ep.EP_empresa, ep.EP_plano, emp.EMP_ds
    FROM EmpresaPlano ep 
    INNER JOIN SipsSCSJC.dbo.Empresa emp ON emp.EMP_cd = ep.EP_empresa
  ";


  
$consulta_grupo_empresa = sqlsrv_query($conn, $sql_grupos_empresa);
if( $consulta_grupo_empresa === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_cobertura_empresa = sqlsrv_query($conn, $sql_cobertura_empresa);
if( $consulta_cobertura_empresa === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_obs_empresa = sqlsrv_query($conn, $sql_obs_empresa);
if( $consulta_cobertura_empresa === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_setor_empresa = sqlsrv_query($conn, $sql_setor_empresa);
if( $consulta_setor_empresa === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_grupo_cobertura = sqlsrv_query($conn, $sql_grupo_cobertura);
if( $sql_grupo_cobertura === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_tipo_cobertura = sqlsrv_query($conn, $sql_tipo_contrato);
if( $sql_tipo_contrato === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_operadora_repasse = sqlsrv_query($conn, $sql_operadora_repasse);
if( $sql_operadora_repasse === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_regiao = sqlsrv_query($conn, $sql_regiao);
if( $sql_regiao === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$consulta_tp_contrato = sqlsrv_query($conn, $sql_tp_contrato);
if( $sql_tp_contrato === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$sql_info_tab = sqlsrv_query($conn, $sql_info_tb);
if( $sql_info_tb === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$sql_info_preco = sqlsrv_query($conn, $sql_info_tb_preco);
if( $sql_info_tb_preco === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$sql_form_cob_serv = sqlsrv_query($conn, $sql_form_cob_serv_prest);
if( $sql_form_cob_serv_prest === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$sql_form_cob = sqlsrv_query($conn, $sql_forma_cobrancas);
if( $sql_forma_cobrancas === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$sql_replica_limite = sqlsrv_query($conn, $sql_replica_limite_uso);
if( $sql_replica_limite_uso === false) {
  die( print_r( sqlsrv_errors(), true) );
}

?>

