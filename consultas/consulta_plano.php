<?php 
$sql_cadastro_plano =
"
SELECT  Distinct P.PLA_cd, P.PLA_ds, P.PLA_simpas, P.PLA_SCPA,       
P.PLA_acomodacao, P.PLA_obs,                                 
P.PLA_encaminhar, P.PLA_carencia, C.CAR_ds, P.Pla_DescResumo,
P.Pla_Status, P.Pla_TipoContrato, PrecoContrato_ds           
FROM Plano P                                                         
LEFT OUTER JOIN Carencia C ON P.PLA_carencia = C.CAR_cd              
LEFT JOIN PrecoContrato ON Pla_TipoContrato = PrecoContrato_cd       
where P.Pla_cd is not null                                           
order by P.Pla_cd
";

$sql_cadastro_plano_full =
"
SELECT DISTINCT  P.PLA_cd, P.PLA_ds,
		P.PLA_simpas,
        P.PLA_acomodacao,
        P.PLA_obs,
        P.PLA_encaminhar,
        P.PLA_carencia,
        c.CAR_ds,
        P.PLA_FatorModerador,
        P.Pla_TipoPlano,
        P.Pla_TipoCobertura,
        pc.PrecoCobertura_ds,
        P.Pla_AbranGeog,
        pag.Abr_ds,
        P.Pla_LeiPlano,
        P.Pla_DescResumo,
        P.Pla_AcomodacaoTiss,
        tiss.TAc_ds,
        P.Pla_SCPA,
        P.Pla_Grupo, 
        gpo.gp_ds,
        P.Pla_DtVigenciaInicial,
        P.Pla_DtVigenciaFinal,
        P.Pla_Status,
        P.Pla_TipoContrato,
        pct.PrecoContrato_ds
FROM Plano P
inner join SipsSCSJC.dbo.TISS_TipoAcomodacao tiss on P.Pla_AcomodacaoTiss = tiss.TAc_cd
INNER JOIN PrecoCobertura pc ON pc.PrecoCobertura_cd = P.Pla_TipoCobertura
inner join PrecoAbranGeog pag on pag.Abr_cd = P.Pla_AbranGeog
inner join PrecoContrato pct on pct.PrecoContrato_cd = P.Pla_TipoContrato
inner join GrupoPlano gpo on gpo.gp_cd = P.Pla_Grupo
INNER JOIN Carencia c ON c.CAR_cd = P.PLA_carencia
where p.Pla_cd = '".$_GET['cod']."'
";

$sql_carencias = "
SELECT DISTINCT CAREpossivel.POS_cd, CAREpossivel.POS_ds, Car_Dias
FROM CAREpossivel, Carencia
WHERE Carencia.car_CarPossivel = CAREpossivel.POS_cd
and Carencia.Car_cd = '".$_GET['cod']."'
";

$sql_grupo_plano = 
"
Select gp_cd,
       gp_ds
From GrupoPlano
Order By gp_cd
";

$sql_grupo_plano_d =
"
SELECT gpi_grupo,
gpi_seq,
gpi_empresa,
( SELECT Emp_ds 
  FROM Empresa
  WHERE Emp_cd = gpi_empresa ) empresa
FROM GrupoEmpresaItens
WHERE gpi_grupo = '".$_GET['cod']."'
";

$sql_grupo_plano_info =
"
SELECT grp_codigo, 
       grp_Descricao,
       grp_operadora
FROM GrupoEmpresa
WHERE  grp_codigo = '".$_GET['cod']."'
";

$sql_cadastro_carencia = 
"
SELECT POS_cd Código, POS_ds Carência, 
       POS_abreviatura Abreviação, POS_seq_Impressao 'Seq impressão', 
       POS_Imprime Impressão, Pos_Sexo Sexo
FROM  CAREpossivel
WHERE POS_cd Is Not Null
ORDER BY POS_cd
";

$sql_tabela_carencia = 
"
SELECT Distinct C.CAR_cd, C.CAR_ds 
FROM  Carencia C                   
WHERE C.CAR_cd is not null         
ORDER BY CAR_cd 
";

$sql_tabela_carencia_full = 
"
SELECT C.CAR_cd, C.CAR_ds, C.car_CarPossivel, P.Pos_ds as car_CarPossivel_ds,
       C.CAR_dias, C.CAR_seq_Impressao,           
       C.CAR_validade,                            
       P.POS_cd                                   
FROM  Carencia C, CAREpossivel P                  
WHERE C.car_CarPossivel = P.POS_cd and C.Car_cd = '".$_GET['cod']."'
";

$sql_abrangencia_geo = 
"
SELECT Abr_cd, Abr_ds, Abr_PorcDesc
FROM  PrecoAbranGeog
ORDER BY Abr_ds
";

$sql_tipo_cobertura = 
"
SELECT PrecoCobertura_cd Código,
       PrecoCobertura_ds 'Preço cobertura',
       PrecoCobertura_PorcDesc 'Perc desconto',
       PrecoCobertura_RefANS 'Referencia ANS'
FROM  PrecoCobertura
ORDER BY PrecoCobertura_cd
";

$sql_tipo_cobertura_full = 
"
SELECT PrecoCobertura_cd,
               PrecoCobertura_ds,
               PrecoCobertura_PorcDesc,
               PrecoCobertura_RefANS
FROM  PrecoCobertura
WHERE PrecoCobertura_cd = '".$_GET['cod']."'
";

$sql_tipo_plano = 
"
SELECT PrecoPlano_cd Código,
       PrecoPlano_ds 'Preço Plano'
FROM  PrecoPlano
ORDER BY PrecoPlano_cd

";

$cadastro_plano = sqlsrv_query($conn, $sql_cadastro_plano);
if( $cadastro_plano === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$cadastro_plano_full = sqlsrv_query($conn, $sql_cadastro_plano_full);
if( $cadastro_plano_full === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$carencias = sqlsrv_query($conn, $sql_carencias);
if( $carencias === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$grupo_plano = sqlsrv_query($conn, $sql_grupo_plano);
if( $grupo_plano === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$grupo_plano_d = sqlsrv_query($conn, $sql_grupo_plano_d);
if( $grupo_plano_d === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$grupo_plano_info = sqlsrv_query($conn, $sql_grupo_plano_info);
if( $grupo_plano_info === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$cadastro_carencia = sqlsrv_query($conn, $sql_cadastro_carencia);
if( $cadastro_carencia === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$tabela_carencia = sqlsrv_query($conn, $sql_tabela_carencia);
if( $tabela_carencia === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$tabela_carencia_full = sqlsrv_query($conn, $sql_tabela_carencia_full);
if( $tabela_carencia_full === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$abrangencia_geo = sqlsrv_query($conn, $sql_abrangencia_geo);
if( $sql_abrangencia_geo === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$tipo_cobertura = sqlsrv_query($conn, $sql_tipo_cobertura);
if( $tipo_cobertura === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$tipo_cobertura_full = sqlsrv_query($conn, $sql_tipo_cobertura_full);
if( $sql_tipo_cobertura_full === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$tipo_plano = sqlsrv_query($conn, $sql_tipo_plano);
if( $sql_tipo_plano === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>

