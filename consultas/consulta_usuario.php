<?php
// STATUS
if($_POST['status'] == 'Ativo'){
    $status = "AND (dbo.fStatusUsuario(Usu.C_IncUsu, Usu.C_DteUsu, GetDate()) = 'A')";
}elseif($_POST['status'] == 'Cancelados'){
    $status = "AND (Usu.C_dteUsu is not null and Usu.C_dteUsu <= GetDate())";
}else{
    $status = '';
}
// CODIGO
if(!empty($_POST['codigo'])){
    $codigo = "AND (Usu.C_CTRUSU LIKE '%".$_POST['codigo']."%')";
}else{
    $codigo = '';
}
// NOME
if(!empty($_POST['nome'])){
    $nome = "AND (Usu.C_NOMUSU LIKE '%".$_POST['nome']."%')";
}else{
    $nome = '';
}
// CPF
if(!empty($_POST['cpf'])){
    $cpf = "AND (Usu.C_cicUSU LIKE '%".$_POST['cpf']."%')";
}else{
    $cpf = '';
}
// CONTRATO
if(!empty($_POST['contrato'])){
    $contrato = "AND (Usu.Usu_NroContrato LIKE '%".$_POST['contrato']."%')";
}else{
    $contrato = '';
}
// COD ABRANGE
if(!empty($_POST['cod_abrange'])){
    $cod_abrange = "AND ( Usu.C_CODIGOCOMPOSTO = '".$_POST['cod_abrange']."'
                    OR ( REPLACE(REPLACE(Usu.C_CTRUSU,'-',''),'.','') LIKE '".$_POST['cod_abrange']."%')     
                    OR (Usu.C_ABRUSU LIKE '%".$_POST['cod_abrange']."%') )";
}else{
    $cod_abrange = '';
}
// DT NASCIMENTO
if(!empty($_POST['data_nasc'])){
    $data_nasc = "AND (Usu.C_NASUSU = '".$_POST['data_nasc']."')";
}else{
    $data_nasc = '';
}
// CCO
if(!empty($_POST['CCO'])){
    $CCO = "AND (Com.Usu_CCOANS = '".$_POST['CCO']."')";
}else{
    $CCO = '';
}
// Cod. Interno 
if(!empty($_POST['cod_interno'])){
    $cod_interno = "AND usu_interno LIKE '%".$_POST['cod_interno']."%'";
}else{
    $cod_interno = '';
}
// Empresa
if(!empty($_POST['empresa'])){
    $cod_empresa = explode(" ", $_POST['empresa']);
    $empresa = "AND (Emp.Emp_cd = '".$cod_empresa[0]."')";
}else{
    $empresa = '';
}
// Plano
if(!empty($_POST['plano'])){
    $cod_plano = explode(" ", $_POST['plano']);
    $plano = "AND (Plano.Pla_cd = '".$cod_plano[0]."')";
}else{
    $plano = '';
}

$full_sql = 
"
SELECT                           
       Usu.C_CTRUSU,             
       Usu.C_NOMUSU, 
       Usu.C_SEXUSU,
       Usu.C_ENDUSU,
       Usu.Usu_EndNum,
       Usu.Usu_EndComp,
       Usu.C_ESTUSU,
       Usu.C_CEPUSU,
       Usu.C_TIPUSU,
       Emp.EMP_ds,
       Plano.Pla_ds,
       Usu.C_VlmUsu,
       Usu.C_IncUsu,
       Usu.C_DteUsu,
       Usu.Usu_DtCarencia,
       Com.Usu_DataCadastro,
       Com.Usu_OperadorINC,
       Com.Usu_DataAlteracao,
       Com.Usu_OperadorALT,
       Com.C_RgUsu,
       Com.Usu_CCOANS,
       Plano.Pla_Obs,
       Usu.USU_CORRETORA,
       Com.C_EmailUsu,
       Cid_ds Naturalidade,
       C_NATEST,
       Usu_CodSFO,
       Usu.C_CODIGOCOMPOSTO
FROM T_ARQUSU Usu
  INNER JOIN Empresa Emp ON Emp.EMP_cd = Usu.C_EMPUSU
  LEFT JOIN Plano ON Plano.PLA_cd = Usu.C_PLAUSU
  LEFT JOIN Usu_Complemento Com ON Com.C_CtrUsu = Usu.C_CtrUsu
  LEFT JOIN Cidade ON Cid_cd = Usu.C_NATUSU
WHERE Usu.C_CtrUsu IS NOT NULL
".$status.$codigo.$nome.$cpf.$contrato.$cod_abrange.$data_nasc.$CCO.$cod_interno.$empresa.$plano."
ORDER BY Usu.C_CTRUSU
";

$full_sql_id = 
"
SELECT                           
       Usu.C_CTRUSU,             
       Usu.C_NOMUSU, 
       Usu.C_SEXUSU,
       Usu.C_ENDUSU,
       Usu.Usu_EndNum,
       Usu.Usu_EndComp,
       Usu.C_ESTUSU,
       Usu.C_CEPUSU,
       Usu.C_TIPUSU,
       Emp.EMP_ds,
       Plano.Pla_ds,
       Usu.C_VlmUsu,
       Usu.C_IncUsu,
       Usu.C_DteUsu,
       Usu.Usu_DtCarencia,
       Com.Usu_DataCadastro,
       Com.Usu_OperadorINC,
       Com.Usu_DataAlteracao,
       Com.Usu_OperadorALT,
       Com.C_RgUsu,
       Com.Usu_CCOANS,
       Plano.Pla_Obs,
       Usu.USU_CORRETORA,
       Com.C_EmailUsu,
       Cid_ds Naturalidade,
       C_NATEST,
       Usu_CodSFO,
       Usu.C_CODIGOCOMPOSTO
FROM T_ARQUSU Usu
  INNER JOIN Empresa Emp ON Emp.EMP_cd = Usu.C_EMPUSU
  LEFT JOIN Plano ON Plano.PLA_cd = Usu.C_PLAUSU
  LEFT JOIN Usu_Complemento Com ON Com.C_CtrUsu = Usu.C_CtrUsu
  LEFT JOIN Cidade ON Cid_cd = Usu.C_NATUSU
WHERE Usu.C_CtrUsu IS NOT NULL
AND (Usu.C_CTRUSU LIKE  '".$_GET['cod']."')
ORDER BY Usu.C_CTRUSU
";

$sql_financeiro = 
"
SELECT C_CTRUSU, C_MAEUSU, C_RGUSU, C_OBIUSU, C_OC1USU, C_OC2USU, C_DVCUSU,
       C_CUSUSU, C_EXAUSU, C_EMCUSU, C_PISUSU, C_OEXUSU, C_MEDUSU, C_EMAILUSU, C_EMAILUSU2,
       Usu_profissao, Usu_DtAdmissao, Usu_ContaCrr , Usu_NroCNS, Usu_RgOrgaoEmissor, Usu_RgPaisEmissor,
       Usu_DataCadastro, Usu_DataAlteracao, Usu_OperadorINC, Usu_OperadorALT, Usu_MotInclusao,
       Usu_InformacoesComplentares, Usu_FoneCom, Usu_FoneCel,Usu_OpPortabilidade,Usu_PlaPortabilidade,
       Usu_ObsPortabilidade, Usu_CCOANS, Usu_PaisResid, Usu_MesReajusteDiferenciado, C_PAIUSU,
       Usu_PlaMigradoAdaptado, Usu_DataMigracaoAdaptPlano, Usu_NomeBenefCartao, 
      Usu_SCPA_Alternativo, Usu_VinculoEmpregaticio, Usu_EmpresaSetor, Usu_InicioFatDif, C_DTEXPEDICAORG
FROM usu_Complemento 
WHERE C_CTRUSU =  '".$_GET['cod']."'
";


$sql_dados_bancarios = 
"
SELECT
   CC_Banco,
   CC_Agencia,
   CC_Cd,
   CC_Codigo
FROM
   Conta_Corr
WHERE
   CC_Codigo = '".$_SESSION['Usu_ContaCrr']."'
";

$sql_carencias = 
"
SELECT *, CP.POS_ds FROM USU_CARE_Possivel USP
INNER JOIN SipsSCSJC.dbo.CAREpossivel CP ON USP.UCP_CPos = CP.POS_cd
WHERE UCP_Usu = '".$_GET['cod']."'
ORDER BY Ucp_Usu, Ucp_Seq_Impressao
";

$sql_dados_familia =
"
SELECT
  * 
FROM
   t_Arqusu
WHERE C_CTRUSU = '".$_GET['cod']."'
";




$consulta_usuario = sqlsrv_query($conn, $full_sql);
    if( $full_sql === false) {
        die( print_r( sqlsrv_errors(), true) );
    }


$full_id = sqlsrv_query($conn, $full_sql_id);
    if( $full_sql_id === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$financeiro = sqlsrv_query($conn, $sql_financeiro);
    if( $sql_financeiro === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$dados_bancarios = sqlsrv_query($conn, $sql_dados_bancarios);
    if( $sql_dados_bancarios === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$carencias = sqlsrv_query($conn, $sql_carencias);
    if( $sql_carencias === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$dados_familia = sqlsrv_query($conn, $sql_dados_familia);
    if( $sql_dados_familia === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
?>

