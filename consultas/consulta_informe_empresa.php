<?php 

$tip_cod = $_POST['tip_cod'];
$empresa = $_POST['empresa'];
$contrato = $_POST['cod_contrato'];
$fantasia = $_POST['fantasia'];

############ Código Empresa ####################

if(isset($_POST['cod_empresa'])){
  $cod_empresa = $_POST['cod_empresa'];
}else{
  $cod_empresa = '';
}

if ($tip_cod == 'Todos') {
  $codigo = 'e.EMP_cd Is Not Null';
}elseif ($tip_cod == 'Igual') {
  $codigo = "EMP_cd = ".$cod_empresa."";
}elseif ($tip_cod == 'Maior Igual') {
  $codigo = "EMP_cd >= ".$cod_empresa."";
}elseif ($tip_cod == 'Menor Igual') {
  $codigo = "EMP_cd <= ".$cod_empresa."";
}elseif ($tip_cod == 'Diferente') {
  $codigo = "EMP_cd <> ".$cod_empresa."";
}
#################################################

############ Contrato ###########################

if(strlen($contrato) >= 1) {
  $cd_contrato = "and e.EMP_NroContrato like '".$contrato."'";
}else{
  $cd_contrato = "";
}
#################################################

############ Nome Empresa #######################

if ($empresa == 'Todas') {
  $nm_empresa = "";
}elseif ($empresa == 'Começa com') {
  $nm_empresa = "and  UPPER(EMP_ds) Like UPPER('".$_POST['nm_empresa']."%')";
}elseif ($empresa == 'Palavra Chave') {
  $nm_empresa = "and  UPPER(EMP_ds) Like UPPER('%".$_POST['nm_empresa']."%')";
}elseif ($empresa == 'Igual') {
  $nm_empresa = "and  UPPER(EMP_ds) = UPPER('".$_POST['nm_empresa']."')";
}
##################################################

############ Nome Fantasia #######################

if ($fantasia == 'Todas') {
  $nm_fantasia = "";
}elseif ($fantasia == 'Começa com') {
  $nm_fantasia = "UPPER(Emp_Fantasia) Like UPPER('".$_POST['nm_fantasia']."%')";
}elseif ($fantasia == 'Palavra Chave') {
  $nm_fantasia = "UPPER(Emp_Fantasia) Like UPPER('%".$_POST['nm_fantasia']."%')";
}elseif ($fantasia == 'Igual') {
  $nm_fantasia = "UPPER(Emp_Fantasia) = UPPER('".$_POST['nm_fantasia']."')";
}

###################################################

###################### CNPJ #######################

if (empty($_POST['cnpj'])) {
  $cnpj = '';
}else{
  $cnpj = "and EMP_CGC = ".$_POST['cnpj']."";
}
###################################################

###################### Tipo Cobranca ##############
if ($_POST['tip_cobranca'] == 'E' or $_POST['tip_cobranca'] == 'U') {
  $tp_cob = "and  e.Emp_TipoCobranca = '".$_POST['tip_cobranca']."'";
}else{
  $tp_cob = "";
}

###################################################

###################### Tipo Pessoa ################

if ($_POST['tipo_pessoa'] == 'F' or $_POST['tipo_pessoa'] == 'J') {
  $tp_pessoa = "and  e.Emp_Pessoa = '".$_POST['tipo_pessoa']."'";
}else{
  $tp_pessoa = "";
}

###################################################

############## Repasse Serviço ####################

if ($_POST['rep_servico'] == 'N' or $_POST['rep_servico'] == 'S' or $_POST['rep_servico'] == 'R') {
  $rep_servico = "and  c.Cob_Repasse = '".$_POST['rep_servico']."'";
}else{
  $rep_servico = "";
}

###################################################

####################### DATAS #####################

if(empty($_POST['data_ini']) or empty($_POST['data_fim'])){
  $data_inclusao = '';
}else{
  $data_inclusao = "and Emp_dtInicio BETWEEN '".$_POST['data_ini']."' and '".$_POST['data_fim']."'";
}

###################################################

############### MÊS DE REAJUSTE#####################

if ($_POST['mes_reajuste'] == '0') {
  $reajuste = '';
}else{
  $reajuste = "and  IsNull( e.Emp_MesReajuste,0) = ".$_POST['mes_reajuste']."";
}

###################################################

$consulta_completa = "SELECT e.EMP_cd, e.EMP_ds, e.EMP_endereco,e.EMP_EndNum, e.EMP_CEP,
       e.EMP_bairro, e.EMP_cidade, e.EMP_UF,
       e.EMP_contato, e.EMP_IE, e.EMP_CGC, e.EMP_CPF,
       e.EMP_fone1, e.EMP_fone2, e.EMP_agregado,
       e.EMP_inclusao, e.EMP_carencia,e.EMP_fator,
       e.EMP_exclusao, e.EMP_adicional, e.EMP_mail,
       e.emp_PESSOA, e.EMP_CobFatorModerador, e.EMP_DtTermino,
       CASE WHEN e.EMP_TipoCobranca = 'U' THEN 'Usuário'  
            WHEN e.EMP_TipoCobranca = 'E' THEN 'Empresa' ELSE 'NÃO INFORMADO' END EMP_TipoCobranca, 
            e.EMP_NroContrato, e.EMP_IdadeDep,e.EMP_EmpRepasse, 
       e.EMP_Pagamento, e.Emp_Fantasia, CONVERT(VARCHAR(MAX),e.Emp_Obs) Emp_Obs, e.Emp_dtInicio,E.Emp_Regiao, 
       E.Emp_ObsCarteirinha, E.Emp_TipoContrato, E.Emp_OperadorRepasse, 
       e.Emp_PlanoFamiliar, e.Emp_PlanoIndividual, e.Emp_MudaPlano, 
       e.Emp_SipTipoBen, e.Emp_SipTipoCont, e.Emp_MesReajuste, e.Emp_IR, e.Emp_VlMiminoIR, 
       e.Emp_DescricaoComp, e.Emp_IM,e.Emp_CustoOperacional, 
       CONVERT(VARCHAR(MAX),e.EMP_ObsCobertura) EMP_ObsCobertura, CONVERT(VARCHAR(MAX), e.EMP_ObsOutros) EMP_ObsOutros, e.EMP_RegistroANS,
       (SELECT COUNT(*) FROM T_ARQUSU WHERE dbo.fStatusUsuario(C_INCUSU,C_DTEUSU,GETDATE())= 'A' and (C_TipUsu <> 'T' OR C_TipUsu IS NULL) and (C_EmpUsu = e.Emp_cd)) QtdUsuAtivos, 
       (SELECT IsNull(COUNT(*),0) FROM Historico_Inadimplentes WHERE hi_empresa = E.Emp_cd and hi_status not in ('R','F')) HistInadiplentes, 
       e.EMP_CodOrigem, e.Emp_DtCadastro,e.Emp_RN, e.Emp_RN309, e.EMP_PlaMigradoAdaptado, e.Emp_CEI, MCAN_Ds, e.Emp_CalcIssFat,e.Emp_NaoEnviaPortal,e.Emp_UtilizaCoberturaDif, EMP_EndNum 
       ,e.Emp_AdmBeneficio, e.Emp_AtividadeEconomica, 
       e.Emp_Complemento,                             
       e.EMP_EmailAdm,                                
       e.Emp_RN195, e.Emp_DtAdapRN195,                
CONVERT(INT, NULL) AS PLA_cd, CONVERT(VARCHAR, NULL) AS PLA_ds,
 e.Emp_MotInclusao 
FROM  Empresa e
   LEFT JOIN CobrancaEMP c on e.EMP_cd = c.Cob_emp
   LEFT JOIN Mot_Cancelamento ON MCAN_Cd = EMP_MotCancelamento_MCAN_Cd
WHERE ".$codigo." ".$nm_empresa." ".$cnpj." ".$cd_contrato." ".$nm_fantasia." ".$tp_cob." ".$tp_pessoa." ".$rep_servico." ".
$reajuste."ORDER BY EMP_cd";


$consulta = sqlsrv_query($conn, $consulta_completa);
if( $consulta_completa === false) {
  die( print_r( sqlsrv_errors(), true) );
}

?>

