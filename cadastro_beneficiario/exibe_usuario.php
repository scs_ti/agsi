<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_usuario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Informe Usuários</h3>
    </div><br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Sexo</th>
                            <th>Enderço</th>
                            <th>Número</th>
                            <th>Comple</th>
                            <th>Visualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $consulta_usuario, SQLSRV_FETCH_ASSOC) ) {
                            echo "<tr>
                            <td>".$row['C_CTRUSU'].                            "</td>
                            <td>".$row['C_NOMUSU'].                            "</td>
                            <td>".$row['C_SEXUSU'].                            "</td>
                            <td>".$row['C_ENDUSU'].                            "</td>
                            <td>".$row['Usu_EndNum'].                          "</td>
                            <td>".$row['Usu_EndComp'].                         "</td>
                            <td><a href='exibe_usuario_comple.php?cod=".$row['C_CTRUSU']."'>
                                    <button class='btn btn-primary'>Visualizar</button></a></td>
                          </tr>";
                        }
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>












<!-- <td>".$row['C_ESTUSU'].                            "</td>
                            <td>".$row['C_CEPUSU'].                            "</td>
                            <td>".$row['C_TIPUSU'].                            "</td>
                            <td>".$row['EMP_ds'].                            "</td>
                            <td>".$row['Pla_ds'].                            "</td>
                            <td>".$row['C_VlmUsu'].                            "</td>
                            <td>".date_format($row['C_IncUsu'], 'd/m/Y').     "</td>
                            <td>".$row['C_DteUsu'].                            "</td>
                            <td>".date_format($row['Usu_DtCarencia'], 'd/m/Y').             "</td>
                            <td>".date_format($row['Usu_DataCadastro'], 'd/m/Y').                            "</td>
                            <td>".$row['Usu_OperadorINC'].                            "</td>
                            <td>".date_format($row['Usu_DataAlteracao'], 'd/m/Y').                            "</td>
                            <td>".$row['Usu_OperadorALT'].                            "</td>
                            <td>".$row['C_RgUsu'].                            "</td>
                            <td>".$row['Usu_CCOANS'].                            "</td>
                            <td>".$row['Pla_Obs'].                            "</td>
                            <td>".$row['USU_CORRETORA'].                            "</td>
                            <td>".$row['C_EmailUsu'].                            "</td>
                            <td>".$row['Naturalidade'].                            "</td>
                            <td>".$row['C_NATEST'].                            "</td>
                            <td>".$row['Usu_CodSFO'].                            "</td>
                            <td>".$row['C_CODIGOCOMPOSTO'].                            "</td>