<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/obs_usuario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Indicação Usuário</h3>
    </div><br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Sexo</th>
                            <th>Empresa</th>
                            <th>Plano</th>
                            <th>Cidade</th>
                            <th>Idade</th>
                            <th>Visualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array($indicacao_usuario, SQLSRV_FETCH_ASSOC) ) {
                            echo "<tr>
                            <td>".$row['C_CTRUSU'].                            "</td>
                            <td>".$row['C_NOMUSU'].                            "</td>
                            <td>".$row['C_SEXUSU'].                            "</td>
                            <td>".$row['EMP_DS'].                            "</td>
                            <td>".$row['PLA_DS'].                            "</td>
                            <td>".$row['CID_DS'].                            "</td>
                            <td>".$row['Idade'].                            "</td>
                            <td><a href='indicacao_usuario_desc.php?cod=".$row['C_CTRUSU']."'>
                                    <button class='btn btn-primary'>Visualizar</button></a></td>
                          </tr>";
                        }
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

