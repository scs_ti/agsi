<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php';
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}

$empresa = sqlsrv_query($conn, "select EMP_cd, EMP_ds from Empresa");
if( $empresa === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>



<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Informe Usuários</h3>
            </div>
        </div>
    </div>

    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_usuario.php" method="POST">
                                <!--<form class="form" action="consultas/informe_usuario.php" method="POST">-->
                                <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Código</label>
                                                <input type="text" onkeypress="mascara(this,cod)" maxlength="19" name="codigo" class="form-control" placeholder="Entre com o código" name="lname-column">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Nome</label>
                                                <input type="text" id="first-name-column" name="nome" class="form-control"
                                                placeholder="Insira o Nome" name="fname-column">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>CPF</label>
                                                <input type="text" name="cpf" onBlur="validaCPF(this)" maxlength="11" class="form-control" placeholder="xxx.xxx.xxx-xx">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Contrato</label>
                                                <input type="text" class="form-control" placeholder="" name="contrato">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Cod. Abrange</label>
                                                <input type="text" name="cod_abrange" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    <option>Ativo</option>
                                                    <option>Cancelados</option>
                                                    <option>Ambos</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Data de Nascimento</label>
                                                <input type="date" name="data_nasc" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>CCO</label>
                                                <input type="text" name="CCO" class="form-control">
                                            </div>
                                        </div> 
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Cód. Interno</label>
                                                <input type="text" name="cod_interno" class="form-control">
                                            </div>
                                        </div>                                                  
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Selecione a Empresa</label>
                                                <select class="form-control" name="empresa">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option>".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label>Selecione o Plano</label>
                                                <select class="form-control" name="plano">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $quarto, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option>".$row['PLA_cd']." - ".$row['PLA_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset" class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- // Basic multiple Column Form section end -->
    </div>

    <?php include '../html/footer.html'?>