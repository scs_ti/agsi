<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_usuario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 d-flex col-md-6 order-md-1">
                <a href="javascript:history.back()" style="color: white;"><button class="btn btn-primary me-1 mb-1">Voltar</a></button>
                <h3>Informe Usuário</h3>
            </div>
        </div>      
    </div>
    <br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                
                                    <?php while( $row = sqlsrv_fetch_array( $full_id, SQLSRV_FETCH_ASSOC) ) {
                                            echo "  <div class='row'>
                                                        <div class='col-md-4 col-12'>
                                                            <div class='form-group'>
                                                                <label>Código Usuário</label>
                                                                <input type='text' value='".$row['C_CTRUSU']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                                placeholder='Insira o Nome' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-12'>
                                                            <div class='form-group'>
                                                                <label>Contrato</label>
                                                                <input type='text' value='".$row['EMP_ds']."' readonly='readonly' id='first-name-column' name='nome' class='form-control'
                                                                placeholder='Insira o Nome' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-12'>
                                                            <div class='form-group'>
                                                                <label>Plano</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['Pla_ds']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-12'>
                                                            <div class='form-group'>
                                                                <label>Plano</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_NOMUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-12'>
                                                            <div class='form-group'>
                                                                <label>Taxa Mensal</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='R$ ".$row['C_VlmUsu']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Data de Inclusão</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".date_format($row['C_IncUsu'], 'd/m/Y').     "' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Data de cadastro</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".date_format($row['Usu_DataCadastro'], 'd/m/Y').     "' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Endereço</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_ENDUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>CEP</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_CEPUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>

                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>UF</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_ESTUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Número</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['Usu_EndNum']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Complemento</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['Usu_EndComp']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>RG</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_RgUsu']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
                                            }while( $row = sqlsrv_fetch_array( $financeiro, SQLSRV_FETCH_ASSOC) ) {

                                            $_SESSION['Usu_ContaCrr'] = $row['Usu_ContaCrr'];

                                            echo " <div class='row'>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Orgão</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['Usu_RgOrgaoEmissor']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-4 col-12'>
                                                            <div class='form-group'>
                                                                <label>Observação</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_OBIUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-3 col-12'>
                                                            <div class='form-group'>
                                                                <label>Nome da Mãe</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_MAEUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-3 col-12'>
                                                            <div class='form-group'>
                                                                <label>Nome do Pai</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_PAIUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
                                            }while( $row = sqlsrv_fetch_array( $dados_bancarios, SQLSRV_FETCH_ASSOC) ) {
                                            echo " <div class='row'>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Banco</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['CC_Banco']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Agência</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['CC_Agencia']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Conta Banco</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['CC_Cd']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                    </div>";
                                            }echo "<br><h3>Dados da Familia</h3>";
                                            while( $row = sqlsrv_fetch_array( $dados_familia, SQLSRV_FETCH_ASSOC) ) {

                                            $hoje = date('d/m/Y');
                                            $dt_nasc = date_format($row['C_NASUSU'], 'd/m/Y');
                                            $exp_hj = explode("/", $hoje);
                                            $exp_nasc = explode("/", $dt_nasc);

                                            if($exp_hj[1] > $exp_nasc[1]){
                                                $idade = $exp_hj[2] - $exp_nasc[2];
                                            }elseif($exp_hj[1] < $exp_nasc[1]){
                                                $idade = $exp_hj[2] - $exp_nasc[2] - 1;
                                            }elseif ($exp_hj[1] == $exp_nasc[1] and $exp_hj[0] <= $exp_nasc[0]) {
                                                $idade = $exp_hj[2] - $exp_nasc[2];
                                            }elseif ($exp_hj[1] == $exp_nasc[1] and $exp_hj[0] > $exp_nasc[0]) {
                                                $idade = $exp_hj[2] - $exp_nasc[2] - 1;
                                            }

                                            if(is_null($row['Usu_DtSuspenso'])){
                                                $dt_fim = '';
                                            }else{
                                                $dt_fim = date_format($row['Usu_DtSuspenso'], 'd/m/Y');
                                            }

                                            $_SESSION['bairro'] = $row['C_BAIUSU'];

                                            

                                            echo " <div class='row'>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Código</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_CTRUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Nome</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_NOMUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>CPF</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_CICUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Parentesco</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['C_PARUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Dt. Nascimento</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".date_format($row['C_NASUSU'], 'd/m/Y')."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Dt. Término</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$dt_fim."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>        
                                                                <label>Dt Carência</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".date_format($row['C_CARUSU'], 'd/m/Y')."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Dt Inclusão</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".date_format($row['C_INCUSU'], 'd/m/Y')."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Valor Mensalidade</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='R$ ".$row['C_VLMUSU']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Tipo Dependente</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$row['Usu_TipoDependente']."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                        <div class='col-md-2 col-12'>
                                                            <div class='form-group'>
                                                                <label>Idade Atual</label>
                                                                <input type='text' id='first-name-column' name='nome' readonly='readonly' class='form-control'
                                                                value='".$idade."' name='fname-column'>
                                                            </div>
                                                        </div>
                                                    </div>";
                                            }
                                    ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Seq. Impressão</th>
                            <th>Código</th>
                            <th>Procedimento</th>
                            <th>Dias</th>
                            <th>Imprime?</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $carencias, SQLSRV_FETCH_ASSOC) ) {
                            echo "<tr>
                            <td>".$row['UCP_seq_Impressao'].                            "</td>
                            <td>".$row['UCP_CPos'].                            "</td>
                            <td>".$row['POS_ds'].                            "</td>
                            <td>".$row['UCP_dias'].                            "</td>
                            <td>".$row['UCP_imprime'].                            "</td>
                          </tr>";
                        }
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>


</div>
<?php include '../html/footer.html'?>