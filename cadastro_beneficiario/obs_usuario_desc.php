<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_cad_beneficiario.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Observação Usuário</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                
                            <?php
                                while( $row = sqlsrv_fetch_array($obs_usu_full, SQLSRV_FETCH_ASSOC) ) {
                                    if($row['Aler_DtFinal'] != ''){$x = date_format($row['Aler_DtFinal'], 'd/m/Y');
                                    }else{ $x = 'N/A';}
                                echo "
                                <div class='row'>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Código</label>
                                        <input type='text' class='form-control' disabled value=".$row['Aler_cd'].">
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Cód Usuário</label>
                                        <input type='text' value='".$row['Aler_Usuario']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Cod Emp</label>
                                        <input type='text' value='".$row['Aler_Emp']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Plano</label>
                                        <input type='text' value='".$row['Aler_Pla']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Dt Início</label>
                                        <input type='text' value='".date_format($row['Aler_DtInicial'], 'd/m/Y')."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Dt Termino</label>
                                        <input type='text' value='".$x."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-8 col-12'>
                                        <label>Descrição do Alerta</label>
                                        <input type='text' value='".$row['Aler_Alerta']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Alerta eterno</label>
                                        <input type='text' value='".$row['Aler_Eterno']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Alerta para</label>
                                        <input type='text' value='".$row['Aler_Tipo']."' readonly='readonly' class='form-control'>
                                    </div>
                                    <div class='form-group col-md-2 col-12'>
                                        <label>Tipo de Alerta</label>
                                        <input type='text' value='".$row['Aler_Bloquear']."' readonly='readonly' class='form-control'>
                                    </div>
                                </div>";}
                            ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

