<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';
include '../consultas/consulta_informe_credenciados.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Observação Usuário</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="obs_usuario.php" method="POST">
                                    <div class="row">
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Cód Empresa</label>
                                                <input type="text" name="cod_emp" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-10 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <input type="text" name="emp" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Cód Usuário</label>
                                                <input type="text" name="cod_usu" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-10 col-12">
                                            <div class="form-group">
                                                <label>Usuário</label>
                                                <input type="text" name="usu" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Alerta</label>
                                                <input type="text" name="alerta" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Tipo</label>
                                                <select id="options" class="form-control" name="status" onchange="verifica_desc(this.value)">
                                                    <option value="T">Todos</option>
                                                    <option value="E">Empresa</option>
                                                    <option value="U">Usuário</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Tipo</label>
                                                <select id="options" class="form-control" name="tipo" onchange="verifica_desc(this.value)">
                                                    <option value="T">Todas</option>
                                                    <option value="A">Avisar</option>
                                                    <option value="B">Bloquear</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>