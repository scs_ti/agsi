<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>


<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Cadastro Beneficiário</h3>
            </div>
        </div>
    </div>

    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
            <div class="col-12">
                <div class="card">
                        <div class="card-body">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <th scope="row">1</th>
                                              <td>Usuário</td>
                                              <td style="padding-left: 15%"><a href="usuario.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                            </tr>
                                          <tr>
                                              <th scope="row">2</th>
                                              <td>Observação para Usuário</td>
                                              <td style="padding-left: 15%"><a href="obs_usu.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">3</th>
                                              <td>Indicação para Usuário</td>
                                              <td style="padding-left: 15%"><a href="indicacao_usuario.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">4</th>
                                              <td>Função / Profissão</td>
                                              <td style="padding-left: 15%"><a href="funcao_profissao.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">5</th>
                                              <td>Motivos de Bloqueio</td>
                                              <td style="padding-left: 15%"><a href="motivo_bloqueio.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">6</th>
                                              <td>Motivo de Cancelamento</td>
                                              <td style="padding-left: 15%"><a href="motivo_cancelamento.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">7</th>
                                              <td>Tipos de vinculos de Beneficiário</td>
                                              <td style="padding-left: 15%"><a href="vinculo_beneficiario.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">8</th>
                                              <td>Motivo de Carteirinhas</td>
                                              <td style="padding-left: 15%"><a href="motivo_carteirinha.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                        </div>
                </div>
            </div>
  </section>
  <!-- // Basic multiple Column Form section end -->
</div>

<style>
    .td_left {
        padding-left: 20%;
    }
</style>


<?php include '../html/footer.html'?>