<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/obs_usu.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Observação Usuário</h3>
    </div><br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Plano</th>
                            <th>Cód Usuário</th>
                            <th>Nome</th>
                            <th>Observação</th>
                            <th>Dt Início</th>
                            <th>Dt Termino</th>
                            <th>Alerna eterno</th>
                            <th>Cód</th>
                            <th>Visualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array($full, SQLSRV_FETCH_ASSOC) ) {

                            if($row['Aler_DtFinal'] != ''){
                                $df = date_format($row['Aler_DtFinal'], 'd/m/Y');
                            }else{
                                $df = 'N/A';
                            }

                            echo "<tr>
                            <td>".$row['Plano'].                                    "</td>
                            <td>".$row['Aler_Usuario'].                             "</td>
                            <td>".$row['c_NomUsu'].                                 "</td>
                            <td>".$row['Aler_Alerta'].                              "</td>
                            <td>".date_format($row['Aler_DtInicial'], 'd/m/Y').     "</td>
                            <td>".$df.                                               "</td>
                            <td>".$row['Aler_Eterno'].                              "</td>
                            <td>".$row['Aler_cd'].                                  "</td>        
                            <td><a href='obs_usuario_desc.php?cod=".$row['Aler_cd']."'>
                                    <button class='btn btn-primary'>Visualizar</button></a></td>
                          </tr>";
                        }
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

