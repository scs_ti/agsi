<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Informe Empresa</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="informe_empresa_result.php" method="POST">
                                    <div class="row">
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Código</label>
                                                <select id="options" name="tip_cod" class="form-control" onchange="verifica(this.value)">
                                                    <option value="Todos">Todos</option>
                                                    <option value="Igual">Igual</option>
                                                    <option value="Maior Igual">Maior Igual</option>
                                                    <option value="Menor Igual">Menor Igual</option>
                                                    <option value="Diferente">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" name="cod_empresa" id="input" onkeypress="mascara(this,cod)" maxlength="19"  class="form-control" placeholder="Entre com o código" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <select id="options" class="form-control" name="empresa" onchange="verifica_desc(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Começa com</option>
                                                    <option value="Palavra Chave">Palavra Chave</option>
                                                    <option value="Igual">Igual</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" id="input2" maxlength="19" name="nm_empresa" class="form-control" placeholder="Entre com o código" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Fantasia</label>
                                                <select class="form-control" name="fantasia" onchange="verifica_cidade(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Começa com">Começa com</option>
                                                    <option value="Palavra Chave">Palavra Chave</option>
                                                    <option value="Igual">Igual</option>
                                                    <option value="Diferente">Diferente</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label></label>
                                                <input type="text" name="nm_fantasia" id="input3" maxlength="19" class="form-control" placeholder="Entre com a cidade" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>CNPJ</label>
                                                <input type="text" name="cnpj" maxlength="19" class="form-control" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Contrato</label>
                                                <input type="text" name="cod_contrato" maxlength="19" class="form-control" placeholder="" >
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo Cobrança</label>
                                                <select class="form-control" name="tip_cobranca">
                                                    <option>Todas</option>
                                                    <option value="E">Empresa</option>
                                                    <option value="U">Usuário</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Tipo Pessoa</label>
                                                <select class="form-control" name="tipo_pessoa">
                                                    <option value="">Todas</option>
                                                    <option value="F">Física</option>
                                                    <option value="J">Jurídica</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Repasse Serviço</label>
                                                <select class="form-control" name="rep_servico">
                                                    <option value="">Todas</option>
                                                    <option value="N">Normal</option>
                                                    <option value="S">Repasse</option>
                                                    <option value="R">Repassada</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Data de Inclusão inicial</label>
                                                <input type="date" class="form-control" placeholder="" name="data_ini">
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Data de Inclusão final</label>
                                                <input type="date" class="form-control" placeholder="" name="data_fim">
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Mês de reajuste</label>
                                                <select class="form-control" name="mes_reajuste">
                                                    <option value="0">Todos</option>
                                                    <option value="1">01 - Janeiro</option>
                                                    <option value="2">02 - Fevereiro</option>
                                                    <option value="3">03 - Março</option>
                                                    <option value="4">04 - Abril</option>
                                                    <option value="5">05 - Maio</option>
                                                    <option value="6">06 - Junho</option>
                                                    <option value="7">07 - Setembro</option>
                                                    <option value="8">08 - Agosto</option>
                                                    <option value="9">09 - Setembro</option>
                                                    <option value="10">10 - Outubro</option>
                                                    <option value="11">11 - Novembro</option>
                                                    <option value="12">12 - Dezembro</option>
                                                </select>
                                            </div>
                                        </div>                                        
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>