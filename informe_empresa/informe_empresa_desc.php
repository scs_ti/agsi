<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_informe_empresa_desc.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="informe_empresa.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Informe Empresa</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                <div class="row">
                                <?php 
                                    while( $row = sqlsrv_fetch_array( $contabilidade, SQLSRV_FETCH_ASSOC) ) {
                                    echo "
                                    <h3>Contabilidade</h3>
                                    <div class='col-md-1 col-12'>
                                        <div class='form-group'>
                                            <label>Cód</label>
                                            <input type='text' class='form-control' value='".$row['Cob_CtaCtabil']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-5 col-12'>
                                        <div class='form-group'>
                                            <label>Desc Conta Contábil</label>
                                            <input type='text' class='form-control' value='".$row['ct_contabil']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-1 col-12'>
                                        <div class='form-group'>
                                            <label>Cód</label>
                                            <input type='text' class='form-control' value='".$row['ct_contabil_fm']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-5 col-12'>
                                        <div class='form-group'>
                                            <label>Desc Conta Contábil de Fator Moderado</label>
                                            <input type='text' class='form-control' value='".$row['ct_PPCNG']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-1 col-12'>
                                        <div class='form-group'>
                                            <label>Cód</label>
                                            <input type='text' class='form-control' value='".$row['Cob_CtaCtabilPPCNG']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-5 col-12'>
                                        <div class='form-group'>
                                            <label>Desc Conta Contábil PPCNG</label>
                                            <input type='text' class='form-control' value='".$row['ct_contabil_receb']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-1 col-12'>
                                        <div class='form-group'>
                                            <label>Cód</label>
                                            <input type='text' class='form-control' value='".$row['Cob_CtaCtabilRecAnt']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-5 col-12'>
                                        <div class='form-group'>
                                            <label>Desc Conta Contábil de Recebimento Antecipado</label>
                                            <input type='text' class='form-control' value='".$row['tip_pessoa']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    ";
                                }?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <h3>Dados Cobrança</h3>
                    <thead>
                        <tr>
                            <th>Cód Banco</th>
                            <th>Banco</th>
                            <th>Cód Agência</th>
                            <th>Agência</th>
                            <th>Cód Conta</th>
                            <th>Descrição Conta</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $desc_cobranca, SQLSRV_FETCH_ASSOC) ) {
                            echo "Descrição: ".$row['EVencto_ds'].", Dia de Vencimento: ".$row['EVencto_dia'].", Número
                            de parcelas: ".$row['EVencto_parcela'].", Tipo de Cobrança: ".$row['EVencto_Tcobranca']."";
                        }

                        while( $row = sqlsrv_fetch_array( $dados_cobranca, SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                        <td>".$row['Age_cd'].                                    "</td>
                        <td>".$row['Age_ds'].                                    "</td>
                        <td>".$row['Ban_cd'].                              "</td>
                        <td>".$row['Ban_ds'].                                "</td>
                        <td>".$row['CC_cd'].                                   "</td>
                        <td>".$row['CC_ds'].                                "</td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <h3>Histórico de Reajuste</h3>
                    <thead>
                        <tr>
                            <th>Cód Empresa</th>
                            <th>Empresa</th>
                            <th>Cód Plano</th>
                            <th>Plano</th>
                            <th>Natureza do Reajuste</th>
                            <th>Data Reajuste</th>
                            <th>Operador</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $hist_at_preco, SQLSRV_FETCH_ASSOC) ) {

                        if (is_null($row['HIST_DATA'])) {
                            $data = '';
                        }else{
                            $data = date_format($row['HIST_DATA'], 'd/m/Y');
                        }

                        echo "<tr>
                        <td>".$row['emp_cd'].                                    "</td>
                        <td>".$row['emp_ds'].                                    "</td>
                        <td>".$row['PLA_CD'].                              "</td>
                        <td>".$row['PLA_DS'].                                "</td>
                        <td>".$row['HIST_NATUREZA'].                                   "</td>
                        <td>".$data.                                   "</td>
                        <td>".$row['HIST_USUSISTEMA'].                                "</td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <h3>Outros Dados - Grupo de Empresa</h3>
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Grupo</th>                            
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $grupo_empresa, SQLSRV_FETCH_ASSOC) ) {

                        echo "<tr>
                        <td>".$row['Codigo'].                                    "</td>
                        <td>".$row['Grupo'].                                    "</td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html'?>