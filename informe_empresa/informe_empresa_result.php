<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_informe_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Informe Empresa</h3>
    </div><br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Empresa</th>
                            <th>Endereço</th>
                            <th>Numero</th>
                            <th>CEP</th>
                            <th>Bairro</th>
                            <th>Cidade</th>
                            <th>UF</th>
                            <th>Contato</th>
                            <th>IE</th>
                            <th>CGC</th>
                            <th>CPF</th>
                            <th>Fone1</th>
                            <th>Fone2</th>
                            <th>Agregado</th>
                            <th>Inclusão</th>
                            <th>Carencia</th>
                            <th>Fator</th>
                            <th>Exclusão</th>
                            <th>Adicional</th>
                            <th>Pessoa</th>
                            <th>CobFatorModerado</th>
                            <th>Dt Termino</th>
                            <th>Tipo Cobrança</th>
                            <th>Contrato</th>
                            <th>Idade</th>
                            <th>Email</th>
                            <th>Repasse</th>
                            <th>Pagamento</th>
                            <th>Nome Fantasia</th>
                            <th>OBS</th>
                            <th>Dt inicio</th>
                            <th>Região</th>
                            <th>Obs Carteirinha</th>
                            <th>Operador Repasse</th>
                            <th>Tipo Contrato</th>
                            <th>Plano Familiar</th>
                            <th>Plano Inidividual</th>
                            <th>Muda plano</th>
                            <th>Sip Ben</th>
                            <th>Sip Cont</th>
                            <th>Mês Reajuste</th>
                            <th>IR</th>
                            <th>Vl Minimo</th>
                            <th>Qtd Usuário Ativos</th>
                            <th>RN195</th>
                            <th>Dt Adap RN195</th>
                            <th>RN309</th>
                            <th>PLA_cd</th>
                            <th>PLA_ds</th>
                            <th>Visualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $consulta, SQLSRV_FETCH_ASSOC) ) {

                        if (is_null($row['EMP_DtTermino'])) {
                            $td_termino = '';
                        }else{
                            $td_termino = date_format($row['EMP_DtTermino'], 'd/m/Y');
                        }

                        if (is_null($row['Emp_dtInicio'])) {
                            $td_inicio = '';
                        }else{
                            $td_inicio = date_format($row['Emp_dtInicio'], 'd/m/Y');
                        }



                        echo "<tr>
                        <td>".$row['EMP_cd'].                                    "</td>
                        <td>".$row['EMP_ds'].                                    "</td>
                        <td>".$row['EMP_endereco'].                              "</td>
                        <td>".$row['EMP_EndNum'].                                "</td>
                        <td>".$row['EMP_CEP'].                                   "</td>
                        <td>".$row['EMP_bairro'].                                "</td>
                        <td>".$row['EMP_cidade'].                                "</td>
                        <td>".$row['EMP_UF'].                                    "</td>
                        <td>".$row['EMP_contato'].                               "</td>
                        <td>".$row['EMP_IE'].                                    "</td>
                        <td>".$row['EMP_CGC'].                                   "</td>
                        <td>".$row['EMP_CPF'].                                   "</td>
                        <td>".$row['EMP_fone1'].                                 "</td>
                        <td>".$row['EMP_fone2'].                                 "</td>
                        <td>".$row['EMP_agregado'].                              "</td>
                        <td>".$row['EMP_inclusao'].                              "</td>
                        <td>".$row['EMP_carencia'].                              "</td>
                        <td>".$row['EMP_fator'].                                 "</td>
                        <td>".$row['EMP_exclusao'].                              "</td>
                        <td>".$row['EMP_adicional'].                             "</td>
                        <td>".$row['emp_PESSOA'].                                "</td>
                        <td>".$row['EMP_CobFatorModerador'].                     "</td>
                        <td>".$td_termino.                                       "</td>
                        <td>".$row['EMP_TipoCobranca'].                          "</td>
                        <td>".$row['EMP_NroContrato'].                           "</td>
                        <td>".$row['EMP_IdadeDep'].                              "</td>
                        <td>".$row['EMP_mail'].                                  "</td>
                        <td>".$row['EMP_EmpRepasse'].                            "</td>
                        <td>".$row['EMP_Pagamento'].                             "</td>
                        <td>".$row['Emp_Fantasia'].                              "</td>
                        <td>".$row['Emp_Obs'].                                   "</td>
                        <td>".$td_inicio.                                        "</td>
                        <td>".$row['Emp_Regiao'].                                "</td>
                        <td>".$row['Emp_ObsCarteirinha'].                        "</td>
                        <td>".$row['Emp_OperadorRepasse'].                       "</td>
                        <td>".$row['Emp_TipoContrato'].                          "</td>
                        <td>".$row['Emp_PlanoFamiliar'].                         "</td>
                        <td>".$row['Emp_PlanoIndividual'].                       "</td>
                        <td>".$row['Emp_MudaPlano'].                             "</td>
                        <td>".$row['Emp_SipTipoBen'].                            "</td>
                        <td>".$row['Emp_SipTipoCont'].                           "</td>
                        <td>".$row['Emp_MesReajuste'].                           "</td>
                        <td>".$row['Emp_IR'].                                    "</td>
                        <td>".$row['Emp_VlMiminoIR'].                            "</td>
                        <td>".$row['QtdUsuAtivos'].                              "</td>
                        <td>".$row['Emp_RN195'].                                 "</td>
                        <td>".$row['Emp_DtAdapRN195'].                           "</td>
                        <td>".$row['Emp_RN309'].                                 "</td>
                        <td>".$row['PLA_cd'].                                    "</td>
                        <td>".$row['PLA_ds'].                                    "</td>
                        <td><a href='informe_empresa_desc.php?cod=".$row['EMP_cd']."'>
                        <button class='btn btn-primary'>Visualizar</button></a></td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

