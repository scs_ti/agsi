<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php
include '../assets/conn.php';


error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php';
include '../assets/session_started.php';

$empresa_select = sqlsrv_query($conn, 'Select Emp_cd,Emp_ds From Empresa Order by 2');
if('Select Emp_cd,Emp_ds From Empresa Order by 2' === false){
    die(print_r(sqlsrv_errors(), true));
}

$motivo_select = sqlsrv_query($conn, 'Select Bloqueamento,Blo_Motivo From Motivo_Bloqueio Order by 2');
if('Select Bloqueamento,Blo_Motivo From Motivo_Bloqueio Order by 2' === false){
    die(print_r(sqlsrv_errors(), true));
}


?>

<script>

</script>


<?php include '../html/menu_acess.html'; ?>

<div class="page-heading">
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Bloqueio de dependente por idade</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="relat_dep_idade.php" method="POST">
                                <div class="col-md-8 col-12">
                                    <div class="form-group">
                                        <label>Selecionar Empresa</label>
                                        <select class="form-control" id="input2" name="selec_emp">
                                            <option></option>
                                            <?php
                                            while( $row = sqlsrv_fetch_array( $empresa_select, SQLSRV_FETCH_ASSOC) ) {
                                               echo "<option id='input2' value=".$row['Emp_cd'].">".$row['Emp_cd']." - ".$row['Emp_ds']."</option>";}
                                               ?>
                                           </select>
                                       </div>

                                       <div class="form-group">
                                        <label>Selecionar Motivo</label>
                                        <select class="form-control" id="demonstracao" name="selec_motivo">
                                            <option></option>
                                            <?php
                                            while( $row = sqlsrv_fetch_array( $motivo_select, SQLSRV_FETCH_ASSOC) ) {
                                               echo "<option id='input2' value=".$row['Blo_Motivo'].">".$row['Blo_Motivo']."".$row['Motivo_Bloqueio']."</option>";}
                                               ?>
                                           </select>
                                       </div>

                                       <div class="form-group"><br>
                                        <a href="relat_dep_idade.php">    
                                            <input type="button" class="btn btn-primary me-1 mb-1" name="teste" value="Buscar">
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php include '../html/footer.html' ?>