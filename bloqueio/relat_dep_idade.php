<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';

$selec_emp = $_POST['selec_emp'];
$selec_motivo = $_POST['selec_motivo'];


$_SESSION['consulta'] = "SELECT c_EmpUsu, Emp_ds, c_ctrusu, c_NomUsu, a.IDep_TipoDep, a.IDep_Idade, b.TDep_ds
FROM t_ArqUsu
INNER JOIN Empresa ON c_EmpUsu = Emp_cd
INNER JOIN Plano ON c_PlaUsu = Pla_cd
INNER JOIN Emp_IdadeDependentes a ON c_EmpUsu = a.IDep_Empresa and Usu_TipoDependente = a.IDep_TipoDep 
INNER JOIN Tipo_Dependente b ON a.IDep_TipoDep = b.TDep_cd";

$_SESSION['consulta'] = sqlsrv_query($conn, $_SESSION['consulta']);
if ($_SESSION['consulta'] === false) {
    die(print_r(sqlsrv_errors(), true));
}

?>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario'] ?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Bloqueio de dependente por motivo de idade</h3>
    </div>
    <br />
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Cód. Empresa</th>
                            <th>Empresa</th>
                            <th>Cód Usuário</th>
                            <th>Dependente</th>
                            <th>Tipo</th>
                            <th>Idade</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $_SESSION['c_ctrusu'] = '';
                        while ($row = sqlsrv_fetch_array($_SESSION['consulta'], SQLSRV_FETCH_ASSOC)) {
                            echo "<tr>
                            <td>" . $row['c_ctrusu'] .                                    "</td>
                            <td>" . $row['Emp_ds'] .                                    "</td>
                            <td>" . $row['c_EmpUsu'] .                                    "</td>
                            <td>" . $row['c_NomUsu'] .                                    "</td>
                            <td>" . $row['TDep_ds'] .                                   "</td>
                            <td>" . $row['IDep_Idade'] .                                    "</td>
                            </tr>";

                            if (strlen($_SESSION['c_ctrusu']) < 1) {
                                $_SESSION['c_ctrusu'] .= "'" . $row['c_ctrusu'] . "'";
                            }
                            if (strlen($_SESSION['c_ctrusu']) >= 1) {
                                $_SESSION['c_ctrusu'] .= ", '" . $row['c_ctrusu'] . "'";
                            }
                        }
                        ?>

                    </tbody>
                </table>


                <div class="col-12 d-flex col-md-6 order-md-1">
                    <a href="" style="color: white;">
                        <button type="submit" class="btn btn-primary me-1 mb-1">Gravar</a></button>
                    <a href="gera_plan_dep_idade.php" style="color: white;">
                        <button type="submit" class="btn btn-info me-1 mb-1">Imprimir</a></button>
                </div>
            </div>
        </div>

    </section>
</div>

<?php include '../html/footer.html' ?>