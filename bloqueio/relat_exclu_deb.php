<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';


unset($_SESSION['c_ctrUsu']);

$data_ini = explode("-", $_POST['data_ini']);
$dti = $data_ini[2]."/".$data_ini[1]."/".$data_ini[0];

$data_fim = explode("-", $_POST['data_fim']);
$dtf = $data_fim[2]."/".$data_fim[1]."/".$data_fim[0];

$dti2 = $data_ini[1]."/".$data_ini[2]."/".$data_ini[0];

$data_venc = explode("-", $_POST['data_venc']);
$dtfv = $data_venc[3].$data_venc[2]."/".$data_venc[1]."/".$data_venc[0];

$_SESSION['dtf'] = $dtf;

$selec_emp_inicial = $_POST['selec_emp_inicial'];
$selec_emp_final = $_POST['selec_emp_final'];
$selec_mot_can = $_POST['selec_mot_can'];
$mensa_atraso = $_POST['mensa_atraso'];
$pessoa_f_j = $_POST['pessoa_f_j'];
$relatorio = $_POST['relatorio'];
$ordenar_por = $_POST['ordenar_por'];
$_SESSION['mot_exclu'] = $_POST['mot_exclu'];

$var .= '';

if ($_POST['empresa'] != "Todas" or	isset($selec_emp_inicial)) {
    $var .= "and c_EmpUsu = ".$selec_emp_inicial." ";
}

if ($_POST['selec_mot_can'] != "Todos" or isset($selec_mot_can)) {
    $var .= "and MCan_cd = ".$selec_mot_can." ";
}


$_SESSION['consultaexclusao'] = "Select '$pessoa_f_j' as Tipo,u.c_ctrusu,u.c_nomusu, 
count(cp.pg_usu_cd) 'pg_usu_cd'  INTO 
From t_ArqUsu U 
Inner join contapagar cp on u.c_ctrusu=cp.pg_usu_cd
Inner join empresa e on u.c_empusu=e.Emp_cd
where cp.pg_statusBaixa='$ordenar_por'
and dbo.fDataVencto(PG_Vencto,PG_Mes) BETWEEN '$dti' and '$dti2'
and cp.pg_usu_cd is not null and u.c_dteusu is null
and dbo.fDataVencto(PG_Vencto,PG_Mes) < '$dtfv'
and e.emp_pessoa='$pessoa_f_j'
group by u.c_ctrusu,u.c_nomusu
having count(cp.pg_usu_cd)>='$mensa_atraso'
UNION
Select '$pessoa_f_j' as Tipo,convert(char(5),e.emp_cd),e.Emp_ds, 
count(cp.pg_empresa)
From Empresa E 
inner join contapagar cp on e.emp_cd=cp.pg_empresa
where cp.pg_statusBaixa='$ordenar_por'
and dbo.fDataVencto(PG_Vencto,PG_Mes) BETWEEN '$dti' and '$dti2'
and cp.pg_empresa is not null and e.emp_dttermino is null
and dbo.fDataVencto(PG_Vencto,PG_Mes) < '$dtfv'
and e.emp_pessoa='$pessoa_f_j'
group by e.emp_cd,e.Emp_ds
having count(cp.pg_empresa)>='$mensa_atraso'
Order By c_CtrUsu, c_NomUsu";


$_SESSION['consultaexclusao_sql'] = sqlsrv_query($conn, $_SESSION['consultaexclusao']);
    if ($_SESSION['consultaexclusao'] === false) {
        die(print_r(sqlsrv_errors(), true));
    };
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="exclu_deb.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Exclusão por débito</h3>
    </div>
    <br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Cód. Empresa</th>
                            <th>Usuário</th>
                            <th>Mensalidade Atraso</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      	 $_SESSION['c_ctrusu'] = '';
                         while( $row = sqlsrv_fetch_array($_SESSION['consultaexclusao_sql'], SQLSRV_FETCH_ASSOC) ) {
                         echo "<tr>
                         <td>".$row['emp_cd'].                                    "</td>
                         <td>".$row['Emp_ds'].                                    "</td>
                         <td>".$row[$mensa_atraso].                                    "</td>
                         <td>".$row[$pessoa_f_j].                                    "</td>
                         </tr>";                        
                    	
	                     	if (strlen($_SESSION['c_ctrusu']) < 1) {
	                         	$_SESSION['c_ctrusu'] .= "'".$row['c_ctrusu']."'";
	                         }if (strlen($_SESSION['c_ctrusu']) >= 1) {
	                         	$_SESSION['c_ctrusu'] .= ", '".$row['c_ctrusu']."'";
	                         }
                    	 }
                      ?>
                        </tbody>
                    </table>

                    <div class="col-12 d-flex col-md-6 order-md-1">
                        <a href="gera_plan_del_p_idade.php" style="color: white;">
                            <button type="submit" class="btn btn-primary me-1 mb-1">
                                Salvar
                            </a>
                        </button>
                    </div>
                </div>
            </div>

        </section>
    </div>

<?php include '../html/footer.html' ?>

