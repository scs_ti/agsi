<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/grupo_empresa.php';
include '../html/body_header.html';
include '../assets/session_started.php';


unset($_SESSION['c_ctrUsu']);

$data_ini = explode("-", $_POST['data_ini']);
$dti = $data_ini[2]."/".$data_ini[1]."/".$data_ini[0];
$data_fim = explode("-", $_POST['data_fim']);
$dtf = $data_fim[2]."/".$data_fim[1]."/".$data_fim[0];
$dti2 = $data_ini[1]."/".$data_ini[2]."/".$data_ini[0];
$_SESSION['dtf'] = $dtf;

$selec_emp = $_POST['selec_emp'];
$selec_plano = $_POST['selec_plano'];
$selec_contrato = $_POST['selec_contrato'];
$_SESSION['mot_exclu'] = $_POST['mot_exclu'];

$var .= '';

if ($_POST['empresa'] != "Todas" or	isset($selec_emp)) {
    $var .= "and c_EmpUsu = ".$selec_emp." ";
}

if ($_POST['plano'] != "Todos" or isset($selec_plano)) {
    $var .= "and c_PlaUsu = ".$selec_plano." ";
}



$_SESSION['user2delete'] = "SELECT c_EmpUsu, Emp_ds, c_ctrusu, c_NomUsu, c_NasUsu,
Year('".$dti2."')-Year(c_NasUsu)-1+( CASE WHEN MONTH('".$dti2."') > MONTH(c_NasUsu) THEN 1 ELSE
        CASE WHEN MONTH('".$dti2."') = MONTH(c_NasUsu) and DAY('".$dti2."') >= DAY(C_NasUSU) THEN 1 ELSE 0 END END ) Idade
FROM t_ArqUsu INNER JOIN Empresa ON c_EmpUsu = Emp_cd
INNER JOIN Plano ON c_PlaUsu = Pla_cd
INNER JOIN Emp_IdadeDependentes ON c_EmpUsu = IDep_Empresa and Usu_TipoDependente = IDep_TipoDep
WHERE c_TipUsu = 'U' and c_ParUsu <> 'T' and c_ParUsu <> 'E' ".$var."
and Emp_DtTermino IS NULL
and ( dbo.fStatusUsuario(C_IncUsu, C_DteUsu, '".$dti2."') = 'A' )
and Emp_IdadeDep = 'S'
and IDep_Idade <= Year('".$dti2."')-Year(c_NasUsu)-1+( CASE WHEN MONTH('".$dti2."') > MONTH(c_NasUsu) THEN 1 ELSE
        CASE WHEN MONTH('".$dti2."') = MONTH(c_NasUsu) and DAY('".$dti2."') >= DAY(C_NasUSU) THEN 1 ELSE 0 END END )
ORDER BY c_EmpUsu, c_CtrUsu";

$_SESSION['user2delete_sql'] = sqlsrv_query($conn, $_SESSION['user2delete']);
    if ($_SESSION['user2delete'] === false) {
        die(print_r(sqlsrv_errors(), true));
    }
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="exclu_dep_idade.php" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Exclusão Dep Idade</h3>
    </div>
    <br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Cód. Empresa</th>
                            <th>Empresa</th>
                            <th>Cód Usuário</th>
                            <th>Nome</th>
                            <th>Dt Nascimento</th>
                            <th>Idade</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      	$_SESSION['c_ctrusu'] = '';
                        while( $row = sqlsrv_fetch_array($_SESSION['user2delete_sql'], SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                        <td>".$row['c_EmpUsu'].                                    "</td>
                        <td>".$row['Emp_ds'].                                    "</td>
                        <td>".$row['c_ctrusu'].                                    "</td>
                        <td>".$row['c_NomUsu'].                                    "</td>
                        <td>".$row['c_NasUsu']->format('d/m/Y').                                    "</td>
                        <td>".$row['Idade'].                                    "</td>
                        </tr>";                        
                    	
	                    	if (strlen($_SESSION['c_ctrusu']) < 1) {
	                        	$_SESSION['c_ctrusu'] .= "'".$row['c_ctrusu']."'";
	                        }if (strlen($_SESSION['c_ctrusu']) >= 1) {
	                        	$_SESSION['c_ctrusu'] .= ", '".$row['c_ctrusu']."'";
	                        }
                    	}
                      ?>
                        </tbody>
                    </table>

                    <div class="col-12 d-flex col-md-6 order-md-1">
                        <a href="gera_plan_del_p_idade.php" style="color: white;">
                            <button type="submit" class="btn btn-primary me-1 mb-1">Salvar</a></button>
                        <a href="del_dep.php" style="color: white;">
                            <button type="submit" class="btn btn-danger me-1 mb-1">Excluir</a></button>
                    </div>
                </div>
            </div>

        </section>
    </div>

<?php include '../html/footer.html' ?>

