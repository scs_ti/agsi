<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$empresa_ini = sqlsrv_query($conn, 'SELECT DISTINCT EMP_cd, EMP_ds FROM Empresa ORDER BY 1 ASC');
    if( 'SELECT DISTINCT EMP_cd, EMP_ds FROM Empresa ORDER BY 1 ASC' === false) {
        die( print_r( sqlsrv_errors(), true) );
    }

$plano = sqlsrv_query($conn, "SELECT PLA_cd, PLA_ds FROM Plano");
    if ("SELECT PLA_cd, PLA_ds FROM Plano" === false) {
        die(print_r(sqlsrv_errors(), true));
    }

$contrato = sqlsrv_query($conn, "SELECT PrecoContrato_cd, PrecoContrato_ds FROM PrecoContrato ORDER BY PrecoContrato_cd");
    if ("SELECT PrecoContrato_cd, PrecoContrato_ds FROM PrecoContrato ORDER BY PrecoContrato_cd" === false) {
        die(print_r(sqlsrv_errors(), true));
    }

$mot_can = sqlsrv_query($conn, "SELECT MCan_cd, MCan_ds FROM Mot_Cancelamento ORDER BY MCan_ds");
    if ("SELECT MCan_cd, MCan_ds FROM Mot_Cancelamento ORDER BY MCan_ds" === false) {
        die(print_r(sqlsrv_errors(), true));
    }



?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javASCript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Bloqueio / Cancelamento - Exclusão Dep Idade</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="relat_exc_pep_idade.php" method="POST">
                                    <div class="row">
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Empresa</label>
                                                <select id="options" class="form-control" name="empresa" onchange="verifica_emp(this.value)">
                                                    <option value="Todas">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Selecionar Empresa</label>
                                                <select class="form-control" id="input2" disabled name="selec_emp">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $empresa_ini, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['EMP_cd'].">".$row['EMP_cd']." - ".$row['EMP_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Plano</label>
                                                <select id="options" class="form-control" name="plano" onchange="usuexiste(this.value)">
                                                    <option value="Todos">Todas</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-12">
                                            <div class="form-group">
                                                <label>Selecionar plano</label>
                                                <select class="form-control" id="live_search" disabled name="selec_plano">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $plano, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['PLA_cd'].">".$row['PLA_cd']." - ".$row['PLA_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-12">
                                            <div class="form-group">
                                                <label>Contrato</label>
                                                <select id="options" class="form-control" name="contrato" onchange="selecionadata(this.value)">
                                                    <option value="Todos">Todos</option>
                                                    <option value="Selecionar">Selecionar</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Selecionar contrato</label>
                                                <select class="form-control" id="date1" disabled name="selec_contrato">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $contrato, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['PrecoContrato_cd'].">".$row['PrecoContrato_cd']." - ".$row['PrecoContrato_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5 col-12">
                                            <div class="form-group">
                                                <label>Motivo Exclusão</label>
                                                <select class="form-control" name="mot_exclu">
                                                    <option></option>
                                                    <?php 
                                                    while( $row = sqlsrv_fetch_array( $mot_can, SQLSRV_FETCH_ASSOC) ) {
                                                        echo "<option id='input2' value=".$row['MCan_cd'].">".$row['MCan_cd']." - ".$row['MCan_ds']."</option>";}
                                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Data Base</label>
                                                <input type="date" required="required" class="form-control" name="data_ini">
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-12">
                                            <div class="form-group">
                                                <label>Data Exclusão</label>
                                                <input type="date" required="required" class="form-control" name="data_fim">
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 d-flex justify-content-end">
                                            <button type="submit"
                                            class="btn btn-primary me-1 mb-1">Buscar</button>
                                            <button type="reset"
                                            class="btn btn-light-secondary me-1 mb-1">Limpar Campos</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php include '../html/footer.html'?>