<?php
	error_reporting(E_ERROR | E_PARSE);

	include '../assets/conn.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Contato</title>
	<head>
	<body>
		<?php
		// Definimos o nome do arquivo que será exportado
		$arquivo = 'deletar_por_idade.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '';
		$html .= '<table border="1">';

		// Configurações da tabela
		#$html .= '<tr>';
		#$html .= '<td colspan="5">Planilha Mensagem de Contatos</tr>';
		#$html .= '</tr>';
		
		
		$html .= '<tr>';
            $html .= '<td><b>Cód. Empresa</b></td>';
            $html .= '<td><b>Empresa</b></td>';
            $html .= '<td><b>Cód Usuário</b></td>';
            $html .= '<td><b>Nome</b></td>';
            $html .= '<td><b>Dt Nascimento</b></td>';
            $html .= '<td><b>Idade</b></td>';
        $html .= '</tr>';
		
		//Selecionar todos os itens da tabela 

        $user2delete = sqlsrv_query($conn, $_SESSION['user2delete']);
        if ($_SESSION['user2delete'] === false) {
            die(print_r(sqlsrv_errors(), true));
        }
    	
        while( $row = sqlsrv_fetch_array($user2delete, SQLSRV_FETCH_ASSOC) ) {
            $html .= '<tr>';
            $html .= "<td>".$row['c_EmpUsu'].                   "</td>";
            $html .= "<td>".$row['Emp_ds'].                     "</td>";
            $html .= "<td>".$row['c_ctrusu'].                   "</td>";
            $html .= "<td>".$row['c_NomUsu'].                   "</td>";
            $html .= "<td>".$row['c_NasUsu']->format('d/m/Y').  "</td>";
            $html .= "<td>".$row['Idade'].                      "</td>";
            $html .= '</tr>';
        }
                        
                    
		// Configurações header para forçar o download
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
		header ("Content-Description: PHP Generated Data" );
		// Envia o conteúdo do arquivo
		echo $html;
		exit; ?>
	</body>
</html>