<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

<?php
include '../assets/conn.php';


error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php';
include '../assets/session_started.php';

$empresa_inicial = sqlsrv_query($conn, 'SELECT Emp_cd, Emp_ds FROM Empresa ORDER BY Emp_ds');
if ('SELECT Emp_cd, Emp_ds FROM Empresa ORDER BY Emp_ds' === false) {
    die(print_r(sqlsrv_errors(), true));
}

$empresa_final = sqlsrv_query($conn, 'SELECT Emp_cd, Emp_ds FROM Empresa ORDER BY Emp_ds');
if ('SELECT Emp_cd, Emp_ds FROM Empresa ORDER BY Emp_ds' === false) {
    die(print_r(sqlsrv_errors(), true));
}

$motivo_can = sqlsrv_query($conn, 'SELECT MCan_cd, MCan_ds FROM Mot_Cancelamento ORDER BY MCan_ds asc');
if ('SELECT MCan_cd, MCan_ds FROM Mot_Cancelamento ORDER BY MCan_ds asc' === false) {
    die(print_r(sqlsrv_errors(), true));
}

?>


<div class="page-heading">
    <?php include '../html/menu_acess.html'; ?>
</div>

<div class="col-12 d-flex col-md-6 order-md-1">
    <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
    <h3>Exclusão por débito</h3>
</div><br>

<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form" action="relat_exclu_deb.php" method="POST">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label>Empresa Inicial</label>
                                        <select class="form-control" id="input1" name="selec_emp_inicial">
                                            <option></option>
                                            <?php
                                            while ($row = sqlsrv_fetch_array($empresa_inicial, SQLSRV_FETCH_ASSOC)) {
                                                echo "<option id='input2' value=" . $row['Emp_cd'] . ">" . $row['Emp_cd'] . " - " . $row['Emp_ds'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Empresa Final</label>
                                        <select class="form-control" id="input2" name="selec_emp_final">
                                            <option></option>
                                            <?php
                                            while ($row = sqlsrv_fetch_array($empresa_final, SQLSRV_FETCH_ASSOC)) {
                                                echo "<option id='input2' value=" . $row['Emp_cd'] . ">" . $row['Emp_cd'] . " - " . $row['Emp_ds'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <!--<div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Todas
                                        </label>
                                    </div>-->
                                    <br />
                                    <div class="form-group">
                                        <label>Motivo cancelamento</label>
                                        <select class="form-control" name="selec_mot_can" id="input3">
                                            <option></option>
                                            <?php
                                            while ($row = sqlsrv_fetch_array($motivo_can, SQLSRV_FETCH_ASSOC)) {
                                                echo "<option id='input2' value=" . $row['MCan_cd'] . ">" . $row['MCan_cd'] . " - " . $row['MCan_ds'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Empresa Pessoa</label>
                                        <select class="form-control" name="empresa_pessoa">
                                            <option value="F">Física</option>
                                            <option value="J">Jurídica</option>
                                        </select>
                                    </div>
                                </div>


                               <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ordenar por</label>
                                        <select class="form-control" name="empresa_pessoa">
                                            <option value="C">Carteirinha</option>
                                            <option value="N">Nome</option>
                                            <option value="A">Atraso</option>
                                            <option value="V">Vigência</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Relatório</label>
                                        <select class="form-control" name="relatorio">
                                            <option value="C">Analitico</option>
                                            <option value="N">Sintentico</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Atraso</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Mensalidades em atraso
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Acumulativo (Ano de vigencia)
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Regulamentação</label>
                                    <br />
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Todas
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Antiga Regulamentação
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Nova Regulamentação
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Insira a data de vencimento</label>
                                    <div id="date-picker-example" class="md-form md-outline input-with-post-icon datepicker" inline="true">
                                        <input type="date" required="required" class="form-control" name="data_venc">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Mensalidades em atraso</label>
                                    <div class="form-check">

                                        <input type="checkbox" name="check" value="1">
                                        <input type="checkbox" name="check" value="2" />
                                        <input type="checkbox" name="check" value="3" />
                                        <input type="checkbox" name="check" value="4" />
                                    </div>

                                    <!--<input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                    <label class="form-check-label" for="flexCheckIndeterminate">
                                        Todas em aberto
                                    </label>-->
                                </div>
                            </div>

                            <div class="row">
                                <!--<div class="col-md-4">
                                    <label>Dias de atraso</label>
                                    <input type="date" required="required" class="form-control" name="data_ini">
                                    </input>
                                </div>-->

                                <div class="col-md-6">
                                    <label>Data inicio</label>
                                    <input type="date" required="required" class="form-control" name="data_ini">
                                </div>

                                <div class="col-md-6">
                                    <label>Data fim</label>
                                    <input type="date" required="required" class="form-control" name="data_fim">
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <!--<div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Calcular utilizações
                                        </label>
                                    </div>
                                </div>-->

                                <!--<div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Considerar somente em aberto
                                        </label>
                                    </div>
                                </div>-->

                                <!--<div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
                                        <label class="form-check-label" for="flexCheckIndeterminate">
                                            Cancelar apenas boletos com vencimento futuro
                                        </label>
                                    </div>
                                </div>-->
                            </div>

                            <div class="form-group"><br>
                                <a href="relat_exclu_deb.php">
                                    <input type="button" class="btn btn-primary me-1 mb-1" name="teste" value="Buscar">
                                </a>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>