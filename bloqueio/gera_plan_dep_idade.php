<?php
error_reporting(E_ERROR | E_PARSE);

include '../assets/conn.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Contato</title>
    <head>
        <body>
            <?php
        // Definimos o nome do arquivo que será exportado
            $arquivo = 'relatorio_dep_idade.xls';

        // Criamos uma tabela HTML com o formato da planilha
            $html = '';
            $html .= '<table border="1">';

        // Configurações da tabela
        #$html .= '<tr>';
        #$html .= '<td colspan="5">Planilha Mensagem de Contatos</tr>';
        #$html .= '</tr>';


            $html .= '<tr>';


            if(!empty($_SESSION['var1'])){
                $html .= '<td><b>Cód Empresa</b></td>';
            }if(!empty($_SESSION['var2'])){
                $html .= '<td><b>Empresa</b></td>';
            }if(!empty($_SESSION['var3'])){
                $html .= '<td><b>Cód Usuário</b></td>';
            }if(!empty($_SESSION['var4'])){
                $html .= '<td><b>Dependente</b></td>';
            }if(!empty($_SESSION['var5'])){
                $html .= '<td><b>Tipo</b></td>';
            }if(!empty($_SESSION['var6'])){
                $html .= '<td><b>Idade</b></td>';
            };

            $html .= '</tr>';

        //Selecionar todos os itens da tabela 
            $select_from = sqlsrv_query($conn, $_SESSION['consulta']);
            if( $_SESSION['consulta'] === false) {
                die( print_r( sqlsrv_errors(), true) );
            }

            while( $row = sqlsrv_fetch_array($select_from, SQLSRV_FETCH_ASSOC) ) {

                $html .= '<tr>';

                if(!empty($_SESSION['var1'])){
                    $html .= "<td>".$row['c_ctrusu'].                "</td>";
                }if(!empty($_SESSION['var2'])){
                    $html .= "<td>".$row['Emp_ds'].                "</td>";
                }if(!empty($_SESSION['var3'])){
                    $html .= "<td>".$row['c_EmpUsu'].               "</td>";
                }if(!empty($_SESSION['var4'])){
                    $html .= "<td>".$row['c_NomUsu'].           "</td>";
                }if(!empty($_SESSION['var5'])){
                    $html .= "<td>".$row['TDep_ds'].                  "</td>";
                }if(!empty($_SESSION['var6'])){
                    $html .= "<td>".$row['IDep_Idade'].            "</td>";
                };

                $html .= '</tr>';
            };
        // Configurações header para forçar o download
            header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
            header ("Cache-Control: no-cache, must-revalidate");
            header ("Pragma: no-cache");
            header ("Content-type: application/x-msexcel");
            header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
            header ("Content-Description: PHP Generated Data" );
        // Envia o conteúdo do arquivo
            echo $html;
            exit; ?>
        </body>
        </html>