<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_plano.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Cadastro Plano</h3>
    </div><br>
    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Plano</th>
                            <th>Cód Simpas</th>
                            <th>Código SCPA</th>
                            <th>Acomodação</th>
                            <th>Observação</th>
                            <th>Encaminhar MS</th>
                            <th>Cód Carência</th>
                            <th>Tabela de Carência</th>
                            <th>Descrição Resumida</th>
                            <th>Status</th>
                            <th>Cód. Tipo Contrato</th>
                            <th>Desc. Tipo Contrato</th>
                            <th>Visualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $cadastro_plano, SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                        <td>".$row['PLA_cd'].                                    "</td>
                        <td>".$row['PLA_ds'].                                    "</td>
                        <td>".$row['PLA_simpas'].                                    "</td>
                        <td>".$row['PLA_SCPA'].                                    "</td>
                        <td>".$row['PLA_acomodacao'].                                    "</td>
                        <td>".$row['PLA_obs'].                                    "</td>
                        <td>".$row['PLA_encaminhar'].                                    "</td>
                        <td>".$row['PLA_carencia'].                                    "</td>
                        <td>".$row['CAR_ds'].                                    "</td>
                        <td>".$row['Pla_DescResumo'].                                    "</td>
                        <td>".$row['Pla_Status'].                                    "</td>
                        <td>".$row['Pla_TipoContrato'].                                    "</td>
                        <td>".$row['PrecoContrato_ds'].                                    "</td>
                        <td><a href='cadastro_plano_comple.php?cod=".$row['PLA_cd']."'><button class='btn btn-primary'>Visualizar</button></a></td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

