<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_plano.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Débitos Usuário</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                <div class="row">
                                <?php 
                                    while( $row = sqlsrv_fetch_array( $cadastro_plano_full, SQLSRV_FETCH_ASSOC) ) {
                                    
                                    if($row['Pla_TipoPlano'] == 2){
                                        $plano_desc = 'Plano sem Co-participação';
                                    }elseif($row['Pla_TipoPlano'] == 1){
                                        $plano_desc = 'Plano com Co-participação';
                                    }

                                    echo "
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Plano</label>
                                            <input type='text' class='form-control' value='".$row['PLA_cd']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Descrição</label>
                                            <input type='text' class='form-control' value='".$row['PLA_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Descrição Resumida</label>
                                            <input type='text' class='form-control' value='".$row['Pla_DescResumo']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>SINPAS</label>
                                            <input type='text' class='form-control' value='".$row['PLA_simpas']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Acomodação</label>
                                            <input type='text' class='form-control' value='".$row['PLA_acomodacao']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Acomodação TISS COD</label>
                                            <input type='text' class='form-control' value='".$row['Pla_AcomodacaoTiss']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Acomodação TISS</label>
                                            <input type='text' class='form-control' value='".$row['TAc_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Encaminhar ANS</label>
                                            <input type='text' class='form-control' value='".$row['PLA_encaminhar']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-2 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Plano COD</label>
                                            <input type='text' class='form-control' value='".$row['Pla_TipoPlano']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-5 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Plano DESC</label>
                                            <input type='text' class='form-control' value='".$plano_desc."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-5 col-12'>
                                        <div class='form-group'>
                                            <label>Código SCPA</label>
                                            <input type='text' class='form-control' value='".$row['Pla_SCPA']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-2 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Cobertura COD</label>
                                            <input type='text' class='form-control' value='".$row['Pla_TipoCobertura']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-4 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Cobertura DESC</label>
                                            <input type='text' class='form-control' value='".$row['PrecoCobertura_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-2 col-12'>
                                        <div class='form-group'>
                                            <label>Abrangência Geo COD</label>
                                            <input type='text' class='form-control' value='".$row['Pla_AbranGeog']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-4 col-12'>
                                        <div class='form-group'>
                                            <label>Abrangência Geo DESC</label>
                                            <input type='text' class='form-control' value='".$row['Abr_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-2 col-12'>
                                        <div class='form-group'>
                                            <label>Fator Moderado</label>
                                            <input type='text' class='form-control' value='".$row['PLA_FatorModerador']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-10 col-12'>
                                        <div class='form-group'>
                                            <label>Observação</label>
                                            <input type='text' class='form-control' value='".$row['PLA_obs']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-2 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Contrato COD</label>
                                            <input type='text' class='form-control' value='".$row['Pla_TipoContrato']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-4 col-12'>
                                        <div class='form-group'>
                                            <label>Tipo Contrato DESC</label>
                                            <input type='text' class='form-control' value='".$row['PrecoContrato_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Tab Carência COD</label>
                                            <input type='text' class='form-control' value='".$row['PLA_carencia']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Tab Carência DESC</label>
                                            <input type='text' class='form-control' value='".$row['CAR_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Grupo COD</label>
                                            <input type='text' class='form-control' value='".$row['Pla_Grupo']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Grupo DESC</label>
                                            <input type='text' class='form-control' value='".$row['gp_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Regulamentação</label>
                                            <input type='text' class='form-control' value='".$row['CAR_ds']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    <div class='col-md-3 col-12'>
                                        <div class='form-group'>
                                            <label>Status</label>
                                            <input type='text' class='form-control' value='".$row['Pla_Status']."' disabled>
                                            </select>
                                        </div>
                                    </div>
                                    ";
                                }?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="section">
            <div class="card">
                <div class="card-header">
                    Carências
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                            <th>Código</th>
                            <th>Carência</th>
                            <th>Dias</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        while( $row = sqlsrv_fetch_array( $carencias, SQLSRV_FETCH_ASSOC) ) {
                            echo "<tr>
                                <td>".$row['POS_cd'].                      "</td>
                                <td>".$row['POS_ds'].                      "</td>
                                <td>".$row['Car_Dias'].                    "</td>
                            </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html'?>