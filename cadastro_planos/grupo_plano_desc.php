<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../consultas/consulta_plano.php';
include '../html/body_header.html';
include '../assets/session_started.php';
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>Cadastro Plano</h3>
    </div><br>

    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="exibe_informe_usuario.php" method="POST">
                                <!--<form class="form" action="consultas/informe_usuario.php" method="POST">-->
                                <?php 
                                    while( $row = sqlsrv_fetch_array( $grupo_plano_info, SQLSRV_FETCH_ASSOC) ) {
                                    echo "
                                        <div class='row'>
                                            <div class='col-md-4 col-12'>
                                                <div class='form-group'>
                                                    <label>Código</label>
                                                    <input type='text' id='first-name-column' readonly='readonly' value=".$row['grp_codigo']." class='form-control'>
                                                </div>
                                            </div>
                                            <div class='col-md-4 col-12'>
                                                <div class='form-group'>
                                                    <label>Descrição</label>
                                                    <input type='text' id='first-name-column' readonly='readonly' value=".$row['grp_Descricao']." class='form-control'>
                                                </div>
                                            </div>
                                            <div class='col-md-4 col-12'>
                                                <div class='form-group'>
                                                    <label>Operadora</label>
                                                    <input type='text' id='first-name-column' readonly='readonly' value='".$row['grp_operadora']."' class='form-control'>
                                                </div>
                                            </div>
                                        </div>
                                    ";}
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Seq</th>
                            <th>Cód Empresa</th>
                            <th>Empresa</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                        while( $row = sqlsrv_fetch_array( $grupo_plano_d, SQLSRV_FETCH_ASSOC) ) {
                        echo "<tr>
                        <td>".$row['gpi_seq'].                                    "</td>
                        <td>".$row['gpi_empresa'].                                    "</td>
                        <td>".$row['empresa'].                                    "</td>
                        </tr>";}
                      ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<?php include '../html/footer.html' ?>

