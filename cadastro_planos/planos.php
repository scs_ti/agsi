<?php
include '../assets/conn.php';

error_reporting(E_ERROR | E_PARSE);
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>


<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Cadastro Plano</h3>
            </div>
        </div>
    </div>

    <!-- // Basic multiple Column Form section start -->
    <section id="multiple-column-form">
            <div class="col-12">
                <div class="card">
                        <div class="card-body">
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <table class="table">
                                          <tbody>
                                            <tr>
                                              <th scope="row">1</th>
                                              <td>Cadastro de Planos</td>
                                              <td style="padding-left: 15%"><a href="cadastro_plano.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                            </tr>
                                          <tr>
                                              <th scope="row">2</th>
                                              <td>Grupo de Planos</td>
                                              <td style="padding-left: 15%"><a href="grupos_planos.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">3</th>
                                              <td>Carências Possíveis</td>
                                              <td style="padding-left: 15%"><a href="carencias_possiveis.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">4</th>
                                              <td>Tabela de Carências</td>
                                              <td style="padding-left: 15%"><a href="tabela_carencias.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">5</th>
                                              <td>Abrangência Geográfica</td>
                                              <td style="padding-left: 15%"><a href="abrangencia_geografica.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">6</th>
                                              <td>Tipo de Cobertura</td>
                                              <td style="padding-left: 15%"><a href="tipo_cobertura.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                          <tr>
                                              <th scope="row">7</th>
                                              <td>Tipo Plano</td>
                                              <td style="padding-left: 15%"><a href="tipo_plano.php"><button  class="btn btn-primary">Acessar</button></a></td>
                                          </tr>
                                      </tbody>
                                  </table>
                              </div>
                        </div>
                </div>
            </div>
  </section>
  <!-- // Basic multiple Column Form section end -->
</div>

<style>
    .td_left {
        padding-left: 20%;
    }
</style>


<?php include '../html/footer.html'?>