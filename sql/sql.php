<?php
error_reporting(E_ERROR | E_PARSE);
include '../assets/conn.php';
include '../html/head.html';
include '../html/body_header.html';
include '../js/js.php'; 
include '../assets/session_started.php';

$quarto = sqlsrv_query($conn, "SELECT * FROM Plano ORDER BY PLA_cd asc");
if( $quarto === false) {
  die( print_r( sqlsrv_errors(), true) );
}
?>

<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-12 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">Bem-vindo(a) <?php echo $_SESSION['usuario']?></li>
                        <li class="breadcrumb-item"><a href="../assets/logout.php">Logout</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-12 d-flex col-md-6 order-md-1">
        <a href="javascript:history.back()" style="color: white;"><button type="submit" class="btn btn-primary me-1 mb-1">Voltar</a></button>
        <h3>SQL</h3>
    </div><br>
    <section id="multiple-column-form">
        <div class="row match-height">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                            <div class="form-group">
                                                <label>Consulta</label>
                                                <textarea id="sql" class="form-control" name="sql" rows="5" cols="33" placeholder="Insira sua consulta"></textarea> 
                                            </div>
                                        </div>
                                <div class="col-12 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary me-1 mb-1">Buscar</button>
                                    <button type="reset" class="btn btn-light-secondary me-1 mb-1">Limpar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 
        <?php include '../consultas/consulta_sql.php';?>

        <section class="section">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                            <tr>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                if (!empty($consulta_sql)) {
                                    while( $row = sqlsrv_fetch_array( $consulta_sql, SQLSRV_FETCH_ASSOC) ) {
                                        echo "<tr>";
                                        foreach ($row as $val) {
                                            if (gettype($val) == 'object') {
                                                echo "<td>".date_format($val,"Y/m/d H:i:s")."</td>". PHP_EOL;
                                            }else{
                                            echo "<td>".$val ."</td>". PHP_EOL;
                                            }
                                        }
                                        echo "</tr>";
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    

    <?php include '../html/footer.html'?>